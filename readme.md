#### Downloading npm packages
~~~bash
npm install
~~~

#### Building npm bundle for vue
~~~bash
npm run prod
~~~

#### Downloading composer package  and dumping
~~~bash
composer install
composer dump-autoload
~~~

#### Configure project
~~~php
php artisan cache:clear
php artisan config:cache
php artisan key:generate
~~~
### Create a database name and change credential in `.env` file


### migrate and seed database with fake data
~~~bash
php artisan migrate --seed
~~~

### Serving laravel project
~~~
php artisan serve
~~~


### login user:
* shibu@gmail.com
* ridwanul@gmail.com
* palash@gmail.com
* sumon@gmail.com

##  password for all user: 
`secret`      
   
