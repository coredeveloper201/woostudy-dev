const mix = require('laravel-mix');
// const tailwindcss = require('tailwindcss');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (!mix.inProduction()) {
  mix.webpackConfig({
    devtool: 'inline-source-map'
  })
}

if (mix.inProduction()) {
  mix.options({
    uglify: {
      uglifyOptions: {
        compress: {
          warnings: false,
          drop_console: true
        },
        output: {
          comments: false
        }
      }
    }
  });
}

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
.browserSync('http://woostudy.bd/');

mix.webpackConfig({
    output: {
        chunkFilename: 'js/[name].[chunkhash].js',
    },
});
