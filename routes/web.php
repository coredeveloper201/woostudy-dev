<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WebController@index')->name('home');
Route::get('/dev-db-users', 'WebController@devDbUsers')->name('dev.db.users'); // this is import users
Route::get('/dev-db-migration', 'WebController@devDbMigration')->name('dev.db.migration'); // this is import data
Route::get('/dev-db-migration/del', 'WebController@devDbMigrationDeletePak')->name('dev.db.migration.del.pak'); // this is delete data api working fine
Route::get('/term-of-condition', 'WebController@term')->name('term-of-condition');
Route::get('/privacy-policy', 'WebController@policy')->name('privacy-policy');
Route::get('/services', 'WebController@services')->name('services');
Route::get('/how-it-works', 'WebController@howItWorks')->name('how-it-works');
Route::get('/news-and-events', 'WebController@newsEvent')->name('news-and-events');
Route::get('/faqs', 'WebController@faqs')->name('faqs');
Route::get('/jobs', 'WebController@jobs')->name('jobs');

Route::get('/about-us', 'WebController@aboutus')->name('about-us');
Route::get('/contact-us', 'WebController@contactus')->name('contact-us');

Route::get('/school-profile', 'WebController@schoolProfile')->name('school-profile');
Route::get('/email', 'WebController@email')->name('email');

/*Route::get( '/profile/{user_name}/{vue_route?}', 'ProfileController@index' )->where( 'vue_route', '(.*)' )->middleware( 'auth' );*/

//Footer contact form mail sent...
Route::post('contact-send', 'ContactController@sendMail')->name('contact-send');
Route::post('subscribe-email', 'ContactController@subscribe')->name('subscribe-email');

//For social popup
Route::get( 'redirect-url', 'Auth\SocialProviderController@redirectPopup' );
Route::get( 'set-role', 'Auth\SocialProviderController@setRoleView' );

Route::get('s/{short_url}', 'HomeController@shortUrl');

Route::group( ['namespace' => 'Auth'], function () {
    Route::post( 'login/setRole', 'SocialProviderController@setRole' )->name( 'setRole' );
    Route::get( 'login/{provider}', 'SocialProviderController@redirectToProvider' );
    Route::get( 'login/{provider}/callback', 'SocialProviderController@handleProviderCallback' );
});

/**
 * JSON End point
 */
require __dir__ . '/jsonend.php';

/**
 * Test End point
 */
require __dir__ . '/test.php';

Route::group( ['namespace' => 'Twitter', 'prefix' => 'twitter'], function () {
    //Set twitter role
    Route::get( 'geo-point', 'TweetController@testGeoPoint' );
    Route::get( 'search-from-db', 'TweetController@searchFromDB' );
    Route::get( 'fetch-twitter-from-db/{id}', 'TweetController@fetchTimeLineTweetsFromDB' );
    Route::get( 'fetch-twitter-from-rows', 'TweetController@fetchTweetsFromRows' );
    Route::get( 'query-by-db', 'TweetController@queryDbByIds' );
    Route::get( 'search-from-db-minimal-data', 'TweetController@searchFromDBMinimalData' );
    Route::get( 'init-tweets-for-twitter-timeline/{id?}', 'TweetController@initTweetsForTwitterTimeLine' );
    Route::get( 'populate-training-data/{query}', 'TweetController@populateTrainingData' );
    Route::get( 'populate-twitter-data', 'TweetController@populateTwitterData' );
    Route::get( 'get-user-timeline/{screenName}', 'TweetController@getUserTimeLine' );
    Route::get( 'tag-tweets-from-aliyan/{tweetIds}', 'TweetController@tagTweetsFromAliyan' );
    Route::get( 'enrich-pending-tweets', 'TweetController@enrichPendingTweets' );
    Route::get( 'get-twitter-response', 'TweetController@getTwitterResponse' );
    Route::get( 'populate-text-from-documents', 'TweetController@populateTextFromDocuments' );
    Route::get( 'inject-and-prepare-data', 'TweetController@injectAndPrepareData' );
    Route::post( 'search', 'TweetController@search' );
    Route::post( 'send-message', 'TweetController@sendMessage' );
    Route::post( 'retweet', 'TweetController@reTweet' );
    //Route::post( 'send-message', 'TweetController@sendDirectMessage' );
    Route::post( 'reply', 'TweetController@reply' );

    Route::get( 'login', 'TwitterAuthController@login' )->name('twitter.login');
    Route::get( 'login-check', 'TwitterAuthController@loginCheck' );
    Route::get( 'callback', 'TwitterAuthController@callback' )->name('twitter.callback');
    Route::get( 'logout', 'TwitterAuthController@logout' )->name('twitter.logout');
    Route::get( 'topics', 'TweetController@topics' )->name('twitter.topics');

    //Route::get( 'login/auth/{hash}', 'TwitterMessageAuthController@login' )->name('twitter.login.auth');
    //Route::get( 'callback/auth/{hash}', 'TwitterMessageAuthController@callback' )->name('twitter.callback.auth');
} );

Route::post( 'facebook/search', 'SocialController@fetch_facebook_feed' );
Route::post( 'instagram/search', 'SocialController@fetch_instagram_feed' );


Route::post( 'linkedin/search', 'SocialController@fetch_linkedin_feed' );



/**
 * Testing middleware
 */

Route::get( 'view/test', function () {
    return json_encode( auth()->user()->isStudent() );
} );

Route::get( '/view/student', function () {
    return 'only student can view student page';
} )->middleware( 'student' );

Route::get( '/view/admin', function () {
    return 'only admin can view admin page';
} )->middleware( 'admin' );

Route::get( '/view/school', function () {
    return 'only school can view school page';
} )->middleware( 'school' );

Route::get( '/view/tutor', function () {
    return 'only tutor can view tutor page';
} )->middleware( 'tutor' );

Route::get( '/view/counselor', function () {
    return 'only counselor can view counselor page';
} )->middleware( 'counselor' );

Route::get( '/view/parent', function () {
    return 'only parent can view parent page';
} )->middleware( 'parent' );

Auth::routes( ['verify' => true] );

Route::get( 'faker/user/{loop}', 'FakerController@runUser' )->name('faker.user');

Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::get('/public/profile/{school_slug}','ProfileController@publicProfileView');
});

Route::get( '/{vue_route?}', 'HomeController@index' )->where( 'vue_route', '(.*)' )->middleware( 'auth' );
