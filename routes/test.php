<?php

Route::get('/test', 'EmailStudentsController@sendConnectionInvitation');
Route::get('/delete-user', 'TestController@index');
Route::get('/backup-user', 'TestController@roleBackUsers');
Route::resource('instabot', 'InstabotController');
Route::get('/mongo-save/{user_role_id}/{user_id}', 'Wsapi\MongoSaveController@mongoSave');
