<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\User;
use Illuminate\Support\Facades\Broadcast;

Broadcast::channel( 'App.User.{id}', function ($user, $id) {
    return (int)$user->id === (int)$id;
} );


Broadcast::channel( 'chats.{id}-{another_id}', function ($user, $id, $another_id) {
    //\Log::info($user);
    if ($id && $another_id) {
        if ($id < $another_id) {
            if (in_array( $another_id, User::find( $id )->friendList() )) {
                return \App\User::find( $another_id );
            }
        } else {
            if (in_array( $id, User::find( $another_id )->friendList() )) {
                return \App\User::find( $id );
            }
        }
    }

} );