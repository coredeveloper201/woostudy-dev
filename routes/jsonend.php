<?php

Route::group( ['prefix' => 'json'], function () {

    /*
    global search
    */

    Route::get( 'search/{key}', 'HomeController@search');
    Route::get( 'setting', 'SettingController@setting');


    /**
     * Profile page
     */
    Route::get( 'public/profile/education-levels/{institute_id}', 'ProfileController@getAllEducationLevels' );
    Route::get( 'public/profile/programs/{institute_id}', 'ProfileController@getProgramsByEducationLevel' );
    Route::get( 'public/profile/{userName}', 'ProfileController@publicProfile' );
    Route::get( 'open-public/profile/{userName}', 'ProfileController@openPublicProfile' );
    Route::get( 'public/top-disciplines/{userName}', 'ProfileController@getTopDisciplines' );
    Route::get( 'get_auth_user_data', 'ProfileController@get_auth_user_data' );

    //personal info
    Route::get( 'get_profile_personal_info/{profile_id}', 'ProfileController@get_profile_personal_info' );
    Route::post( 'post_profile_personal_info', 'ProfileController@post_profile_personal_info' );


    //contact info
    Route::get( 'get_contact_details/{profile_id}', 'ProfileController@get_contact_details' );
    Route::post( 'post_contact_details', 'ProfileController@post_contact_details' );


    // social media url
    Route::get( 'get_social_media_details/{profile_id}', 'ProfileController@get_social_media_details' );
    Route::post( 'post_social_media_details', 'ProfileController@post_social_media_details' );


    // education details
    Route::get( 'get_education_details/{profile_id}', 'ProfileController@get_education_details' );
    Route::post( 'post_education_details', 'ProfileController@post_education_details' );
    Route::delete( 'delete_education_item/{id}', 'ProfileController@delete_education_item' );
    Route::put( 'update_education_item/{id}', 'ProfileController@update_education_item' );


    // ielts or tofel details
    Route::get( 'get_ielts_details/{profile_id}', 'ProfileController@get_ielts_details' );
    Route::post( 'post_ielts_details', 'ProfileController@post_ielts_details' );
    Route::delete( 'delete_ielts_item/{id}', 'ProfileController@delete_ielts_item' );
    Route::put( 'update_ielts_item/{id}', 'ProfileController@update_ielts_item' );


    //Institute
    Route::resource( 'institute-programs', 'ProgramController' );
    Route::resource( 'institute', 'InstituteController' );
    Route::get('institute-approval', 'InstituteController@showApprovalRequest');
    Route::post('institute-approval-change-status/{id}', 'InstituteController@changeApprovalStatus');
    Route::resource( 'education-level', 'EducationLevelController' );
    Route::resource( 'ielts-exam-level', 'IeltsExamLevelController' );

    //Countries, regions
    Route::get( 'regions', 'RegionController@index' );
    Route::get( 'countries', 'CountryController@index' );
    Route::get( 'states/country/{id}', 'StateController@byCountry' );
    Route::resource( 'states', 'StateController' );
    Route::resource( 'address', 'AddressController' );

    Route::resource( 'department', 'DepartmentController' );
    Route::resource( 'institute-role', 'InstituteRoleController' );

    //Users
    Route::group( ['namespace' => 'Auth'], function () {
        Route::post( 'password-change', 'UserController@changPassword' );
    } );

    Route::group( ['namespace' => 'User', 'prefix' => 'user'], function () {
        Route::resource( 'interests', 'UserInterestController' );
        Route::resource( 'educations', 'UserEducationController' );
        Route::resource( 'ielts', 'UserIeltsController' );
        Route::resource( 'videos', 'UserVideoController' );
        Route::resource( 'avater', 'UserImageController' );
        Route::resource( 'cover', 'UserCoverController' );
        Route::resource( 'experience', 'UserExperienceController' );
        Route::resource( 'licence', 'UserLicenceController' );
        Route::post('course/image', 'UserCourseController@image');
        Route::post('course/video', 'UserCourseController@video');
        Route::resource( 'course', 'UserCourseController' );
        Route::resource( 'institute-author', 'InstituteAuthorController' );

        Route::post( 'upgrade-account', 'UpgradeController@upgrade');
    } );

    Route::group( ['prefix' => 'chat'], function () {
        Route::post( 'friend/send-message', 'ChattingController@sendMessage' );
        Route::post( 'friend/block-message', 'ChattingController@blockUnblock' );
        Route::get( 'friend/message/history/{id}', 'ChattingController@history' );
        Route::get( 'friend/list/recent', 'ChattingController@chatFriendLists' );
    } );

    Route::get('valid/tweet/account/{name}' , 'Twitter\TweetController@isValid');
    Route::get('get/pending/' , 'Twitter\TweetController@check_pending');
    Route::post('send/pending/' , 'Twitter\TweetController@send_pending');
    Route::post('pending/social' , 'Twitter\TweetController@pending');


    Route::get('social-dashboard-data', 'Twitter\TweetController@socialDashboardData');
    Route::post('social-search', 'Twitter\TweetController@socialSearch');
    Route::get('social-analysis-data', 'Twitter\TweetController@socialAnalysisData');
    Route::get('social-connector-topics', 'Twitter\TweetController@searchTopics');
    Route::post('social-connector-topic-update', 'Twitter\TweetController@updateTopics');
    Route::get('social-connector-topic-data', 'Twitter\TweetController@socialConnectorsData');

    Route::group( ['namespace' => 'Find', 'prefix' => 'finds'], function () {
        // Route::get( 'student', 'FindMongoController@findStudent' );
        // Route::get( 'tutor', 'FindMongoController@findTutor' );
        // Route::get( 'institute', 'FindMongoController@findInstitute' );
        // Route::get( '/mutuals', 'FindMongoController@mutuals' );

        // Route::get( 'student', 'FindController@findStudent' );
        // Route::get( 'tutor', 'FindController@findTutor' );
        // Route::get( 'institute', 'FindController@findInstitute' );
        // Route::get( '/mutuals', 'FindController@mutuals' );

        Route::get( 'student', 'FindMongoSqlController@findStudent' );
        Route::get( 'random-student', 'FindMongoSqlController@findRandomStudents' );
        Route::get( 'tutor', 'FindMongoSqlController@findTutor' );
        Route::get( 'institute', 'FindMongoSqlController@findInstitute' );
        Route::get( '/mutuals', 'FindMongoSqlController@mutuals' );
        Route::get( 'online-urls', 'FindMongoSqlController@allOnlineCourseUrls' );
        Route::get( 'find-course', 'FindMongoSqlController@findCourseById' );
        Route::get( 'online-courses', 'FindMongoSqlController@findOnlineCourses' );

        Route::post( 'invite', 'InvitationController@sendInvitation' );
        Route::get( 'invitation/pending-request-lists', 'InvitationController@pendingRequestList' );
        Route::get( 'invitation/pending-lists', 'InvitationController@invitationPendingList' );
        Route::get( 'invitation/pending-single', 'InvitationController@invitationPendingSingle' );
        Route::patch( 'invitation/accept', 'InvitationController@acceptRequest' );
        Route::patch( 'invitation/accept/all', 'InvitationController@acceptAllRequest' );
        Route::patch( 'invitation/ignore', 'InvitationController@ignoreRequest' );
        Route::patch( 'invitation/ignore/all', 'InvitationController@ignoreAllRequest' );
        Route::patch( 'invitation/cancel', 'InvitationController@cancelRequest' );
        Route::patch( 'invitation/cancel/all', 'InvitationController@cancelAllRequest' );

        Route::get( 'friend/count', 'FriendController@count' );
        Route::get( 'friend/list', 'FriendController@index' );
        Route::get( 'friend/my-friends', 'FriendController@myFriends' );

        //atik
        Route::get( 'friend/unfriend/{friend_id}', 'FriendController@unfriend' );
        //Route::get( '/mutual/{id}', 'InstituteController@fetchMutuals' );
        //search  the program, course, student
        //Route::get( 'institute/programs', 'InstituteController@findPrograms' );
        //Route::get( 'tutor/courses', 'TutorController@findCourses' );
        //Route::get( 'tutor/courses/student', 'TutorController@findCoursesForStudent' );
        //Route::get( 'students', 'StudentController@findStudents' );
        //Route::get( 'students/for-student', 'StudentController@findStudentsForStudent' );

        //Send Invitation
        //Route::post( 'student/invite', 'StudentController@sendStudentInvitation' );
        //Route::post( 'tutor/course/invite', 'TutorController@sendCourseInvitation' );
        //Route::post( 'institute/program/invite', 'InstituteController@sendProgramInvitation' );

        //Route::post( 'student/invite/emailinstitute/', 'StudentController@sendEmailInvitation' );
        //Route::post( 'tutor/course/invite/email', 'TutorController@sendEmailInvitation' );
        //Route::post( 'institute/program/invite/email', 'InstituteController@sendEmailInvitation' );

        // Route::get( 'invitation/pending-lists/program', 'InvitationController@invitationPendingProgramList' );
        // Route::get( 'invitation/pending-lists/program/first', 'InvitationController@invitationPendingProgramFirst' );
        // Route::get( 'invitation/pending-lists/student', 'InvitationController@invitationPendingStudentList' );
        // Route::get( 'invitation/pending-lists/student/first', 'InvitationController@invitationPendingStudentFirst' );
        // Route::get( 'invitation/pending-lists/course', 'InvitationController@invitationPendingCourseList' );
        // Route::get( 'invitation/pending-lists/course/first', 'InvitationController@invitationPendingCourseFirst' );

    } );

    Route::post( 'media/video', 'MediaController@uploadVideo' );
    Route::post( 'posts/video', 'PostController@videoPost' );
    Route::post( 'posts/image', 'PostController@imagePost' );
    Route::get( 'posts/count', 'PostController@countPost' );
    Route::get( 'posts/mine', 'PostController@myPost' );
    Route::resource( 'posts', 'PostController' )->except( ['create', 'edit'] );
    Route::resource( 'comments', 'CommentController' )->only( ['store', 'update', 'delete'] );
    Route::patch( 'notifications/all/read', 'NotificationController@markAllAsRead' );
    Route::patch( 'notifications/{id}/read', 'NotificationController@markAsRead' );
    Route::patch( 'notifications/{id}/unread', 'NotificationController@markAsUnRead' );
    Route::get( 'notifications/read', 'NotificationController@readNotification' );
    Route::get( 'notifications/unread', 'NotificationController@unreadNotification' );
    Route::get( 'notifications/count', 'NotificationController@unreadNotificationCount' );
    Route::get( 'notifications', 'NotificationController@index' );

} );


