const sidebarMenu =
    [
        {
            groups: "Profile",
            role_id: ['*'],
            route_name: 'profile',
            className: 'fa fa-fw fa-user fa-lg',
            menus: [
                {
                    title: "Basic Information",
                    role_id: [1, 2, 4, 5],
                    route_name: 'User Personal  Info',
                },
                {
                    title: "Basic Information",
                    role_id: [3],
                    route_name: 'Institute Basic Information',
                },
                {
                    title: "Contact Details",
                    role_id: [1, 2, 4, 5],
                    route_name: 'User Connect',
                },
                {
                    title: "Contact Details",
                    role_id: [3],
                    route_name: 'Institute Branches',
                },
                {
                    title: "Financial Details",
                    role_id: [3],
                    route_name: 'Financial Details',
                },
                {
                    title: "Education",
                    role_id: [2, 4],
                    route_name: 'User Education',
                },
                {
                    title: "Ielts/Tofel Scores",
                    role_id: [2],
                    route_name: 'User Ielts Tofel',
                },
                {
                    title: "Interests",
                    role_id: [2],
                    route_name: 'User Interest',
                },
                {
                    title: "License",
                    role_id: [4],
                    route_name: 'User Licence',
                },
                {
                    title: "Work Experience",
                    role_id: [4],
                    route_name: 'User Experience',
                },
                {
                    title: "Gallery",
                    role_id: ['*'],
                    route_name: 'User Video Block',
                },
            ],
        },
        {
            groups: "Upgrade to premium",
            role_id: [3, 4],
            className: 'fa fa-address-book fa-lg',
            route_name: 'Upgrade to premium',
        },
        {
            groups: "Offer Courses",
            role_id: [3, 4],
            className: 'fa fa-graduation-cap fa-lg',
            route_name: 'Offer Courses',
        },
        {
            groups: "Connections",
            role_id: ['*'],
            className: 'fa fa-address-book  fa-lg',
            route_name: 'User Connect',
            menus: [
                /*{
                    title: "Find Mutual Friend",
                    role_id: [2,3,4],
                    route_name: 'Find Mutual Friend',
                },*/

                /*Student Role*/
                {
                    title: "Find Institute",
                    role_id: [2],
                    route_name: 'Find Institute',
                },
                /*{
                    title: "Find Tutors",
                    role_id: [2],
                    route_name: 'Find Tutors',
                },*/
                {
                    title: "Find Others Student",
                    role_id: [2],
                    route_name: 'Find Student',
                },
                {
                    title: "Find Mutual Friend",
                    role_id: [2],
                    route_name: 'Find Mutual Friend',
                },

                /*School Role*/
                {
                    title: "Find Student",
                    role_id: [3],
                    route_name: 'Find Student',
                },/*  {
                    title: "Find Tutors",
                    role_id: [3],
                    route_name: 'Find Tutors',
                }, */
                {
                    title: "Find Others Schools",
                    role_id: [3],
                    route_name: 'Find Institute',
                },


                /*Teacher Role*/
                {
                    title: "Find Student",
                    role_id: [4],
                    route_name: 'Find Student',
                },
                {
                    title: "Find Others Teachers",
                    role_id: [4],
                    route_name: 'Find Tutors',
                },
                {
                    title: "Find Schools",
                    role_id: [4],
                    route_name: 'Find Institute',
                },

                /*All Role*/
                {
                    title: "Explore Social Media",
                    role_id: [2, 4],
                    route_name: 'Social Media Link',
                },
                {
                    title: "Invitations",
                    role_id: ['*'],
                    route_name: 'User Invitations List',
                },
                {
                    title: "Pending Requests",
                    role_id: ['*'],
                    route_name: 'Pending Requests List',
                }

            ]
        },
        {
            groups: "Chat",
            role_id: ['*'],
            className: 'fa fa-comment fa-lg',
            route_name: 'Chatting Page',
        },
        // {
        //     groups: "Social Media",
        //     role_id: [3],
        //     className: 'fa fa-share-square-o fa-lg',
        //     route_name: 'Social Media Dashboard',
        // },
        {
            groups: "Social Media",
            role_id: [3],
            className: 'fa fa-share-square-o fa-lg',
            route_name: 'Social Media Dashboard',
            menus: [
                {
                    title: "Dashboard",
                    role_id: [3],
                    route_name: 'Social Media Dashboard',
                },
                {
                    title: "Connector",
                    role_id: [3],
                    route_name: 'Social Media Connector',
                },
                {
                    title: "Analysis",
                    role_id: [3],
                    route_name: 'Social Media Analysis',
                },
            ]
        },
        {
            groups: "Notifications",
            role_id: ['*'],
            className: 'fas fa-bell',
            route_name: 'User Notification Page',
        },
        {
            groups: "Education Programs",
            role_id: [3],
            className: 'fa fa-graduation-cap fa-lg',
            route_name: 'Education programs',
            menus: [
                {
                    title: "Offer a programs",
                    role_id: [3],
                    route_name: 'Offer a programs',
                },
            ]
        },
        {
            groups: "Account Requests",
            role_id: [3],
            route_name: 'Account Requests',
        },
        {
            groups: "Login and Security",
            role_id: ['*'],
            className: 'fa fa-lock',
            route_name: 'Login and Security',
            menus: [
                {
                    title: "Change Password",
                    role_id: ['*'],
                    route_name: 'Change Password',
                },
            ]
        }
    ];

//If premium user then hide Upgrade to premium button
if (auth.premium_user==1) {
  sidebarMenu.splice(1 , 1);
}
export default sidebarMenu;
