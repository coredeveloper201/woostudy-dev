import api from "../../config/api";

const state = {
    institutes: []
};

const getters = {
    institutes: state => state.institutes,
    institute: (state, rootgetters) => state.institutes.filter(value => {
        value.usr_id = rootgetters.authUser.id
    })
};

const mutations = {
    setInstituteMutation(state, payleoad) {
        return state.institutes = payleoad;
    },
    addNewInstituteMutation(state, payleoad) {
        Vue.set(state.institutes, state.institutes.length, payleoad)
    },
    updateInstituteMutation(state, payleoad) {
        state.institutes.forEach((value, key) => {
            if (value.id === payleoad.id) {
                Vue.set(state.institutes, key, payleoad);
                return false
            }

        });

    }
};

const actions = {
    async setInstituteAction({commit}) {
        commit('setInstituteMutation',
            await axios.get(api.institute).then(response => {
                return response.data;
            }))
    },
    async addNewInstituteAction({commit, dispatch}, payload) {
        if (payload.id) {
            dispatch('updateInstituteAction', payload);
        }
        else {
            commit('addNewInstituteMutation',
                await axios.post(api.institute, payload).then(response => {
                    return response.data;
                }))
        }

    },
    async updateInstituteAction({commit}, payload) {
        commit('updateInstituteMutation',
            await axios.patch(api.institute + '/' + payload.id, payload).then(response => {
                return response.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}