import api from "../../config/api";

const state = {
    instituteRoles: {}
};

const getters = {
    instituteRoles(state) {
        return state.instituteRoles;
    },
    instituteRole(state) {
        return id => state.instituteRoles.filter(value => {
            return value.id === id
        });

    }
};

const mutations = {
    setInstituteRoleMutation(state, payleoad) {
        return state.instituteRoles = payleoad;
    },
    addNewInstituteRoleMutation(state, payleoad) {
        Vue.set(state.instituteRoles, state.instituteRoles.length, payleoad)
    }
};

const actions = {
    async setInstituteRoleAction({commit}) {
        commit('setInstituteRoleMutation',
            await axios.get(api.instituteRoles).then(response => {
                return response.data;
            }))
    },
    async addNewInstituteRoleAction({commit}, payload) {
        commit('addNewInstituteRoleMutation',
            await axios.post(api.instituteRoles, payload).then(response => {
                return response.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}