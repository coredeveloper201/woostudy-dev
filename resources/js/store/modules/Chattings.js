import api from "../../config/api";

const state = {
    chatFriends: []
};

const getters = {
    chatFriends: state => state.chatFriends
};

const mutations = {
    chatFriendLists(state, payleoad) {
        state.chatFriends = payleoad
    }
};

const actions = {
    async chatFriendLists({commit}) {
        commit('chatFriendLists',
            await axios.get(api.recentChatFriendList).then(response => {
                return response.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}