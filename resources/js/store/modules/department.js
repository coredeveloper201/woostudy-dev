import api from "../../config/api";

const state = {
    departments: []
};

const getters = {
    departments(state) {
        return state.departments;
    },
    department(state) {
        return id => state.departments.filter(value => {
            return value.id === id
        });
    }
};

const mutations = {
    setDepartmentMutation(state, payleoad) {
        return state.departments = payleoad;
    },
    addNewDepartmentMutation(state, payleoad) {
        Vue.set(state.departments, state.departments.length, payleoad)
    }
};

const actions = {
    async setDepartmentAction({commit}) {
        commit('setDepartmentMutation',
            await axios.get(api.department).then(response => {
                return response.data;
            }))
    },
    async addNewDepartmentAction({commit}, payload) {
        commit('addNewDepartmentMutation',
            await axios.post(api.department, payload).then(response => {
                return response.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}