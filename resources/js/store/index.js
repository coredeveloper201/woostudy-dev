import Vue from 'vue';
import Vuex from 'vuex';
import Profile from "./modules/Profile";
import Region from "./modules/Region";
import Country from "./modules/Country";
import institute from "./modules/institute";
import Department from "./modules/department";
import EducationLevel from "./modules/EducationLevel";
import IelstExamLevel from "./modules/IelstExamLevel";
import InstituteRole from "./modules/instituteRole";
import FriendList from "./modules/FriendList";
import Chattings from "./modules/Chattings";
import Notification from "./modules/Notification";


Vue.use(Vuex);

const index = new Vuex.Store({
    modules: {
        Profile,
        Region,
        Country,
        institute,
        EducationLevel,
        IelstExamLevel,
        Department,
        InstituteRole,
        FriendList,
        Chattings,
        Notification

    }
});

export default index;