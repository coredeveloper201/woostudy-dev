import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/home',
            component: r => require.ensure([], () => r(require('../pages/Timeline'))),
            name: 'User Timeline Page',
        },
        {
            path: '/my-timeline',
            component: r => require.ensure([], () => r(require('../pages/MyTimeline'))),
            name: 'My Timeline Page',
        },
        {
            path: '/my-connections',
            component: r => require.ensure([], () => r(require('../pages/MyConnections'))),
            name: 'My Connections Page',
        },
        {
            path: '/public/profile/:userName',
            component: r => require.ensure([], () => r(require('../pages/PublicProfile'))),
            name: 'userPublicProfile',
        },
        {
            path: '/',
            component: r => require.ensure([], () => r(require('../pages/LeftMenuPages'))),
            redirect: '/home',
            children: [
                {
                    path: 'profile/:userName',
                    component: r => require.ensure([], () => r(require('../pages/UserProfile'))),
                    children: [
                        {
                            path: '',
                            component: r => require.ensure([], () => r(require('../components/profile/PublicProfile'))),
                            name: 'User Profile'
                        },
                        {
                            path: 'personal-information',
                            component: r => require.ensure([], () => r(require('../components/profile/PersonalInformation'))),
                            name: 'User Personal  Info'
                        },
                        {
                            path: 'connects',
                            component: r => require.ensure([], () => r(require('../components/profile/ContactDetails'))),
                            name: 'User Connect'
                        }, {
                            path: 'education',
                            component: r => require.ensure([], () => r(require('../components/profile/EducationDetails'))),
                            name: 'User Education'
                        }, {
                            path: 'ielts-tofel',
                            component: r => require.ensure([], () => r(require('../components/profile/IeltsTofel'))),
                            name: 'User Ielts Tofel'
                        }, {
                            path: 'interests',
                            component: r => require.ensure([], () => r(require('../components/profile/Interest'))),
                            name: 'User Interest'
                        }, {
                            path: 'videos',
                            component: r => require.ensure([], () => r(require('../components/profile/VideoBlock'))),
                            name: 'User Video Block'
                        }, {
                            path: 'experiences',
                            component: r => require.ensure([], () => r(require('../components/profile/Experiences'))),
                            name: 'User Experience'
                        },
                        {
                            path: 'licence',
                            component: r => require.ensure([], () => r(require('../components/profile/Licences'))),
                            name: 'User Licence'
                        },
                        {
                            path: 'offer-course',
                            component: r => require.ensure([], () => r(require('../components/course/OfferCourse'))),
                            name: 'Offer Courses',
                        }, {
                            path: 'chats',
                            component: r => require.ensure([], () => r(require('../components/common/UserChattings'))),
                            name: 'User Chattings',
                        },
                        {
                            path: 'institute',
                            component: r => require.ensure([], () => r(require('../components/institute/Institute'))),
                            //name: "Institute",
                            children: [
                                {
                                    path: 'first-login',
                                    component: r => require.ensure([], () => r(require('../components/institute/FirstLogin'))),
                                    name: 'Institute Author User Details'
                                },
                                {
                                    path: 'basic-information',
                                    component: r => require.ensure([], () => r(require('../components/institute/BasicInformation'))),
                                    name: 'Institute Basic Information'
                                },
                                {
                                    path: 'branches',
                                    component: r => require.ensure([], () => r(require('../components/institute/Branches'))),
                                    name: 'Institute Branches'
                                },
                                {
                                    path: 'financial-info',
                                    component: r => require.ensure([], () => r(require('../components/institute/FinancialInformation'))),
                                    name: 'Financial Details'
                                },
                                {
                                    path: 'programs',
                                    component: r => require.ensure([], () => r(require('../components/institute/Programs'))),
                                    name: 'Offer a programs'
                                },
                                {
                                    path: 'account-requests',
                                    component: r => require.ensure([], () => r(require('../components/institute/SubAccountRequest'))),
                                    name: 'Account Requests'
                                }
                            ]
                        }
                    ],
                },
                {
                    path: 'automate-school',
                    component: r => require.ensure([], () => r(require('../components/automation/automateSchool'))),
                    children: [
                        {
                            path: 'on-boarding',
                            component: r => require.ensure([], () => r(require('../components/automation/onBoardignCriteria'))),
                            name: 'Boarding Criteria',
                        },
                        {
                            path: 'message-blast',
                            component: r => require.ensure([], () => r(require('../components/automation/messageBlast'))),
                            name: 'Message Blast',
                        },
                    ]
                },
                {
                path: 'connections',
                component: r => require.ensure([], () => r(require('../components/finds/Finds'))),
                children: [
                  {
                    path: '/',
                    component: r => require.ensure([], () => r(require('../components/finds/FindInvitation'))),
                    children: [
                      {
                        path: 'e-learning',
                        component: r => require.ensure([], () => r(require('../components/finds/FindELearning'))),
                        name: 'E-Learning'
                      },
                      {
                        path: 'e-learning-course-details/:courseId',
                        component: r => require.ensure([], () => r(require('../components/finds/CourseDetails'))),
                        name: 'E-Learning-Course-Details'
                      },
                      {
                        path: 'institute',
                        component: r => require.ensure([], () => r(require('../components/finds/FindInstitute'))),
                        name: 'Find Institute'
                      },
                      {
                        path: 'tutors',
                        component: r => require.ensure([], () => r(require('../components/finds/FindTutor'))),
                        name: 'Find Tutors'
                      },
                      {
                        path: 'mutual-friends',
                        component: r => require.ensure([], () => r(require('../components/finds/FindMutualFriend'))),
                        name: 'Find Mutual Friend'
                      },
                      {
                        path: 'find-others-student/',
                        component: r => require.ensure([], () => r(require('../components/finds/FindStudent'))),
                        name: 'Find Student'
                      },
                      {
                        path: 'find-others-student/:hash',
                        component: r => require.ensure([], () => r(require('../components/finds/FindStudent'))),
                        name: 'find-others-student'
                      }
                    ]
                  },
                  {
                    path: 'invitations-list',
                    component: r => require.ensure([], () => r(require('../components/finds/InvitationList'))),
                    name: 'User Invitations List'
                  },
                  {
                    path: 'pending-requests-list',
                    component: r => require.ensure([], () => r(require('../components/finds/PendingRequestList'))),
                    name: 'Pending Requests List'
                  },
                  {
                    path: 'social-media-link',
                    component: r => require.ensure([], () => r(require('../components/finds/common/SocialMediaLink'))),
                    name: 'Social Media Link'
                  },
                  {
                    path: 'friend-list',
                    component: r => require.ensure([], () => r(require('../components/finds/FriendList'))),
                    name: 'Friend List'
                  }
                ]
              },
                {
                    path: '/upgrade',
                    component: r => require.ensure([], () => r(require('../pages/Upgrade'))),
                    name: 'Upgrade to premium',
                },
                {
                    path: '/chatting',
                    component: r => require.ensure([], () => r(require('../pages/Chatting'))),
                    name: 'Chatting Page',
                },
                {
                    path: 'change-password',
                    component: r => require.ensure([], () => r(require('../components/security/ChangePassword'))),
                    name: 'Change Password',
                },
                {
                    path: 'notifications/',
                    component: r => require.ensure([], () => r(require('../pages/Notifications'))),
                    name: 'User Notification Page',
                },
                {
                    path: 'social-media/',
                    component: r => require.ensure([], () => r(require('../pages/SocialMediaDashboard'))),
                    name: 'Social Media Dashboard',
                },
                {
                    path: 'social-media-connector',
                    component: r => require.ensure([], () => r(require('../pages/SocialMediaConnector'))),
                    name: 'Social Media Connector'
                },
                {
                    path: 'social-media-analysis',
                    component: r => require.ensure([], () => r(require('../pages/SocialMediaAnalysis'))),
                    name: 'Social Media Analysis'
                },
                {
                    path: 'search/:key',
                    component: r => require.ensure([], () => r(require('../pages/Search'))),
                    name: 'Search'
                }

            ]

        },

    ],

});
