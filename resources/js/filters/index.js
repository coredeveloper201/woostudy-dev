import Slugify from './Slugify';
import strLimit from './StrLimit';
import wordWrap from './WordWrap';

export default {
    install(Vue) {
        Vue.filter('slugify', Slugify);
        Vue.filter('strLimit', strLimit);
        Vue.filter('wordWrap', wordWrap);
    }
}
