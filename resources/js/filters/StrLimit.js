export default function strLimit(input, limit = 15) {
    return input.length > limit ? input.substring(0, limit) + '...' : input;

};