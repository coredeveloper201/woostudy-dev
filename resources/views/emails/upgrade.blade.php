@if($receiver=='user')
{{ $user->name }},<br>You are successfully upgraded your account. Thank you for being with us.<br>
<a href="{{ env('APP_URL') }}/public/profile/{{ $user->user_name }}">Click here</a> to go back your profile<br>
or you can visit our website <a href="{{ env('APP_URL') }}">{{ env('APP_URL') }}</a>.<br><br>

Thank you<br>
{{ config('app.name', 'Laravel') }} Team

@elseif ($receiver=='admin')
User <strong>{{ $user->name }}</strong> upgraded his account.<br>
<a href="{{ env('APP_URL') }}/public/profile/{{ $user->user_name }}">Click here</a> to view profile of <strong>{{ $user->name }}</strong><br>
@endif
