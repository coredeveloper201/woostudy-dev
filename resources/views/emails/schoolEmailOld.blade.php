<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Email</title>
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat:400,700&display=swap" rel="stylesheet">
</head>
<style>
  /** {
    font-family: Lato, sans-serif;
    box-sizing: border-box;
    margin: 0;
    padding: 0
  }

  .email-body {
    width: 740px;
    display: table;
    margin: auto;
  }

  .email-body .e-logo {
    width: 100%;
    display: table;
    text-align: center;
    padding: 15px 0
  }

  .email-body .e-logo img {
    width: 150px
  }*/

  .email-body .e-back {

  }

  .email-body .e-back .mail-emg {

  }

  .email-body .e-back .mail-emg .l-text {

  }

  .email-body .e-back .mail-emg .l-text .text-bold {

  }

  .email-body .e-container {

  }

  .email-body .to-box {

  }

  .email-body .to-box .name {

  }

  .email-body .to-box .font-blue {

  }

  .email-body .to-box .des {

  }

  .email-body .left-sec {

  }

  .email-body .left-sec .school-detail {

  }

  .email-body .left-sec .school-detail .s-logo {

  }

  .email-body .left-sec .school-detail .s-name {

  }

  .email-body .left-sec .school-detail .s-location {

  }

  .email-body .left-sec .school-detail .s-location .s-l-logo {

  }

  .email-body .left-sec .school-detail .s-location .s-l-name {

  }

  .email-body .left-sec .school-detail .s-option {

  }

  .email-body .left-sec .school-detail .s-option .s-e-option a {

  }

  .email-body .left-sec .school-detail .s-option .s-e-option a.active {

  }

  .email-body .left-sec .school-detail .s-info {
    width: 100%;
    display: flex;
    padding: 5px 0 10px 0;
    align-items: center;
    font-size: 14px
  }

  .email-body .left-sec .school-detail .s-info .i-logo {
    color: #797979;
    margin-right: 7px
  }

  .email-body .left-sec .school-detail .s-info .mrc-5 {
    margin-right: 5px
  }

  .email-body .left-sec .school-detail .s-info .text-ash {
    color: #797979;
    font-weight: 700
  }

  .email-body .right-sec {
    width: 72%;
    float: left;
    padding: 0 5px 0 5px
  }

  .email-body .right-sec .people-connect-box {
    width: 100%;
    padding: 5px;
    border: 1px solid #1167a2;
    margin-bottom: 30px;
    text-align: center
  }

  .email-body .right-sec .people-connect-box .h-text {
    padding: 5px 0 0 0;
    font-weight: 700;
    color: #1167a2;
    font-size: 18px;
    text-align: center;
    margin-bottom: 20px;
    font-family: Montserrat, sans-serif
  }

  .email-body .right-sec .people-connect-box .pro-box {
    width: 95px;
    display: inline-block;
    padding: 2px;
    margin: 0 10px;
    text-align: center;
    margin-bottom: 50px
  }

  .email-body .right-sec .people-connect-box .pro-box .pro-pic {
    width: 90px;
    height: 90px;
    border-radius: 50%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    margin-bottom: 5px
  }

  .email-body .right-sec .people-connect-box .pro-box .name {
    padding: 8px 0;
    color: #797979
  }

  .email-body .right-sec .sch-text {
    text-align: center;
    color: #f17b16;
    font-weight: 700;
    font-family: Montserrat, sans-serif;
    margin-bottom: 20px
  }

  .email-body .right-sec .sch-abt-box {
    width: 100%;
    padding: 5px;
    border: 1px solid #f17b16;
    margin-bottom: 30px
  }

  .email-body .right-sec .h-text {
    color: #f17b16;
    font-weight: bold;
    padding: 10px 0
  }

  .email-body .right-sec .divider {
    width: 100%;
    height: 1px;
    display: table;
    background-color: #f17b16
  }

  .email-body .right-sec .detail-text {
    color: #797979;
    padding: 10px 0
  }

  .email-body .footer-box {
    width: 100%;
    background-color: #fafafa;
    padding: 10px;
    text-align: center
  }

  .email-body .footer-box .text-1 {
    color: #797979;
    padding: 10px 0;
    font-weight: 500
  }

  .btn {
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
  }

  @media (prefers-reduced-motion: reduce) {
    .btn {
      transition: none
    }
  }

  .btn:hover {
    color: #212529;
    text-decoration: none
  }

  .btn.focus, .btn:focus {
    outline: 0;
    box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25)
  }

  .btn.disabled, .btn:disabled {
    opacity: .65
  }

  a.btn.disabled, fieldset:disabled a.btn {
    pointer-events: none
  }

  .b-r-0 {
    border-radius: 0
  }

  .btn-connect {
    background-color: #1167a2;
    color: #fff;
    text-decoration: none
  }

  .btn-connect:hover {
    color: #fff !important
  }



</style>
<body style=" font-family: Lato, sans-serif;
    box-sizing: border-box;
    margin: 0;
    padding: 0">
<div style=" width: 740px;
    display: table;
    margin: auto;">
  <div style=" width: 100%;
    display: table;
    text-align: center;
    padding: 15px 0">
    <img style=" width: 150px" src="{{ asset( $school->avater ) }}" alt="{{ $school->institute->title }}">
  </div>
  <div style=" width: 100%;
    display: block;
    background-color: #dae6ed;
    height: 160px">
    <div style="width: 700px;
    margin: auto;
    display: table">
      <div  style="background-image: url('{{ asset($school->cover)}}');  margin: 20px 0 0 0;
          width: 100%;
          background-repeat: no-repeat;
          background-position: center;
          background-size: cover;
          height: 160px" alt="{{ $school->institute->title }}">
        <div style=" padding: 50px 100px;
    color: #fff;
    text-align: right">
          LEARN FROM
          <br>
          <span style="font-weight: 700">ANYWHERE</span>
        </div>
      </div>
    </div>
  </div>
  <div  style="width: 700px;
    margin: auto;
    display: table">
    <div style=" width: 100%;
    padding: 20px 0 10px 0">
      <div style=" font-weight: 700;
    color: #1167a2;
    padding: 10px 0 5px 0">Hi {{ $school->institute->title }}</div>
      <div style="  color: #797979;
    margin-bottom: 20px">
        More than <span style="color: #1167a2">50 students</span> shown below are looking for admissions in your school
      </div>
    </div>
    <div style=" width: 25%;
    float: left;
    padding: 0 5px 0 5px">
      <div style="width: 100%;
    display: table;
    border: 1px solid #dbdbdb;
    background-color: #fcfcfc;
    padding: 4px;
    position: relative;
    z-index: 1;
    text-align: center">
        <div  style="background-image: url('{{ asset('web/img/school_profile/school.png')}}'); width: 70px;
            height: 70px;
            border-radius: 50%;
            border: 3px solid #fff;
            text-align: center;
            display: inline-block;
            background-color: #f17b16;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover"></div>
        <div style=" padding-top: 10px;
    color: #1167a2;
    font-weight: 700;
    text-align: center;
    padding-bottom: 5px">{{ $school->institute->title }}</div>
        <div style="  display: flex;
    justify-content: center;
    padding-bottom: 8px">
          <img style=" width: 18px;
    margin-right: 3px" src="{{ asset('web/img/school_profile/s-location.gif') }}" alt="">
          <div style="color: #797979">{{ $school->schoolAddress->first()->city }}, {{ $school->schoolAddress->first()->country ? $school->schoolAddress->first()->country->title : '' }}</div>
        </div>
        <ul style=" padding: 0;
    list-style: none;
    margin: 0;
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    padding-bottom: 10px">
          <li class="s-e-option">
            <a href="#" style=" text-decoration: none;
    color: #797979;
    font-size: 13px"><img src="{{ asset('web/img/school_profile/e-hat.png') }}" alt=""></a>
          </li>
          <li class="s-e-option">
            <a href="#">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-brif.png') }}" alt=""></a>
            </a>
          </li>
          <li class="s-e-option">
            <a href="#" style=" text-decoration: none;
    color: #797979;
    font-size: 13px">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-doller.png') }}" alt=""></a>
            </a>
          </li>
          <li class="s-e-option">
            <a href="#" style=" text-decoration: none;
    color: #797979;
    font-size: 13px">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-email.png') }}" alt=""></a>
            </a>
          </li>
          <li class="s-e-option">
            <a href="#" style=" text-decoration: none;
    color: #797979;
    font-size: 13px">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-home.png') }}" alt=""></a>
            </a>
          </li>
        </ul>
        <div class="divider"></div>
        <div class="s-info">
          <img class="i-logo" src="{{ asset('web/img/school_profile/e-cer.png') }}" alt="">
          <span class="mrc-5">Founded</span> <span
            class="text-ash"> {{ $school->institute->founded }}</span>
        </div>
        <div class="s-info">
          <img class="i-logo" src="{{ asset('web/img/school_profile/e-uni.png') }}" alt=""><span
            class="mrc-5">Type</span> <span
            class="text-ash"> {{ $school->institute->education_level ? $school->institute->education_level->title : '' }}</span>
        </div>
        <div class="s-info">
          <img class="i-logo" src="{{ asset('web/img/school_profile/e-grp.png') }}" alt=""><span class="mrc-5">Total Students</span>
          <span
            class="text-ash">{{ $school->institute->total_students > 1000 ? $school->institute->total_students/1000 . 'k+' : $school->institute->total_students }}</span>
        </div>
      </div>
    </div>
    <div class="right-sec">
      <div class="people-connect-box">
        <div class="h-text">Students who wants to connect with you</div>
        @foreach ($students as $student)
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset( $student->avater ) }}')"></div>
            <div class="name">{{ $student->name }}</div>
            <a href="{{ env('APP_URL') }}/profile/{{ $student->user_name }}" class="btn btn-connect b-r-0">Connect</a>
          </div>
        @endforeach
      </div>
      <div class="sch-text">Your School Profile</div>
      <div class="sch-abt-box">
        <div class="h-text">About</div>
        <div class="divider"></div>
        <div class="detail-text">
          {!! $school->institute->about !!}
        </div>
      </div>
    </div>
  </div>
  <div class="footer-box">
    <div class="text-1">Download our mobile app on</div>

    <a href="#" style="width: 100px; display: inline-block;">
      <img style="width: 90px" src="{{ asset('web/img/school_profile/apple.png')}}" alt="">
    </a>

    <a href="#" style="width: 100px; display: inline-block;">
      <img style="width: 90px" src="{{ asset('web/img/school_profile/google.png')}}" alt="">
    </a>

    <div class="text-1" style="font-size: 13px">
      © 2020 WooStudy Inc. All rights reserved. WooStudy and the WooStudy logo are registered trademarks of WooStudy,
      Inc. All other trademarks are property of their respective owners. View our Privacy Notice.
    </div>
  </div>
</div>

</body>
</html>
