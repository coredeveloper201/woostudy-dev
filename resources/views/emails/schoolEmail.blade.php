<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="date=no" />
    <meta name="format-detection" content="address=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Raleway:400,400i,700,700i" rel="stylesheet" />
    <!--<![endif]-->
    <title>Email Template</title>
    <!--[if gte mso 9]>
    <style type="text/css" media="all">
        sup { font-size: 100% !important; }
    </style>
    <![endif]-->


    <style type="text/css" media="screen">
        /* Linked Styles */
        body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#333545; -webkit-text-size-adjust:none }
        a { color:#4e54cb; text-decoration:none }
        p { padding:0 !important; margin:0 !important }
        img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
        .mcnPreviewText { display: none !important; }
        .text-footer a { color: #7e7e7e !important; }
        .text-footer2 a { color: #c3c3c3 !important; }

        /* Mobile styles */
        @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
            .mobile-shell { width: 100% !important; min-width: 100% !important; }

            .m-center { text-align: center !important; }
            .m-left { margin-right: auto !important; }

            .center { margin: 0 auto !important; }

            .td { width: 100% !important; min-width: 100% !important; }

            .m-br-15 { height: 15px !important; }
            .m-separator { border-bottom: 1px solid #000000; }
            .small-separator { border-top: 1px solid #333333 !important; padding-bottom: 20px !important; }

            .m-td,
            .m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

            .m-block { display: block !important; }

          /*  .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }*/

            .content-middle { width: 140px !important; padding: 0px !important; }

            .text-white { font-size: 12px !important; }

            .h2-white { font-size: 46px !important; line-height: 50px !important; }
            .h3-white { font-size: 24px !important; line-height: 30px !important; }

            .mpb15 { padding-bottom: 15px; }
            .content { padding: 20px 15px !important; }

            .section-inner { padding: 0px !important; }

            .content-2 { padding: 30px 15px 30px 15px !important; }
            .pt30 { padding-top: 20px !important; }
            .main { padding: 0px !important; }
            .section { padding: 30px 15px 30px 15px !important; }
            .section2 { padding: 0px 15px 30px 15px !important; }
            .section4 { padding: 30px 15px !important; }
            .section-inner2 { padding: 30px 15px !important; }

            .separator-outer { padding-bottom: 30px !important; }
            .section3 { padding: 30px 15px !important; }
            .mpb10 { padding-bottom: 10px !important; padding-top: 5px !important; }
            .preheader { padding-bottom: 20px !important; }

            .column,
            .column-dir,
            .column-top,
            .column-empty,
            .column-empty2,
            .column-bottom,
            .column-dir-top,
            .column-dir-bottom { float: left !important; width: 100% !important; display: block !important; }
            .column-empty { padding-bottom: 30px !important; }
            .column-empty2 { padding-bottom: 10px !important; }
            .content-spacing { width: 15px !important; }
        }
        .school-detail {
            border: 1px solid #dbdbdb;
            background-color: #fcfcfc;
            padding: 4px;
            position: relative;
            z-index: 1;
            text-align: center;
        }
        .school-detail .s-logo {
            width: 70px;
            height: 70px;
            border-radius: 50%;
            border: 3px solid #fff;
            text-align: center;
            display: inline-block;
            background-color: #f17b16;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        .school-detail .s-name {
            padding-top: 10px;
            color: #1167a2;
            font-weight: 700;
            text-align: center;
            padding-bottom: 5px;
        }
        .school-detail .s-location {
            display: table;
            padding-bottom: 8px;
            align-items: center;
        }
        .school-detail .s-location .s-l-logo {
            width: 18px;
            margin-right: 3px;
            float: left;
            display: table;
            height: 18px;
        }
        .school-detail .s-location .s-l-name {
            color: #797979;
            font-size: 14px;
            float: left;
            margin-top: 9px;
            display: table;
        }
        .school-detail .s-option {
            padding: 0;
            list-style: none;
            margin: 0;
            width: 100%;
            display: table;
            padding-bottom: 10px;
            margin-left: 7px;
        }
        .school-detail .s-option .s-e-option a {
            text-decoration: none;
            color: #797979;
            font-size: 13px;
            float: left;
            margin-right: 4px;
            display: table;
        }
        .school-detail .s-option .s-e-option a img{
            width: 16px;
            display: table;
            height: 12px;
            margin-right: 5px;
        }
        .school-detail .s-option .s-e-option a.active {
            color: #57bd66;
        }
        .school-detail .s-info {
            padding: 5px 0 10px 5px;
            display: table;
            font-size: 14px;
        }
        .school-detail .s-info .i-logo {
            color: #797979;
            margin-right: 7px;
            width: 16px;
            display: table;
            height: 16px;
            float: left;
        }
        .school-detail .s-info .mrc-5 {
            margin-right: 5px;
            display: table;
            float: left;
            padding-top: 9px;
        }
        .school-detail .s-info .text-ash {
            color: #797979;
            font-weight: 700;
            display: table;
            float: left;
            padding-top: 9px;
        }
        .people-connect-box {
            padding: 5px;
            border: 1px solid #1167a2;
            margin-bottom: 30px;
            text-align: center;
        }
        .people-connect-box .h-text {
            padding: 5px 0 0 0;
            font-weight: 700;
            color: #1167a2;
            font-size: 18px;
            text-align: center;
            margin-bottom: 20px;
            font-family: Montserrat, sans-serif;
        }
        .people-connect-box .pro-box {
            width: 95px;
            display: inline-block;
            padding: 2px;
            margin: 0 10px;
            text-align: center;
            margin-bottom: 25px;
        }
        .people-connect-box .pro-box .pro-pic {
            width: 90px;
            height: 90px;
            border-radius: 50%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            margin-bottom: 5px;
        }
        .people-connect-box .pro-box .name {
            padding: 8px 0;
            color: #797979;
            font-size: 12px;
            line-height: 13px;
        }
        .sch-text {
            text-align: center;
            color: #f17b16;
            font-weight: 700;
            font-family: Montserrat, sans-serif;
            margin-bottom: 20px;
        }
        .sch-abt-box {
            display: table;
            padding: 9px;
            border: 1px solid #f17b16;
            margin-bottom: 30px;
        }
        .h-text {
            color: #f17b16;
            font-weight: bold;
            padding: 10px 0;
            font-size: 16px;
        }
        .divider {
            width: 100%;
            height: 1px;
            display: table;
            background-color: #f17b16;
            margin: 5px 0;
        }
        .detail-text {
            color: #797979;
            padding: 10px 0;
            font-size: 14px;
        }
        .btn {
            background-color: #1167A2;
            color: #ffffff;
            text-decoration: none;
            display: inline-block;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .365rem .55rem;
            font-size: .9rem;
            line-height: 1.5;
            border-radius: .25rem;
            transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
        }
        .e-logo {
            width: 100%;
            display: table;
            text-align: center;
            padding: 15px 0;
        }
        .e-logo img {
            width: 150px;
            display: table;
            margin: auto;
        }
        .e-back {
            width: 100%;
            display: block;
            background-color: #dae6ed;
            height: 160px;
        }
        .e-back .mail-emg {
            margin: 20px 0 0 0;
            width: 100%;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            height: 160px;
        }
        .e-back .mail-emg .l-text {
            padding: 50px 100px;
            color: #fff;
            text-align: right;
        }
        .e-back .mail-emg .l-text .text-bold {
            font-weight: 700;
        }
        .e-container {
            width: 700px;
            margin: auto;
            display: table;
        }
        .to-box {
            padding: 20px 0 10px 0;
        }
        .to-box .name {
            font-weight: 700;
            color: #1167a2;
            padding: 10px 0 5px 0;
            text-align: left;
        }
        .to-box .font-blue {
            color: #1167a2;
        }
        .to-box .des {
            color: #797979;
            margin-bottom: 20px;
            font-size: 13px;
            text-align: left;
        }
        .footer-box {
            width: 100%;
            background-color: #fafafa;
            padding: 10px;
            text-align: center;
        }
        .footer-box .text-1 {
            color: #797979;
            padding: 10px 0;
            font-weight: 500;
            line-height: 15px;
        }
    </style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#333545; -webkit-text-size-adjust:none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333545">
    <tr>
        <td align="center" valign="top">
            <!-- Main -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" class="main" style="padding:0px 0px 20px 0px;">
                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                            <tr>
                                <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <!-- Two Columns -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                        <tr>
                                            <td class="p30-15" style="padding: 50px 30px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="section-title" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:14px; line-height:20px; text-align:center; padding-bottom:40px; text-transform:uppercase;">
                                                            <div class="e-logo">
                                                                <img src="{{ asset('/web/img/logo.png') }}" alt="WooStudy">
                                                            </div>
                                                            <div class="e-back">
                                                                <div class="e-container">
                                                                    <div class="mail-emg" style="background-image: url('{{ asset($school->cover)}}')" alt="{{ $school->institute->title }}">
                                                                        <div class="l-text">
                                                                            LEARN FROM
                                                                            <br>
                                                                            <span class="text-bold">ANYWHERE</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="to-box">
                                                                <div class="name">Hi {{ $school->institute->title }}</div>
                                                                <div class="des">
                                                                    More than <span class="font-blue">50 students</span> shown below are looking for admissions in your school
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="section-inner">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th class="column-top" width="160" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="fluid-img pb20" style="font-size:0pt; line-height:0pt; text-align:left; padding-bottom:20px;">
                                                                                    <div class="school-detail">
                                                                                        <div class="s-logo" style="background-image: url('{{ asset( $school->avater ) }}')"></div>
                                                                                        <div class="s-name">{{ $school->institute->title }}</div>
                                                                                        <div class="s-location">
                                                                                            <img class="s-l-logo" src="{{ asset('web/img/school_profile/s-location.gif') }}" alt="">
                                                                                            <div class="s-l-name">{{ $school->schoolAddress->first()->city }}, {{ $school->schoolAddress->first()->country ? $school->schoolAddress->first()->country->title : '' }}</div>
                                                                                        </div>
                                                                                        <ul class="s-option">
                                                                                            <li class="s-e-option">
                                                                                                <a href="#"><img src="{{ asset('web/img/school_profile/e-hat.png') }}" alt=""></a>
                                                                                            </li>
                                                                                            <li class="s-e-option">
                                                                                                <a href="#">
                                                                                                    <a href="#"><img src="{{ asset('web/img/school_profile/e-brif.png') }}" alt=""></a>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="s-e-option">
                                                                                                <a href="#" class="">
                                                                                                    <a href="#"><img src="{{ asset('web/img/school_profile/e-doller.png') }}" alt=""></a>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="s-e-option">
                                                                                                <a href="#">
                                                                                                    <a href="#"><img src="{{ asset('web/img/school_profile/e-email.png') }}" alt=""></a>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="s-e-option">
                                                                                                <a href="#" class="">
                                                                                                    <a href="#"><img src="{{ asset('web/img/school_profile/e-home.png') }}" alt=""></a>
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                        <div class="divider"></div>
                                                                                        <div class="s-info">
                                                                                            <img class="i-logo" src="{{ asset('web/img/school_profile/e-cer.png') }}" alt="">
                                                                                            <span class="mrc-5">Founded</span> <span
                                                                                                class="text-ash"> {{ $school->institute->founded }}</span>
                                                                                        </div>
                                                                                        <div class="s-info">
                                                                                            <img class="i-logo" src="{{ asset('web/img/school_profile/e-uni.png') }}" alt=""><span
                                                                                                class="mrc-5">Type</span> <span
                                                                                                class="text-ash"> {{ $school->institute->education_level ? $school->institute->education_level->title : '' }}</span>
                                                                                        </div>
                                                                                        <div class="s-info">
                                                                                            <img class="i-logo" src="{{ asset('web/img/school_profile/e-grp.png') }}" alt=""><span class="mrc-5">Total Students</span>
                                                                                            <span
                                                                                                class="text-ash">{{ $school->institute->total_students > 1000 ? $school->institute->total_students/1000 . 'k+' : $school->institute->total_students }}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>

                                                                        </table>
                                                                    </th>
                                                                    <th class="column-empty" width="20" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
                                                                    <th class="column-top"  style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="text-title pb10" style="color:#000000; font-family:'Noto Serif', Georgia, serif; font-size:22px; line-height:32px; text-align:left; padding-bottom:10px;">
                                                                                                <div class="people-connect-box">
                                                                                                    <div class="h-text">Students who wants to connect with you</div>
                                                                                                    @foreach ($students as $student)
                                                                                                        <div class="pro-box">
                                                                                                            <div class="pro-pic" style="background-image: url('{{ asset( $student->avater ) }}')"></div>
                                                                                                            <div class="name">{{ $student->name }}</div>
                                                                                                            <a href="{{ url('/public/profile/'.$school->user_name) }}" class="btn btn-connect b-r-0">Connect</a>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                                <div class="sch-text">Your School Profile</div>
                                                                                                <div class="sch-abt-box">
                                                                                                    <div class="h-text">About</div>
                                                                                                    <div class="divider"></div>
                                                                                                    <div class="detail-text">
                                                                                                        {!! $school->institute->about !!}
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>

                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                            <table>
                                                                <tr>
                                                                    <th class="column-top">
                                                                        <div class="footer-box">
                                                                            <div class="text-1">Download our mobile app on</div>

                                                                            <a href="#" style="width: 100px; display: inline-block;">
                                                                                <img style="width: 90px" src="{{ asset('web/img/school_profile/apple.png')}}" alt="">
                                                                            </a>

                                                                            <a href="#" style="width: 100px; display: inline-block;">
                                                                                <img style="width: 90px" src="{{ asset('web/img/school_profile/google.png')}}" alt="">
                                                                            </a>

                                                                            <div class="text-1" style="font-size: 13px">
                                                                                © 2020 WooStudy Inc. All rights reserved. WooStudy and the WooStudy logo are registered trademarks of WooStudy,
                                                                                Inc. All other trademarks are property of their respective owners. View our Privacy Notice.
                                                                            </div>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END Two Columns -->

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- END Main -->
        </td>
    </tr>
</table>
</body>
</html>


