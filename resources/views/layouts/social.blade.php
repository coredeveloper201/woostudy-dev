<div class="social">
 {{-- <h4 class="form-group text-center"> Social Login </h4>--}}
    <div class="form-group">
        <!--Google +-->
        <a type="button" onclick="ShowPopup('{{url('login/google')}}')" class="btn btn-gplus">
            <img width="25px" src="{{asset('images/google-icon.png')}}" alt="">&nbsp;Login with Google
        </a>
    </div>
  <div class="form-group">
      <!--Facebook-->
      <a type="button" onclick="ShowPopup('{{url('login/facebook')}}')" class="btn btn-fb">
          <i class="fab fa-facebook-f"></i>&nbsp;&nbsp;&nbsp;Login with Facebook
      </a>
  </div>

  <div class="form-group">
      <!--Twitter-->
      <a type="button" onclick="ShowPopup('{{url('login/twitter')}}')" class="btn btn-tw">
          <i class="fab fa-twitter"></i>&nbsp;&nbsp;Login with Twitter
      </a>
  </div>


</div>
