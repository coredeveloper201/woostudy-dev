<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" />
    <link href="{{ asset('web/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('web/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('web/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('web/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('web/css/frontend-style.css') }}" rel="stylesheet">

</head>
<body data-app_domain={{request()->root()}}>
    <!--overlay-search-->
    <div id="myOverlay" class="overlay">
        <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
        <div class="overlay-content">
            <form action="">
                <input type="text" placeholder="Search.." name="search">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
    <!--overlay-search end-->

    <!--head-->
    <section class="header">
        <div class="container responsive-container">
            <div class="row">
                <div class="col-4 custom-col-4">
                    <div class="dropdown mt-1">
                        <div class="dropdown-toggle font-ash" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('web/img/world-grid.png') }}" class="globe c-pointer" alt="logo">
                            <span class="c-pointer lang-text">English</span>
                        </div>
                        {{-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Bangla</a>
                            <a class="dropdown-item" href="#">Arabic</a>
                        </div> --}}
                    </div>
                </div>
                <div class="col-8 custom-col-8">
                    <div class="login-info">
                        <span  style="display:none" class="c-pointer fa fa-search font-yellow  mr-3 hidden" onclick="openSearch()"></span>
                        <a href="{{ url('/login') }}" class="btn btn-login">Login</a>
                        <a href="{{ url('/register') }}" class="btn btn-started-head">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--head end-->

    <section class="divider"></section>
    <!--nav start-->
    <section class="navigation-bar">
        <div class="container">
            <nav class="navbar nav-padding navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{ url('/') }}"><img class="logo" src="{{ asset('web/img/logo.png') }}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav custom-navbar-nav ml-auto">
                        <li class="nav-item nav-item-margin active">
                            <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item nav-item-margin">
                            <a class="nav-link" href="{{ env('URL_ABOUT') }}">About us</a>
                        </li>
                        <li class="nav-item nav-item-margin">
                            <a class="nav-link" href="{{ env('URL_BLOG') }}">Blog</a>
                        </li>
                        <li class="nav-item nav-item-margin">
                            <a class="nav-link" href="{{ env('URL_CONTACT') }}">Contact us</a>
                        </li>
                        {{-- <li class="nav-item dropdown nav-item-margin">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span><i class="fa fa-server"></i></span> <span>Category</span>
                            </a>
                            <div class="dropdown-menu" style="display:none" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Category 1</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Category 2</a>
                            </div>
                        </li> --}}
                    </ul>
                </div>
            </nav>
        </div>
    </section>
    <!--nav end-->

    <!--main-slide-->
    <section class="slider">
        <div class="owl-carousel owl-theme">
            <div class="slide-item" style="background-image: url('{{ asset('web/img/student-cover.jpg') }}')">
                <div class="container">
                    <div class="slide-text">
                        <div class="text-1 mb-3">Instantly Connect With Student</div>
                        <div class="text-2">Woo Study shall ensure you find the students you are looking for and they find
                            you too.
                        </div>
                        {{-- <div class="text-1 mb-3">Instantly Connect With Teachers and Student</div>
                        <div class="text-2">Woo Study shall ensure you find the students you are looking for and they find
                            you too.
                        </div> --}}
                        <a class="btn btn-started2" href="{{ url('/login') }}">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="slide-item" style="background-image: url('{{ asset('web/img/Institution-cover.jpg') }}')">
                <div class="container">
                    <div class="slide-text">
                        <div class="text-1 mb-3">Connect with potential students instantly</div>
                        <div class="text-2">Helps any academic institution to find admissions and offering online courses.
                        </div>
                        {{-- <div class="text-1 mb-3">Connect with potential students and teachers instantly</div>
                        <div class="text-2">Helps any academic institution to find admissions and offering online courses.
                        </div> --}}
                        <a class="btn btn-started2" href="{{ url('/login') }}">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="slide-item" style="background-image: url('{{ asset('web/img/teacher-cover.jpg') }}')">
                <div class="container">
                    <div class="slide-text">
                        <div class="text-1 mb-3">Connect with potential students instantly</div>
                        <div class="text-2">Helps any academic institution to find admissions and offering online courses.
                        </div>
                        {{-- <div class="text-1 mb-3">Connect with potential students and teachers instantly</div>
                        <div class="text-2">Helps any academic institution to find admissions and offering online courses.
                        </div> --}}
                        <a class="btn btn-started2" href="{{ url('/login') }}">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--main-slide end-->

    <!--feature-->
    <section class="feature">
        <div class="container mobile-hide">
            <div class="row">
                <div class="col-md-4">
                    <div class="set-middle">
                        <div class="f-logo">
                            <img width="45px" src="{{ asset('web/img/click.png') }}" alt="">
                        </div>
                        <div class="f-text">
                            <strong class="letter-spc-1">Best To</strong>
                            <div>Find Institution</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="set-middle">
                        <div class="f-logo">
                            <img width="45px" src="{{ asset('web/img/success.png') }}" alt="">
                        </div>
                        <div class="f-text">
                            <div>Used</div>
                            <strong class="letter-spc-1">World Wide</strong>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="set-middle">
                        <div class="f-logo">
                            <img width="45px" src="{{ asset('web/img/security.png') }}" alt="">
                        </div>
                        <div class="f-text">
                            <strong class="letter-spc-1">100% Secure</strong>
                            <div>with 24/7 support</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-show">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="set-middle">
                            <div class="f-logo">
                                <img width="45px" src="{{ asset('web/img/click.png') }}" alt="">
                            </div>
                            <div class="f-text">
                                <strong>Best To</strong>
                                <div>Find Institution</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider-feature"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="set-middle">
                            <div class="f-logo">
                                <img width="45px" src="{{ asset('web/img/success.png') }}" alt="">
                            </div>
                            <div class="f-text">
                                <div>Used</div>
                                <strong>World Wide</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider-feature"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="set-middle">
                            <div class="f-logo">
                                <img width="45px" src="{{ asset('web/img/security.png') }}" alt="">
                            </div>
                            <div class="f-text">
                                <strong>100% Secure</strong>
                                <div>with 24/7 support</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--feature end-->

    @yield('content')

    <!--subscribe-->
    <div class="subscribe text-center">
        <div class="s-text mb-2">Stay <strong>Connected</strong></div>
        <div class="s-text-2 text-center">Subscribe for our newsletter for getting latest updates and offers</div>
        <div class="input-group display-inline position-relative">
            <input type="text" class="custom-input" name="subscribe_email" id="subscribe_email" placeholder="Your Email">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1" onclick="subscribe()" id="subscribe_btn">Subscribe</span>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <!--subscribe end-->

    <!--footer-nav-->
    <section class="footer-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <ul class="list-unstyled">
                        <li>
                            <div class="head-text">About US</div>
                            <div class="horizontal-line"></div>
                        </li>
                        <li><a href="{{ env('URL_SERVICES') }}">Services</a></li>
                        <li><a href="{{ env('URL_JOBS') }}">Jobs</a></li>
                        <li><a href="{{ env('URL_BLOG') }}">Blog</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <ul class="list-unstyled">
                        <li>
                            <div class="head-text">Students</div>
                            <div class="horizontal-line"></div>
                        </li>
                        <li><a href="{{ env('URL_ABOUT') }}">About</a></li>
                        <li><a href="{{ env('URL_NEWS_EVENT') }}">News and Events</a></li>
                        <li><a href="{{ env('URL_SUPPORT') }}">Support</a></li>
                        <li><a href="{{ env('URL_FAQ') }}">FAQs</a></li>
                    </ul>
                </div>
                <div class="col-md-3  col-sm-6">
                    <ul class="list-unstyled">
                        <li>
                            <div class="head-text">Academics</div>
                            <div class="horizontal-line"></div>
                        </li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ env('URL_HOW_WORK') }}">How it Works</a></li>
                    </ul>
                </div>
                <div class="col-md-3  col-sm-6">
                    <div class="head-text pb-2 pt-2">Social</div>
                    <div class="horizontal-line mb-3"></div>
                    <a class="social" target="_blank" href="{{ env('SOCIAL_FACEBOOK') }}"><i class="fa fa-facebook"></i></a>
                    <a class="social" target="_blank" href="{{ env('SOCIAL_YOUTUBE') }}"><i class="fa fa-youtube"></i></a>
                    <a class="social" target="_blank" href="{{ env('SOCIAL_INSTAGRAM') }}"><i class="fa fa-instagram"></i></a>
                    <a class="social" target="_blank" href="{{ env('SOCIAL_PINTEREST') }}"><i class="fa fa-pinterest"></i></a>
                    <a class="social" target="_blank" href="{{ env('SOCIAL_LINKEDIN') }}"><i class="fa fa-linkedin"></i></a>
                    <a class="social" target="_blank" href="{{ env('SOCIAL_TWITTER') }}"><i class="fa fa-twitter"></i></a>
                </div>
            </div>
        </div>
    </section>

    <!--footer-nav end-->
    <div class="divider"></div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-logo">
                        <a href="{{ url('/') }}"><img class="" width="150px" src="{{ asset('web/img/logo-white.jpg') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-text mt-2">
                        <a href="{{ url('term-of-condition') }}">Our <strong>Terms and Condition</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('web/js/jquary-3.4.1.js') }}"></script>
    <script src="{{ asset('web/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('web/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('web/js/script.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <script>
        function subscribe() {
          var email = $('#subscribe_email').val();
          if(email=='') {
              toastr.error('Subscribe Email is required!');
              return false;
          }

          $('#subscribe_btn').html('sending...');

          $.ajax({
              type: 'POST',
              url: "{{route('subscribe-email')}}",
              data: {'email': email},
              dataType: "json",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(response) {
                  if (response.success) {
                      toastr.success(response.message);
                      $('#subscribe_email').val('');
                      $('#subscribe_btn').html('Subscribe');
                  } else {
                      toastr.error(response.message);
                  }
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  $('#subscribe_btn').html('Subscribe');
                  toastr.error(xhr.status+' : '+thrownError);
              }
          });
        }
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-65559240-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-65559240-4');
    </script>
</body>
</html>


