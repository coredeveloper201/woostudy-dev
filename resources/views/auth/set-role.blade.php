@extends('layouts.app')

@section('content')
<div class="auth-page">
    <div class="row">
        <div class="col-lg-8 image"></div>
        <div class="col-lg-4 authContent">
            <div class="authBox">
                <form method="POST" action="{{ route('setRole') }}">
                    @csrf
                    @if($emailInput)
                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="role">{{ __('Who are you') }}</label>
                        <select class="form-control" name='role_id' id='role' class="form-control">
                            @foreach ($roles as $role)
                                <option value='{{$role->id}}'>{{$role->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css_stack')
    <style>
        .social-connect {
            padding-top: 20px;
        }
    </style>
@endpush
