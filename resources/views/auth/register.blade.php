@extends('layouts.app')

@section('content')
<div class="auth-page">
    <div class="row">
        <div class="col-lg-7 image"></div>
        <div class="col-lg-4 authContent">
            <div class="authBox">
                <div class="white-box">
                    <div class="sign-in-text text-center">Register with {{ config('app.name', 'Laravel') }}</div>
                    @if(session('combineSchool') != null) <p class="alert alert-warning text-center">{{session('combineSchool')}}</p> @endif
                    <div class="margin-20"></div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif
                        </div>


                        <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="form-group">
                            <label for="role">{{ __('Who are you') }}</label>
                            <select class="form-control" name='role_id' id='role' class="form-control">
                                @foreach ($roles as $role)
                                    <option value='{{$role->id}}'>{{$role->title}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('role_id'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('role_id') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input  id="terms_and_condition" class="mx-3 styled-checkbox" type='checkbox'/>
                            <label for="terms_and_condition">I agree to Terms and conditions. That any information may result in the cancellation of admissions, or you can be block listed to the all our affiliate sites</label>
                        </div>

                        <div class="form-group">
                            <button id="registration_submit_button" type="submit" disabled class="btn login-btn btn-block">
                                {{ __('Register') }}
                            </button>
                        </div>

                        <div class="form-group text-center">
                            <a class="blue-text2" href="{{ route('login') }}">
                                {{ __('Back to login') }}
                            </a>
                        </div>
                        <div class="margin-20"></div>
                        <div class="divider position-relative">
                            <div class="text-or">
                                OR
                            </div>
                        </div>
                        <div class="margin-20"></div>
                        @include('layouts.social')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css_stack')
    <style>
        .social-connect {
            padding-top: 20px;
        }
    </style>
@endpush
@push('js_stack')
    <script>
        $(function () {
            var $rsb = $('#registration_submit_button')
            var $tac = $('#terms_and_condition')
            $tac.on('click', function () {
                if ($(this).is(':checked')) {
                    $rsb.removeAttr('disabled')
                    console.log('remove attr')
                } else {
                    $rsb.attr('disabled', true);
                    console.log('add attr')
                }
            })
        })
    </script>
@endpush
