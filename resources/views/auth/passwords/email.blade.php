@extends('layouts.app')

@section('content')
<div class="auth-page">
    <div class="row">
        <div class="col-lg-7 image"></div>
        <div class="col-lg-4 authContent">
            <div class="authBox">
                <div class="white-box">
                    <div class="sign-in-text text-center">{{ __('Reset Password') }}</div>
                    <div class="margin-20"></div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn login-btn btn-block">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>

                        <div class="form-group text-center">
                            <a class="blue-text2" href="{{ route('login') }}">
                                {{ __('Back to login?') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
