@extends('layouts.app')

@section('content')
<div class="auth-page">
    <div class="row">
        <div class="col-lg-7 image"></div>
        <div class="col-lg-4 authContent">
            <div class="authBox">
             <div class="white-box">
{{--                 <h3 class="text-center">Login</h3>--}}
                 <div class="sign-in-text">Sign in</div>
                 <div><span class="new-acc-text">New User?</span> <a class="create-acc-text" href="{{ route('register') }}">Create an account</a></div>
                 <p class="text-danger text-center">{{ (isset($error))?$error:'' }}</p>
                 <!-- /.flex my-4 flex-center -->
                 <form method="POST" action="{{ route('login') }}">
                     @csrf

                     <div class="form-group">
                         <label class="text-ash" for="email">{{ __('E-Mail') }}</label>
                             <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" autocomplete="new-password" value="{{ old('email') }}" required autofocus>
                         @if ($errors->has('email'))
                             <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                         @endif
                     </div>

                     <div class="form-group position-relative">
                         <label for="password">{{ __('Password') }}</label>
                         <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" autocomplete="new-password" name="password" required>
                         <div class="eye-logo" onclick="changeAttr()"><i id="eyeLogo" class="fa fa-eye-slash"></i></div>
                         @if ($errors->has('password'))
                             <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                         @endif
                     </div>

                     <div class="form-group">
                         <input class="styled-checkbox" id="remember" name="remember" type="checkbox"  {{ old('remember') ? 'checked' : '' }}>
                         <label for="remember"> {{ __('Remember Me') }}</label>
                         <button type="submit" class="btn login-btn float-right">
                             {{ __('Login') }}
                         </button>

                       {{--  <div class="form-check">
                             <input class="form-check-input" type="checkbox" name="remember"
                                    id="remember" {{ old('remember') ? 'checked' : '' }}>

                             <label class="form-check-label" for="remember">
                                 {{ __('Remember Me') }}
                             </label>
                         </div>--}}
                     </div>
                     @if ($message = Session::get('message'))
                            <div class="alert alert-warning alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                     <div class="margin-20"></div>
                     <div class="form-group text-center">
                         <a class="blue-text" href="{{ route('password.request') }}">
                             {{ __('Reset Your Password') }}
                         </a>
                     </div>

                    {{-- <div class="form-group">
                         <a class="btn-link" href="{{ route('register') }}">
                             {{ __('Create New Account?') }}
                         </a>
                     </div>--}}
{{--
                     <div class="form-group">
                         <button type="submit" class="btn btn-primary">
                             {{ __('Login') }}
                         </button>
                     </div>--}}

                  {{--   <div class="form-group">
                         <a class="btn-link" href="{{ route('password.request') }}">
                             {{ __('Forgot Your Password?') }}
                         </a>
                     </div>--}}
                     <div class="margin-20"></div>
                     <div class="margin-20"></div>
                     <div class="divider position-relative">
                         <div class="text-or">
                             OR
                         </div>
                     </div>
                     <div class="margin-20"></div>
                     <div class="margin-20"></div>
                     @include('layouts.social')
                 </form>
             </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css_stack')
    <style>
        .social-connect{
            padding-top: 20px;
        }
    </style>
@endpush
@push('js_stack')
    <script>
        function changeAttr() {
            let input = $('#password');
            if ( input.attr('type') == 'password'){
                input.attr('type', 'text');
                $('#eyeLogo').removeClass('fa-eye-slash');
                $('#eyeLogo').addClass('fa-eye');
            }else {
                input.attr('type', 'password');
                $('#eyeLogo').addClass('fa-eye-slash');
                $('#eyeLogo').removeClass('fa-eye');
            }
        }
    </script>
@endpush

