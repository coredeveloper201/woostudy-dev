@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">HOW <strong>IT WORKS</strong></div>
  <div class="work-area">
      <div class="container">
          <div class="row">
            <div class="col-2"></div>
            <div class="col-2"></div>
              <div class="col-2">
                  <div id="logo-1" class="work-item active" onclick="showFeature(1)">
                      <div class="work-logo">
                          <img class="work-logo-img" src="{{ asset('web/img/ins-logo.png') }}" alt="">
                      </div>
                      <div class="w-l-text">For Schools</div>
                  </div>
              </div>
              <div class="col-2"></div>
              {{-- <div class="col-4">
                  <div id="logo-2" class="work-item" onclick="showFeature(2)">
                      <div class="work-logo">
                          <img class="work-logo-img" src="{{ asset('web/img/teacher-logo.png') }}" alt="">
                      </div>
                      <div class="w-l-text">For Teacher</div>
                  </div>
              </div> --}}
              <div class="col-2">
                  <div id="logo-3" class="work-item" onclick="showFeature(3)">
                      <div class="work-logo">
                          <img class="work-logo-img" src="{{ asset('web/img/stn-logo.png') }}" alt="">
                      </div>
                      <div class="w-l-text">For Students</div>
                  </div>
              </div>
              <div class="col-2"></div>
          </div>
      </div>
  </div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container fadeIn animated">
      <div class="row">
          <div class="col-md-6 position-relative">
              <div class="long-bar bg-ash"></div>
              <div class="text-area">
                  <div class="text-no font-white bg-yellow">1</div>
                  <div class="text-detail">
                      <div class="text-1"><strong>Connect</strong> with potential students instantly</div>
                      <div class="text-2">Let us ensure that your career has that needed boost. Connect instantly with
                          institutions according to your criteria and derive your success yourself.
                      </div>
                     {{--  <div class="text-1"><strong>Connect</strong> with potential students and teachers instantly</div>
                      <div class="text-2">Let us ensure that your career has that needed boost. Connect instantly with
                          institutions according to your criteria and derive your success yourself.
                      </div> --}}
                  </div>
              </div>
              <div class="text-area">
                  <div class="text-no bg-ash font-ash">2</div>
                  <div class="text-detail">
                      <div class="text-1"><span>Accept</span> <strong>online registration students</strong></div>
                      <div class="text-2">An assortment of course await you to learn from with Woo-study. Let us
                          ensure you learn the best from the best.
                      </div>
                      {{-- <div class="text-1"><span>Accept</span> <strong>online registration of teachers and students</strong></div>
                      <div class="text-2">An assortment of course await you to learn from with Woo-study. Let us
                          ensure you learn the best from the best.
                      </div> --}}
                  </div>
              </div>
              <div class="text-area">
                  <div class="text-no bg-ash font-ash">3</div>
                  <div class="text-detail">
                      <div class="text-1"><span>Monitor social media activities</span> <strong>of students</strong></div>
                      <div class="text-2">Become one on Woo-Study to earn and learn online. As a one you can
                          earn better by giving online lectures to students all around the globe.
                      </div>
                      {{-- <div class="text-1"><span>Monitor social media activities</span> <strong>of students and teachers</strong></div>
                      <div class="text-2">Become a teacher on Woo-Study to earn and learn online. As a teacher you can
                          earn better by giving online lectures to students all around the globe.
                      </div> --}}
                  </div>
              </div>
              <div class="text-area">
                  <a class="btn btn-started btn-margin" href="{{ url('/register') }}">Get Started Now</a>
              </div>
          </div>
          <div class="col-md-6">
              <div class="about-img" style="background-image: url('{{ asset('web/img/institution.png') }}')"></div>
          </div>
      </div>
  </div>
  <div id="feature-2" class="container display-hide fadeIn animated">
      <div class="row">
          <div class="col-md-6 position-relative">
              <div class="long-bar bg-ash"></div>
              <div class="text-area">
                  <div class="text-no font-white bg-yellow">1</div>
                  <div class="text-detail">
                      {{-- <div class="text-1"><strong>Connect</strong> with your matching institutions instantly</div>
                      <div class="text-2">Let us ensure that your career has that needed boost. Connect instantly with
                          institutions according to your criteria and derive your success yourself.
                      </div> --}}
                      <div class="text-1"><strong>Connect</strong> with your matching institutions instantly</div>
                      <div class="text-2">Let us ensure that your career has that needed boost. Connect instantly with
                          institutions according to your criteria and derive your success yourself.
                      </div>
                  </div>
              </div>
              <div class="text-area">
                  <div class="text-no bg-ash font-ash">2</div>
                  <div class="text-detail">
                      <div class="text-1"><span>Enroll for more than</span> <strong>50 online Course</strong></div>
                      <div class="text-2">An assortment of course await you to learn from with Woo-study. Let us
                          ensure you learn the best from the best.
                      </div>
                  </div>
              </div>
              <div class="text-area">
                  <div class="text-no bg-ash font-ash">3</div>
                  <div class="text-detail">
                      <div class="text-1"><span>Teach Online and</span> <strong>Earn from your Passion</strong></div>
                      <div class="text-2">Become a teacher on Woo-Study to earn and learn online. As a teacher you can
                          earn better by giving online lectures to students all around the globe.
                      </div>
                  </div>
              </div>
              <div class="text-area">
                  <a class="btn btn-started btn-margin" href="{{ url('/register') }}">Get Started Now</a>
              </div>
          </div>
          <div class="col-md-6">
              <div class="about-img" style="background-image: url('{{ asset('web/img/teacher.png') }}')"></div>
          </div>
      </div>
  </div>
  <div id="feature-3" class="container display-hide fadeIn animated">
      <div class="row">
          <div class="col-md-6 position-relative">
              <div class="long-bar bg-ash"></div>
              <div class="text-area">
                  <div class="text-no font-white bg-yellow">1</div>
                  <div class="text-detail">
                      <div class="text-1"><strong>Connect</strong> with potential schools instantly</div>
                      <div class="text-2">With WooStudy, its easy for you to attend schools in North America and other parts of the world!
                      </div>
                  </div>
                  {{-- <div class="text-detail">
                      <div class="text-1"><strong>Connect</strong> with potential schools and teachers instantly</div>
                      <div class="text-2">With WooStudy, its easy for you to attend schools in North America and other parts of the world!
                      </div>
                  </div> --}}
              </div>
              <div class="text-area">
                  <div class="text-no bg-ash font-ash">2</div>
                  <div class="text-detail">
                      <div class="text-1"><span>Just register</span> <strong>with with some basic info and we will walk you through it.</strong></div>

                      <div class="text-2">
					  You login, create your profile and let schools  connect with you instantly.
                      </div>
                      {{-- <div class="text-1"><span>Just register</span> <strong>with with some basic info and we will walk you through it.</strong></div>

                      <div class="text-2">
					  You login, create your profile and let schools & teachers connect with you instantly.
                      </div> --}}
                  </div>
              </div>
              <div class="text-area">
                  <div class="text-no bg-ash font-ash">3</div>
                  <div class="text-detail">
                      <div class="text-1"><span>Stay connected</span> <strong>with your matched schools via social media too!</strong></div>

                      <div class="text-2">
					  WooStudy makes it easier for you to send tweets to matched entities and to monitor social accounts of your matched listings.
                      </div>
                   {{--    <div class="text-1"><span>Stay connected</span> <strong>with your matched schools and teachers via social media too!</strong></div>

                      <div class="text-2">
					  WooStudy makes it easier for you to send tweets to matched entities and to monitor social accounts of your matched listings.
                      </div> --}}
                  </div>
              </div>
              <div class="text-area">
                  <a class="btn btn-started btn-margin" href="{{ url('/register') }}">Get Started Now</a>
              </div>
          </div>
          <div class="col-md-6">
              <div class="about-img" style="background-image: url('{{ asset('web/img/student.png') }}')"></div>

          </div>
      </div>
  </div>
</section>
@endsection
