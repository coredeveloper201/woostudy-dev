@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">Services</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="text-justify about-term">We are providing the following Services:
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">1</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">
                        <strong>Students and Teachers Connect with Institution:</strong>
                        <p>Woostudy.com provides you a platform in which students and teachers connect with each other. 
                        We accept online registration also and monitored the social media activities of students and teachers.</p>

                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">2</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">
                        <strong>Register yourself and start your teaching career:</strong>
                        <p>Woostudy.com provides you a chance to register yourself in more than 50 courses and start teaching from today.
                        So, convert your teaching passion into a professional teacher and connect with potential 
                        students and institutions instantly</p>
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">3</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">
                      <strong>Connect with the best institution and qualified teachers:</strong>
                      <p>
                        We offer you to get enroll in potential schools and teachers. 
                        Just add your basic information and start study instantly.
                        Woostudy.com gives you a platform in which you will connect with your matched teachers and institutions via social media.</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
