<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Email</title>
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('web/css/email.css') }}">
</head>
<style>
  * {
    font-family: Lato, sans-serif;
    box-sizing: border-box;
    margin: 0;
    padding: 0
  }

  .email-body {
    width: 740px;
    display: table;
    margin: auto;
  }

  .email-body .e-logo {
    width: 100%;
    display: table;
    text-align: center;
    padding: 15px 0
  }

  .email-body .e-logo img {
    width: 150px
  }

  .email-body .e-back {
    width: 100%;
    display: block;
    background-color: #dae6ed;
    height: 160px
  }

  .email-body .e-back .mail-emg {
    margin: 20px 0 0 0;
    width: 100%;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    height: 160px
  }

  .email-body .e-back .mail-emg .l-text {
    padding: 50px 100px;
    color: #fff;
    text-align: right
  }

  .email-body .e-back .mail-emg .l-text .text-bold {
    font-weight: 700
  }

  .email-body .e-container {
    width: 700px;
    margin: auto;
    display: table
  }

  .email-body .to-box {
    width: 100%;
    padding: 20px 0 10px 0
  }

  .email-body .to-box .name {
    font-weight: 700;
    color: #1167a2;
    padding: 10px 0 5px 0
  }

  .email-body .to-box .font-blue {
    color: #1167a2
  }

  .email-body .to-box .des {
    color: #797979;
    margin-bottom: 20px
  }

  .email-body .left-sec {
    width: 25%;
    float: left;
    padding: 0 5px 0 5px
  }

  .email-body .left-sec .school-detail {
    width: 100%;
    display: table;
    border: 1px solid #dbdbdb;
    background-color: #fcfcfc;
    padding: 4px;
    position: relative;
    z-index: 1;
    text-align: center
  }

  .email-body .left-sec .school-detail .s-logo {
    width: 70px;
    height: 70px;
    border-radius: 50%;
    border: 3px solid #fff;
    text-align: center;
    display: inline-block;
    background-color: #f17b16;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover
  }

  .email-body .left-sec .school-detail .s-name {
    padding-top: 10px;
    color: #1167a2;
    font-weight: 700;
    text-align: center;
    padding-bottom: 5px
  }

  .email-body .left-sec .school-detail .s-location {
    display: flex;
    justify-content: center;
    padding-bottom: 8px
  }

  .email-body .left-sec .school-detail .s-location .s-l-logo {
    width: 18px;
    margin-right: 3px
  }

  .email-body .left-sec .school-detail .s-location .s-l-name {
    color: #797979
  }

  .email-body .left-sec .school-detail .s-option {
    padding: 0;
    list-style: none;
    margin: 0;
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    padding-bottom: 10px
  }

  .email-body .left-sec .school-detail .s-option .s-e-option a {
    text-decoration: none;
    color: #797979;
    font-size: 13px
  }

  .email-body .left-sec .school-detail .s-option .s-e-option a.active {
    color: #57bd66
  }

  .email-body .left-sec .school-detail .s-info {
    width: 100%;
    display: flex;
    padding: 5px 0 10px 0;
    align-items: center;
    font-size: 14px
  }

  .email-body .left-sec .school-detail .s-info .i-logo {
    color: #797979;
    margin-right: 7px
  }

  .email-body .left-sec .school-detail .s-info .mrc-5 {
    margin-right: 5px
  }

  .email-body .left-sec .school-detail .s-info .text-ash {
    color: #797979;
    font-weight: 700
  }

  .email-body .right-sec {
    width: 72%;
    float: left;
    padding: 0 5px 0 5px
  }

  .email-body .right-sec .people-connect-box {
    width: 100%;
    padding: 5px;
    border: 1px solid #1167a2;
    margin-bottom: 30px;
    text-align: center
  }

  .email-body .right-sec .people-connect-box .h-text {
    padding: 5px 0 0 0;
    font-weight: 700;
    color: #1167a2;
    font-size: 18px;
    text-align: center;
    margin-bottom: 20px;
    font-family: Montserrat, sans-serif
  }

  .email-body .right-sec .people-connect-box .pro-box {
    width: 95px;
    display: inline-block;
    padding: 2px;
    margin: 0 10px;
    text-align: center;
    margin-bottom: 50px
  }

  .email-body .right-sec .people-connect-box .pro-box .pro-pic {
    width: 90px;
    height: 90px;
    border-radius: 50%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    margin-bottom: 5px
  }

  .email-body .right-sec .people-connect-box .pro-box .name {
    padding: 8px 0;
    color: #797979
  }

  .email-body .right-sec .sch-text {
    text-align: center;
    color: #f17b16;
    font-weight: 700;
    font-family: Montserrat, sans-serif;
    margin-bottom: 20px
  }

  .email-body .right-sec .sch-abt-box {
    width: 100%;
    padding: 5px;
    border: 1px solid #f17b16;
    margin-bottom: 30px
  }

  .email-body .right-sec .h-text {
    color: #f17b16;
    font-weight: bold;
    padding: 10px 0
  }

  .email-body .right-sec .divider {
    width: 100%;
    height: 1px;
    display: table;
    background-color: #f17b16
  }

  .email-body .right-sec .detail-text {
    color: #797979;
    padding: 10px 0
  }

  .email-body .footer-box {
    width: 100%;
    background-color: #fafafa;
    padding: 10px;
    text-align: center
  }

  .email-body .footer-box .text-1 {
    color: #797979;
    padding: 10px 0;
    font-weight: 500
  }

  .btn {
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
  }

  @media (prefers-reduced-motion: reduce) {
    .btn {
      transition: none
    }
  }

  .btn:hover {
    color: #212529;
    text-decoration: none
  }

  .btn.focus, .btn:focus {
    outline: 0;
    box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25)
  }

  .btn.disabled, .btn:disabled {
    opacity: .65
  }

  a.btn.disabled, fieldset:disabled a.btn {
    pointer-events: none
  }

  .b-r-0 {
    border-radius: 0
  }

  .btn-connect {
    background-color: #1167a2;
    color: #fff;
    text-decoration: none
  }

  .btn-connect:hover {
    color: #fff !important
  }

  /*# sourceMappingURL=email.css.map */

</style>
<body>
<div class="email-body">
  <div class="e-logo">
    <img src="{{ asset('web/img/logo.png') }}" alt="">
  </div>
  <div class="e-back">
    <div class="e-container">
      <div class="mail-emg" style="background-image: url('{{ asset('web/img/school_profile/email.png')}}')" alt="">
        <div class="l-text">
          LEARN FROM
          <br>
          <span class="text-bold">ANYWHERE</span>
        </div>
      </div>
    </div>
  </div>
  <div class="e-container">
    <div class="to-box">
      <div class="name">Hi Jonathan</div>
      <div class="des">
        More than <span class="font-blue">50 students</span> shown below are looking for admissions in your school
      </div>
    </div>
    <div class="left-sec">
      <div class="school-detail">
        <div class="s-logo" style="background-image: url('{{ asset('web/img/school_profile/school.png')}}')"></div>
        <div class="s-name">School Name</div>
        <div class="s-location">
          <img class="s-l-logo" src="{{ asset('web/img/school_profile/s-location.gif') }}" alt="">
          <div class="s-l-name">Newyork, USA</div>
        </div>
        <ul class="s-option">
          <li class="s-e-option">
            <a href="#"><img src="{{ asset('web/img/school_profile/e-hat.png') }}" alt=""></a>
          </li>
          <li class="s-e-option">
            <a href="#">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-brif.png') }}" alt=""></a>
            </a>
          </li>
          <li class="s-e-option">
            <a href="#" class="">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-doller.png') }}" alt=""></a>
            </a>
          </li>
          <li class="s-e-option">
            <a href="#">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-email.png') }}" alt=""></a>
            </a>
          </li>
          <li class="s-e-option">
            <a href="#" class="">
              <a href="#"><img src="{{ asset('web/img/school_profile/e-home.png') }}" alt=""></a>
            </a>
          </li>
        </ul>
        <div class="divider"></div>
        <div class="s-info">
          <img class="i-logo" src="{{ asset('web/img/school_profile/e-cer.png') }}" alt="">
          <span class="mrc-5">Founded</span> <span
            class="text-ash"> 1995</span>
        </div>
        <div class="s-info">
          <img class="i-logo" src="{{ asset('web/img/school_profile/e-uni.png') }}" alt=""><span
            class="mrc-5">Type</span> <span
            class="text-ash"> University</span>
        </div>
        <div class="s-info">
          <img class="i-logo" src="{{ asset('web/img/school_profile/e-grp.png') }}" alt=""><span class="mrc-5">Total Students</span>
          <span
            class="text-ash">10k+</span>
        </div>
      </div>
    </div>
    <div class="right-sec">
      <div class="people-connect-box">
        <div class="h-text">Students who wants to connect with you</div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/jsmith.png')}}')"></div>
          <div class="name">John Smith</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p2.png')}}')"></div>
          <div class="name">Anderson</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p3.png')}}')"></div>
          <div class="name">Elisa</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p4.png')}}')"></div>
          <div class="name">Mark P</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p5.png')}}')"></div>
          <div class="name">John Smith</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/jsmith.png')}}')"></div>
          <div class="name">John Smith</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p2.png')}}')"></div>
          <div class="name">Anderson</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
        <div class="pro-box">
          <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p3.png')}}')"></div>
          <div class="name">Elisa</div>
          <a href="#" class="btn btn-connect b-r-0">Connect</a>
        </div>
      </div>
      <div class="sch-text">Your School Profile</div>
      <div class="sch-abt-box">
        <div class="h-text">About</div>
        <div class="divider"></div>
        <div class="detail-text">Founded in 1868, Oregon State is an international public research university located in
          Corvallis, one of the safest, smartest, greenest small cities in the notion. Oregon State accounts for more
          research funding than oll of the state's comprehensive public universities combined. With 11 colleges, 15
          Agricultural Experiment Stations, 35 county Extension offices, the Hatfield Marine Science Center in Newport
          and OSU-Cascades in Bend, Oregon State has a presence in every one of
          Oregon's 36 counties
        </div>
      </div>
    </div>
  </div>
  <div class="footer-box">
    <div class="text-1">Download our mobile app on</div>

    <a href="#" style="width: 100px; display: inline-block;">
      <img style="width: 90px" src="{{ asset('web/img/school_profile/apple.png')}}" alt="">
    </a>

    <a href="#" style="width: 100px; display: inline-block;">
      <img style="width: 90px" src="{{ asset('web/img/school_profile/google.png')}}" alt="">
    </a>

    <div class="text-1" style="font-size: 13px">
      © 2020 WooStudy Inc. All rights reserved. WooStudy and the WooStudy logo are registered trademarks of WooStudy,
      Inc. All other trademarks are property of their respective owners. View our Privacy Notice.
    </div>
  </div>
</div>

</body>
</html>
