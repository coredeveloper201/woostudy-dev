@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">FAQs</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="text-justify about-term">
                  <strong>What is the procedure to connect with woostudy.com as a student?</strong>
                  <p>
                    Just register yourself with some basic info and we will walk you through it.
                    You just login, create your profile and let schools & teachers connect with you instantly.
                  </p>

                  <strong>How many courses I can enroll in?</strong>
                  <p>
                    You can enroll for more than 50 online Courses.
                    An assortment, of course, awaits you to learn from with Woostudy. Let us ensure you learn the best from the best.
                  </p>

                  <strong>Can woostudy.com accept the online registration of teachers?</strong>
                  <p>
                    Yes, we accept online registration of teachers as well as students too.
                  </p>
            </div>
      </div>
  </div>
</section>
@endsection
