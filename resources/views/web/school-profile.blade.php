<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>School Profile</title>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css"/>
  <link href="{{ asset('web/css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('web/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('web/css/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('web/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('web/css/school-profile.css') }}" rel="stylesheet">

</head>
<body>
<!--nav start-->
<section class="navigation-bar">
  <div class="container">
    <nav class="navbar nav-padding navbar-expand-lg navbar-light">
      <a class="navbar-brand" href="#"><img class="logo" src="{{ asset('web/img/logo.png') }}" alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav custom-navbar-nav ml-auto">
          <li class="nav-item nav-item-margin active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item nav-item-margin">
            <a class="nav-link" href="#">Profile</a>
          </li>
          <li class="nav-item nav-item-margin">
            <a class="nav-link" href="#">Connections</a>
          </li>
          <li class="nav-item nav-item-margin">
            <a class="nav-link" href="#">Chats</a>
          </li>
          <li class="nav-item nav-item-margin">
            <a class="nav-link" href="#">Blog</a>
          </li>
          <li class="nav-item nav-item-margin">
            <a class="nav-link" href="#">About Us</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</section>
<!--nav end-->

<!--hero start-->
<div class="hero-img">
  <img src="{{ asset('web/img/school_profile/hero.png') }}" alt="">
</div>
<!--hero end-->

<!--inner content start-->
<div class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-12 col-sm-12 col-p-0">
        <div class="school-detail">
          <div class="s-logo" style="background-image: url('{{ asset('web/img/school_profile/school.png')}}')"></div>
          <div class="s-name">School Name</div>
          <div class="s-location">
            <img class="s-l-logo" src="{{ asset('web/img/school_profile/s-location.gif') }}" alt="">
            <div class="s-l-name">Newyork, USA</div>
          </div>
          <ul class="s-option">
            <li class="s-e-option">
              <a href="#" class="active"><i class="fa fa-graduation-cap" aria-hidden="true"></i></a>
            </li>
            <li class="s-e-option">
              <a href="#">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
              </a>
            </li>
            <li class="s-e-option">
              <a href="#" class="active">
                <i class="fa fa-usd" aria-hidden="true"></i>
              </a>
            </li>
            <li class="s-e-option">
              <a href="#">
                <i class="fa fa-envelope" aria-hidden="true"></i>
              </a>
            </li>
            <li class="s-e-option">
              <a href="#" class="active">
                <i class="fa fa-home" aria-hidden="true"></i>
              </a>
            </li>
          </ul>
          <div class="divider"></div>
          <div class="s-info">
            <i class="fa fa-certificate i-logo"></i><span class="mrc-5">Founded</span> <span
              class="text-ash"> 1995</span>
          </div>
          <div class="s-info">
            <i class="fa fa-university i-logo"></i><span class="mrc-5">Type</span> <span
              class="text-ash"> University</span>
          </div>
          <div class="s-info">
            <i class="fa fa-users i-logo"></i><span class="mrc-5">Total Students</span> <span
              class="text-ash">10k+</span>
          </div>
        </div>
      </div>
      <div class="col-lg-10 col-md-12 col-sm-12">
        <div class="gallery-box">
          <div class="each-img">
            <img src="{{ asset('web/img/school_profile/s-1.png') }}" alt="">
          </div>
          <div class="each-img">
            <img src="{{ asset('web/img/school_profile/s-2.gif') }}" alt="">
          </div>
          <div class="each-img">
            <img src="{{ asset('web/img/school_profile/s-3.gif') }}" alt="">
          </div>
          <div class="each-img">
            <img src="{{ asset('web/img/school_profile/s-1.png') }}" alt="">
          </div>
        </div>
        <div class="tab-box">
          <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-tabs mobile-hide" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                     aria-selected="true">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#features"
                     aria-selected="false">Features</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#location"
                     aria-selected="false">Location</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#disciplines"
                     aria-selected="false">Disciplines</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#disciplines" role="tab" aria-controls="contact"
                     aria-selected="false">Financials</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#programs"
                     aria-selected="false">Programs</a>
                </li>
              </ul>
              <div class="dropdown mobile-show text-right">
                <img id="dropdownMenuButton" data-toggle="dropdown"  class="mr-4" aria-haspopup="true" aria-expanded="false" src="{{ asset('web/img/school_profile/open-menu.png') }}" alt="">
                <div class="dropdown-menu left" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item"  href="#home">About</a>
                  <a class="dropdown-item" href="#features">Features</a>
                  <a class="dropdown-item" href="#location">Location</a>
                  <a class="dropdown-item" href="#disciplines">Disciplines</a>
                  <a class="dropdown-item" href="#disciplines">Financials</a>
                  <a class="dropdown-item" href="#programs">Programs</a>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="h-text">About</div>
              <div class="text-justify font-ash">Founded in 1868, Oregon State is an international public research university located in Corvallis,
                one of the safest, smartest, greenest small cities in the notion. Oregon State accounts for more
                research funding than oll of the state's comprehensive public universities combined. With 11 colleges,
                15 Agricultural Experiment Stations, 35 county Extension offices, the Hatfield Marine Science Center in
                Newport and OSU-Cascades in Bend, Oregon State has a presence in every one of Oregon's 36 counties
              </div>
              <a href="#" class="a-text">See More</a>
            </div>
          </div>
        </div>
        <div id="features" class="feature-box">
          <div class="f-text">Features</div>
          <div class="f-box">
            <div class="inner-box">
              <div class="text-right font-ash">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
              </div>
              <div class="in-logo">
                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
              </div>
              <div class="sub-text">
                Optional Practice Training
              </div>
            </div>
          </div>
          <div class="f-box">
            <div class="inner-box">
              <div class="text-right font-ash">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
              </div>
              <div class="in-logo">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
              </div>
              <div class="sub-text">
                Programs With Co-Operation
              </div>
            </div>
          </div>
          <div class="f-box">
            <div class="inner-box">
              <div class="text-right font-ash">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
              </div>
              <div class="in-logo">
                <i class="fa fa-usd" aria-hidden="true"></i>
              </div>
              <div class="sub-text">
                Programs With Co-Operation
              </div>
            </div>
          </div>
          <div class="f-box">
            <div class="inner-box">
              <div class="text-right font-ash">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
              </div>
              <div class="in-logo">
                <i class="fa fa-envelope" aria-hidden="true"></i>
              </div>
              <div class="sub-text">
                Conditional Offer Letter
              </div>
            </div>
          </div>
          <div class="f-box">
            <div class="inner-box">
              <div class="text-right font-ash">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
              </div>
              <div class="in-logo">
                <i class="fa fa-home" aria-hidden="true"></i>
              </div>
              <div class="sub-text">
                Type of Accomodation
              </div>
            </div>
          </div>
        </div>
        <div id="location" class="map-box">
          <div class="f-text">Location</div>
          <div class="sub-text">Texas. NV30s, USA</div>
          <div class="go-map position-relative">
            <div class="input-group mb-3">
              <input type="text" class="form-control absolute-input" id="autocomplete" placeholder="Enter your address" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <span class="input-group-text btn-map" id="basic-addon2"><i class="fa fa-search"></i></span>
              </div>
            </div>
            <div id="geo-map"></div>
          </div>
        </div>
        <div id="disciplines" class="row">
          <div class="col-md-5">
            <div class="bar-box">
              <div class="f-text">Top Disciplines</div>
              <div class="sub-text">Based on total number of programs</div>
              <div class="logo">
                <img src="{{ asset('web/img/school_profile/prog.png') }}" alt="">
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="cost-box">
              <div class="f-text">Financials</div>
              <table class="table">
                <thead>
                <tr>
                  <th width="60%">Description</th>
                  <th class="text-right">Sub Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Avg/ cost of Tuition/Year</td>
                  <td class="text-right">USD $31,347.00</td>
                </tr>
                <tr>
                  <td>Cost of Living/Year</td>
                  <td class="text-right">USD $12,000.00</td>
                </tr>
                <tr>
                  <td style="border-bottom: none">*Application Fee</td>
                  <td class="text-right" style="border-bottom: none">
                    USD $0.00-$85.00
                    <br>
                    <small>*Charged once for each program</small>
                  </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                  <td>Estimated Total/Year</td>
                  <td class="text-right">USD $38,624.00</td>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <div id="programs" class="perfect-program">
          <div class="h-text">Find Your Perfect Program</div>
          <div class="filter-box">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <select name="" id="" class="form-control c-cont mar-20">
                      <option value="1">Select Education Level</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <select name="" id="" class="form-control c-cont">
                      <option value="1">Select Program</option>
                    </select>
                  </div>
                </div>
                <div class="margin-20"></div>
                <div class="position-relative">
                  <input type="text" class="form-control c-cont" placeholder="Search by program title">
                  <i class="fa fa-search font-ash search-btn"></i>
                </div>
              </div>
              <div class="col-md-4 text-right-c">
                <div class="sort-item mar-top-20">
                  <i class="fa fa-align-right font-ash" aria-hidden="true"></i> <span>Sort By</span>
                </div>
                <div class="sort-item">
                  <a href="#" class="">Clear Filters</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="connect-box">
          <div class="each-box">
            <div class="row">
              <div class="col-md-1">
                <div class="process-box">
                  <img class="cal-img" src="{{ asset('web/img/school_profile/cal-head.png') }}" alt="">
                  <div class="text-box" style="background-color: #E7ECED">
                    <div class="line-hight-15">45 AVG</div>
                  </div>
                  <div class="sub-text">
                    Process Days
                  </div>
                </div>
              </div>
              <div class="col-md-9">
                <div class="y-text">
                  Bachelor of Science - Agricultural Business Management
                </div>
                <div class="row">
                  <div class="col-md-6 font-15">
                    <span class="tuition-fee-box">Tution: USD 31,250.00</span>
                    <div><img style="margin-right: 6px" src="{{ asset('web/img/school_profile/logo-c2.gif') }}" alt="">
                      <span class="tuition-fee-text"> Application Fee: 0 USD</span></div>
                    <div><img style="margin-left: 13px; margin-right: 8px"
                              src="{{ asset('web/img/school_profile/logo-c3.gif') }}" alt=""> <span
                        class="tuition-fee-text"> Start: MAR 2020</span></div>
                  </div>
                  <div class="col-md-6 font-15">
                    <div class="tuition-fee-box pad-top-0">
                      <span><img style="margin-right: 1px;" src="{{ asset('web/img/school_profile/logo-c1.gif') }}" alt=""> 4 Years Bachelor Degree</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="btn-box">
                  <a href="#" class="btn btn-connect">Connect</a>
                </div>
              </div>
            </div>
          </div>
          <div class="each-box">
            <div class="row">
              <div class="col-md-1">
                <div class="process-box">
                  <img class="cal-img" src="{{ asset('web/img/school_profile/cal-head.png') }}" alt="">
                  <div class="text-box" style="background-color: #e45555; color: #ffffff">
                    <div class="line-hight-15">60 AVG</div>
                  </div>
                  <div class="sub-text">
                    Process Days
                  </div>
                </div>
              </div>
              <div class="col-md-9">
                <div class="y-text">
                  Master of Science - Agricultural Education
                </div>
                <div class="row">
                  <div class="col-md-6 font-15">
                    <span class="tuition-fee-box">Tution: USD 31,250.00</span>
                    <div><img style="margin-right: 6px" src="{{ asset('web/img/school_profile/logo-c2.gif') }}" alt="">
                      <span class="tuition-fee-text"> Application Fee: 0 USD</span></div>
                    <div><img style="margin-left: 15px; margin-right: 10px"
                              src="{{ asset('web/img/school_profile/logo-c3.gif') }}" alt=""> <span
                        class="tuition-fee-text"> Start: MAR 2020</span></div>
                  </div>
                  <div class="col-md-6 font-15">
                    <div class="tuition-fee-box pad-top-0">
                      <span><img style="margin-right: 1px;" src="{{ asset('web/img/school_profile/logo-c1.gif') }}" alt=""> Postgraduate certificate/Master's Degree</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="btn-box">
                  <a href="#" class="btn btn-connect">Connect</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="people-connect-box">
          <div class="h-text">Students who wants to connect with you</div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/jsmith.png')}}')"></div>
            <div class="name">John Smith</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p2.png')}}')"></div>
            <div class="name">Anderson</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p3.png')}}')"></div>
            <div class="name">Elisa</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p4.png')}}')"></div>
            <div class="name">Mark P</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p5.png')}}')"></div>
            <div class="name">John Smith</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/jsmith.png')}}')"></div>
            <div class="name">John Smith</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p2.png')}}')"></div>
            <div class="name">Anderson</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p3.png')}}')"></div>
            <div class="name">Elisa</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p4.png')}}')"></div>
            <div class="name">Mark P</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
          <div class="pro-box">
            <div class="pro-pic" style="background-image: url('{{ asset('web/img/school_profile/p5.png')}}')"></div>
            <div class="name">John Smith</div>
            <a href="#" class="btn btn-connect b-r-0">Connect</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--inner content end-->


<script src="{{ asset('web/js/jquary-3.4.1.js') }}"></script>
<script src="{{ asset('web/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAv43_cLHUT0sSGUdaXeE5oKuZRA4T-YzQ&libraries=places&callback=initAutocomplete"></script>

<script>
  // Initialize
  var map;

  // // Add marker
  var marker = new google.maps.Marker({
    position: {lat: -34.397, lng: 150.644},
    map: map
  });

  function initMap() {
    map = new google.maps.Map(document.getElementById('geo-map'), {
      center: {lat: -34.397, lng: 150.644},
      zoom: 10
    });
  }
  initMap();

  /*autocomplete*/
  function initAutocomplete() {
    const input = document.getElementById('autocomplete');
    new google.maps.places.Autocomplete(input);
  }

  initAutocomplete()

  $(document).ready(function(){
    $("a").on('click', function(event) {
      if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        let target = event.target;
        $('.nav-link').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){
          window.location.hash = hash;
        });
      }
    });
  });

  $(function () {
    initMap();
  })
</script>
</body>
</html>


