<?php

use Faker\Generator as Faker;

$factory->define( \App\Licence::class, function (Faker $faker) {
    return [

        'title' =>  $faker->word,
        'authority' =>  $faker->word,
        'user_id' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'start_date' => $faker->date(),
        'valid_till' => $faker->date(),
        'created_at' => $faker->dateTimeThisMonth,
        'updated_at' => $faker->dateTimeThisMonth
    ];
} );
