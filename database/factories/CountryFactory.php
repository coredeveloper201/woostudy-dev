<?php

use Faker\Generator as Faker;

$factory->define( \App\Country::class, function (Faker $f) {
    $country = $f->unique()->country;
    return [
        'title' => $country,
        'code' => $f->unique()->countryCode,
        'region_id' => $f->randomElement( \App\Region::pluck( 'id' )->toArray() ),
        'slug' => str_slug( $country, '_' ),
        'flag' => $f->imageUrl( 30, 30 )
    ];
} );
