<?php

use Faker\Generator as Faker;

$factory->define( \App\EducationLevel::class, function (Faker $faker) {
    return [
        'title' => ucfirst( $faker->randomLetter ) . '.' . ucfirst( $faker->randomLetter ) . '.' . ucfirst( $faker->randomLetter ),
        'status' => $faker->boolean,
        'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'created_at' => $faker->dateTimeThisMonth,
        'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'approved_at' => $faker->dateTimeThisMonth
    ];
} );
