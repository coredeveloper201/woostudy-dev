<?php

use Faker\Generator as Faker;

$factory->define( \App\Course::class, function (Faker $faker) {
    return [

        'title' => $faker->randomElement( ['C++', 'C', 'Python', 'English Literature', 'Religion ', 'Master In math'] ) . ' Course Offer',
        'education_level_id' => $faker->randomElement( \App\EducationLevel::pluck( 'id' )->toArray() ),
        'user_id' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'fee' => random_int( 142, 8745 ),
        'starting_date' => $faker->date(),
        'description' => $faker->unique()->safeEmail,
        'licence_required' => $faker->boolean,
        'licence_id' => $faker->randomElement( \App\Licence::pluck( 'id' )->toArray() ),
        'created_at' => $faker->dateTimeThisMonth,
        'updated_at' => $faker->dateTimeThisMonth
    ];
} );
