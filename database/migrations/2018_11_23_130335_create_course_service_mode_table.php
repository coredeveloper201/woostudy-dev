<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseServiceModeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'course_service_mode', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'course_id' );
            $table->enum( 'mode', ['home', 'tuition', 'remote'] );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'course_service_mode' );
    }
}
