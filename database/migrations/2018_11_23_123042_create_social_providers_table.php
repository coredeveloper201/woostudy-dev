<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'social_providers', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'user_id' );
            $table->string( 'provider_id' );
            $table->string( 'token' );
            $table->string( 'refresh_token' )->nullable();
            $table->string( 'token_secret' )->nullable();;
            $table->string( 'token_expire' )->nullable();;
            $table->string( 'provider' );
            $table->string( 'url' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'social_providers' );
    }
}
