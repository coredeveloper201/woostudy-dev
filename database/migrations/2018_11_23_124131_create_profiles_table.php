<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'profiles', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'user_id' );
            $table->enum( 'gender', ['male', 'female', 'others'] );
            $table->date( 'date_of_birth' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'profiles' );
    }
}
