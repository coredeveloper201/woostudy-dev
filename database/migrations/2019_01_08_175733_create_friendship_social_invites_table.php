<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateFriendshipSocialInvitesTable
 */
class CreateFriendshipSocialInvitesTable extends Migration
{

    public function up() {

      Schema::create( 'friendship_social_invites', function (Blueprint $table) {
          $table->increments( 'id' );
          $table->unsignedInteger( 'user_id' );
          $table->text( 'custom_message' )->nullable();
          $table->date( 'short_url' )->nullable();
          $table->timestamps();
      } );

    }

    public function down() {
        Schema::dropIfExists(config('friendship_social_invites'));
    }

}
