<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUpgradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upgrades', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('payment_type');
            $table->date('payment_date');
            $table->string('card_number')->nullable();
            $table->string('card_full_name')->nullable();
            $table->string('card_expire')->nullable();
            $table->string('card_cvc')->nullable();
            $table->decimal('amount', 10, 2);
            $table->text('payment_info')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upgrades');
    }
}
