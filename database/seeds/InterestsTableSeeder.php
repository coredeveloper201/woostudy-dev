<?php

use App\Interest;
use Faker\Factory;
use Illuminate\Database\Seeder;

class InterestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      Interest::truncate();
      $faker = Factory::create();
      foreach (range(0, 15) as $key) {
        Interest::create([
          "title" => $faker->word,
          "user_id" => rand(1, 4),
          "since" => $faker->date($format = 'Y-m-d', $max = 'now'),
        ]);
      }

    }
}
