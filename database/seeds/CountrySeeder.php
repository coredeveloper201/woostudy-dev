<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /*=>
     * =>4.]*
]    * @return void
     */
    public function run()
    {
        App\Country::insert(
            [
                [
                    "flag" => "https://restcountries.eu/data/afg.svg",
                    "title" => "Afghanistan",
                    "slug"=>"Afghanistan",
                    "code" => "AFG",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/ala.svg",
                    "title" => "Åland Islands",
                    "slug"=>"Åland Islands",
                    "code" => "ALA",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/alb.svg",
                    "title" => "Albania",
                    "slug"=>"Albania",
                    "code" => "ALB",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/dza.svg",
                    "title" => "Algeria",
                    "slug"=>"Algeria",
                    "code" => "DZA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/asm.svg",
                    "title" => "American Samoa",
                    "slug"=>"American Samoa",
                    "code" => "ASM",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/and.svg",
                    "title" => "Andorra",
                    "slug"=>"Andorra",
                    "code" => "AND",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/ago.svg",
                    "title" => "Angola",
                    "slug"=>"Angola",
                    "code" => "AGO",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/aia.svg",
                    "title" => "Anguilla",
                    "slug"=>"Anguilla",
                    "code" => "AIA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/ata.svg",
                    "title" => "Antarctica",
                    "slug"=>"Antarctica",
                    "code" => "ATA",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/atg.svg",
                    "title" => "Antigua and Barbuda",
                    "slug"=>"Antigua and Barbuda",
                    "code" => "ATG",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/arg.svg",
                    "title" => "Argentina",
                    "slug"=>"Argentina",
                    "code" => "ARG",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/arm.svg",
                    "title" => "Armenia",
                    "slug"=>"Armenia",
                    "code" => "ARM",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/abw.svg",
                    "title" => "Aruba",
                    "slug"=>"Aruba",
                    "code" => "ABW",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/aus.svg",
                    "title" => "Australia",
                    "slug"=>"Australia",
                    "code" => "AUS",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/aut.svg",
                    "title" => "Austria",
                    "slug"=>"Austria",
                    "code" => "AUT",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/aze.svg",
                    "title" => "Azerbaijan",
                    "slug"=>"Azerbaijan",
                    "code" => "AZE",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/bhs.svg",
                    "title" => "Bahamas",
                    "slug"=>"Bahamas",
                    "code" => "BHS",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/bhr.svg",
                    "title" => "Bahrain",
                    "slug"=>"Bahrain",
                    "code" => "BHR",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/bgd.svg",
                    "title" => "Bangladesh",
                    "slug"=>"Bangladesh",
                    "code" => "BGD",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/brb.svg",
                    "title" => "Barbados",
                    "slug"=>"Barbados",
                    "code" => "BRB",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/blr.svg",
                    "title" => "Belarus",
                    "slug"=>"Belarus",
                    "code" => "BLR",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/bel.svg",
                    "title" => "Belgium",
                    "slug"=>"Belgium",
                    "code" => "BEL",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/blz.svg",
                    "title" => "Belize",
                    "slug"=>"Belize",
                    "code" => "BLZ",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/ben.svg",
                    "title" => "Benin",
                    "slug"=>"Benin",
                    "code" => "BEN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/bmu.svg",
                    "title" => "Bermuda",
                    "slug"=>"Bermuda",
                    "code" => "BMU",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/btn.svg",
                    "title" => "Bhutan",
                    "slug"=>"Bhutan",
                    "code" => "BTN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/bol.svg",
                    "title" => "Bolivia (Plurinational State of)",
                    "slug"=>"Bolivia (Plurinational State of)",
                    "code" => "BOL",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/bes.svg",
                    "title" => "Bonaire, Sint Eustatius and Saba",
                    "slug"=>"Bonaire, Sint Eustatius and Saba",
                    "code" => "BES",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/bih.svg",
                    "title" => "Bosnia and Herzegovina",
                    "slug"=>"Bosnia and Herzegovina",
                    "code" => "BIH",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/bwa.svg",
                    "title" => "Botswana",
                    "slug"=>"Botswana",
                    "code" => "BWA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/bvt.svg",
                    "title" => "Bouvet Island",
                    "slug"=>"Bouvet Island",
                    "code" => "BVT",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/bra.svg",
                    "title" => "Brazil",
                    "slug"=>"Brazil",
                    "code" => "BRA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/iot.svg",
                    "title" => "British Indian Ocean Territory",
                    "slug"=>"British Indian Ocean Territory",
                    "code" => "IOT",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/umi.svg",
                    "title" => "United States Minor Outlying Islands",
                    "slug"=>"United States Minor Outlying Islands",
                    "code" => "UMI",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/vgb.svg",
                    "title" => "Virgin Islands (British)",
                    "slug"=>"Virgin Islands (British)",
                    "code" => "VGB",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/vir.svg",
                    "title" => "Virgin Islands (U.S.)",
                    "slug"=>"Virgin Islands (U.S.)",
                    "code" => "VIR",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/brn.svg",
                    "title" => "Brunei Darussalam",
                    "slug"=>"Brunei Darussalam",
                    "code" => "BRN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/bgr.svg",
                    "title" => "Bulgaria",
                    "slug"=>"Bulgaria",
                    "code" => "BGR",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/bfa.svg",
                    "title" => "Burkina Faso",
                    "slug"=>"Burkina Faso",
                    "code" => "BFA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/bdi.svg",
                    "title" => "Burundi",
                    "slug"=>"Burundi",
                    "code" => "BDI",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/khm.svg",
                    "title" => "Cambodia",
                    "slug"=>"Cambodia",
                    "code" => "KHM",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/cmr.svg",
                    "title" => "Cameroon",
                    "slug"=>"Cameroon",
                    "code" => "CMR",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/can.svg",
                    "title" => "Canada",
                    "slug"=>"Canada",
                    "code" => "CAN",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/cpv.svg",
                    "title" => "Cabo Verde",
                    "slug"=>"Cabo Verde",
                    "code" => "CPV",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/cym.svg",
                    "title" => "Cayman Islands",
                    "slug"=>"Cayman Islands",
                    "code" => "CYM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/caf.svg",
                    "title" => "Central African Republic",
                    "slug"=>"Central African Republic",
                    "code" => "CAF",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/tcd.svg",
                    "title" => "Chad",
                    "slug"=>"Chad",
                    "code" => "TCD",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/chl.svg",
                    "title" => "Chile",
                    "slug"=>"Chile",
                    "code" => "CHL",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/chn.svg",
                    "title" => "China",
                    "slug"=>"China",
                    "code" => "CHN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/cxr.svg",
                    "title" => "Christmas Island",
                    "slug"=>"Christmas Island",
                    "code" => "CXR",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/cck.svg",
                    "title" => "Cocos (Keeling) Islands",
                    "slug"=>"Cocos (Keeling) Islands",
                    "code" => "CCK",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/col.svg",
                    "title" => "Colombia",
                    "slug"=>"Colombia",
                    "code" => "COL",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/com.svg",
                    "title" => "Comoros",
                    "slug"=>"Comoros",
                    "code" => "COM",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/cog.svg",
                    "title" => "Congo",
                    "slug"=>"Congo",
                    "code" => "COG",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/cod.svg",
                    "title" => "Congo (Democratic Republic of the)",
                    "slug"=>"Congo (Democratic Republic of the)",
                    "code" => "COD",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/cok.svg",
                    "title" => "Cook Islands",
                    "slug"=>"Cook Islands",
                    "code" => "COK",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/cri.svg",
                    "title" => "Costa Rica",
                    "slug"=>"Costa Rica",
                    "code" => "CRI",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/hrv.svg",
                    "title" => "Croatia",
                    "slug"=>"Croatia",
                    "code" => "HRV",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/cub.svg",
                    "title" => "Cuba",
                    "slug"=>"Cuba",
                    "code" => "CUB",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/cuw.svg",
                    "title" => "Curaçao",
                    "slug"=>"Curaçao",
                    "code" => "CUW",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/cyp.svg",
                    "title" => "Cyprus",
                    "slug"=>"Cyprus",
                    "code" => "CYP",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/cze.svg",
                    "title" => "Czech Republic",
                    "slug"=>"Czech Republic",
                    "code" => "CZE",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/dnk.svg",
                    "title" => "Denmark",
                    "slug"=>"Denmark",
                    "code" => "DNK",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/dji.svg",
                    "title" => "Djibouti",
                    "slug"=>"Djibouti",
                    "code" => "DJI",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/dma.svg",
                    "title" => "Dominica",
                    "slug"=>"Dominica",
                    "code" => "DMA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/dom.svg",
                    "title" => "Dominican Republic",
                    "slug"=>"Dominican Republic",
                    "code" => "DOM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/ecu.svg",
                    "title" => "Ecuador",
                    "slug"=>"Ecuador",
                    "code" => "ECU",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/egy.svg",
                    "title" => "Egypt",
                    "slug"=>"Egypt",
                    "code" => "EGY",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/slv.svg",
                    "title" => "El Salvador",
                    "slug"=>"El Salvador",
                    "code" => "SLV",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/gnq.svg",
                    "title" => "Equatorial Guinea",
                    "slug"=>"Equatorial Guinea",
                    "code" => "GNQ",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/eri.svg",
                    "title" => "Eritrea",
                    "slug"=>"Eritrea",
                    "code" => "ERI",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/est.svg",
                    "title" => "Estonia",
                    "slug"=>"Estonia",
                    "code" => "EST",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/eth.svg",
                    "title" => "Ethiopia",
                    "slug"=>"Ethiopia",
                    "code" => "ETH",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/flk.svg",
                    "title" => "Falkland Islands (Malvinas)",
                    "slug"=>"Falkland Islands (Malvinas)",
                    "code" => "FLK",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/fro.svg",
                    "title" => "Faroe Islands",
                    "slug"=>"Faroe Islands",
                    "code" => "FRO",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/fji.svg",
                    "title" => "Fiji",
                    "slug"=>"Fiji",
                    "code" => "FJI",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/fin.svg",
                    "title" => "Finland",
                    "slug"=>"Finland",
                    "code" => "FIN",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/fra.svg",
                    "title" => "France",
                    "slug"=>"France",
                    "code" => "FRA",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/guf.svg",
                    "title" => "French Guiana",
                    "slug"=>"French Guiana",
                    "code" => "GUF",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/pyf.svg",
                    "title" => "French Polynesia",
                    "slug"=>"French Polynesia",
                    "code" => "PYF",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/atf.svg",
                    "title" => "French Southern Territories",
                    "slug"=>"French Southern Territories",
                    "code" => "ATF",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/gab.svg",
                    "title" => "Gabon",
                    "slug"=>"Gabon",
                    "code" => "GAB",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/gmb.svg",
                    "title" => "Gambia",
                    "slug"=>"Gambia",
                    "code" => "GMB",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/geo.svg",
                    "title" => "Georgia",
                    "slug"=>"Georgia",
                    "code" => "GEO",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/deu.svg",
                    "title" => "Germany",
                    "slug"=>"Germany",
                    "code" => "DEU",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/gha.svg",
                    "title" => "Ghana",
                    "slug"=>"Ghana",
                    "code" => "GHA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/gib.svg",
                    "title" => "Gibraltar",
                    "slug"=>"Gibraltar",
                    "code" => "GIB",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/grc.svg",
                    "title" => "Greece",
                    "slug"=>"Greece",
                    "code" => "GRC",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/grl.svg",
                    "title" => "Greenland",
                    "slug"=>"Greenland",
                    "code" => "GRL",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/grd.svg",
                    "title" => "Grenada",
                    "slug"=>"Grenada",
                    "code" => "GRD",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/glp.svg",
                    "title" => "Guadeloupe",
                    "slug"=>"Guadeloupe",
                    "code" => "GLP",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/gum.svg",
                    "title" => "Guam",
                    "slug"=>"Guam",
                    "code" => "GUM",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/gtm.svg",
                    "title" => "Guatemala",
                    "slug"=>"Guatemala",
                    "code" => "GTM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/ggy.svg",
                    "title" => "Guernsey",
                    "slug"=>"Guernsey",
                    "code" => "GGY",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/gin.svg",
                    "title" => "Guinea",
                    "slug"=>"Guinea",
                    "code" => "GIN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/gnb.svg",
                    "title" => "Guinea-Bissau",
                    "slug"=>"Guinea-Bissau",
                    "code" => "GNB",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/guy.svg",
                    "title" => "Guyana",
                    "slug"=>"Guyana",
                    "code" => "GUY",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/hti.svg",
                    "title" => "Haiti",
                    "slug"=>"Haiti",
                    "code" => "HTI",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/hmd.svg",
                    "title" => "Heard Island and McDonald Islands",
                    "slug"=>"Heard Island and McDonald Islands",
                    "code" => "HMD",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/vat.svg",
                    "title" => "Holy See",
                    "slug"=>"Holy See",
                    "code" => "VAT",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/hnd.svg",
                    "title" => "Honduras",
                    "slug"=>"Honduras",
                    "code" => "HND",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/hkg.svg",
                    "title" => "Hong Kong",
                    "slug"=>"Hong Kong",
                    "code" => "HKG",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/hun.svg",
                    "title" => "Hungary",
                    "slug"=>"Hungary",
                    "code" => "HUN",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/isl.svg",
                    "title" => "Iceland",
                    "slug"=>"Iceland",
                    "code" => "ISL",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/ind.svg",
                    "title" => "India",
                    "slug"=>"India",
                    "code" => "IND",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/idn.svg",
                    "title" => "Indonesia",
                    "slug"=>"Indonesia",
                    "code" => "IDN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/civ.svg",
                    "title" => "Côte d'Ivoire",
                    "slug"=>"Côte d'Ivoire",
                    "code" => "CIV",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/irn.svg",
                    "title" => "Iran (Islamic Republic of)",
                    "slug"=>"Iran (Islamic Republic of)",
                    "code" => "IRN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/irq.svg",
                    "title" => "Iraq",
                    "slug"=>"Iraq",
                    "code" => "IRQ",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/irl.svg",
                    "title" => "Ireland",
                    "slug"=>"Ireland",
                    "code" => "IRL",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/imn.svg",
                    "title" => "Isle of Man",
                    "slug"=>"Isle of Man",
                    "code" => "IMN",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/isr.svg",
                    "title" => "Israel",
                    "slug"=>"Israel",
                    "code" => "ISR",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/ita.svg",
                    "title" => "Italy",
                    "slug"=>"Italy",
                    "code" => "ITA",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/jam.svg",
                    "title" => "Jamaica",
                    "slug"=>"Jamaica",
                    "code" => "JAM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/jpn.svg",
                    "title" => "Japan",
                    "slug"=>"Japan",
                    "code" => "JPN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/jey.svg",
                    "title" => "Jersey",
                    "slug"=>"Jersey",
                    "code" => "JEY",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/jor.svg",
                    "title" => "Jordan",
                    "slug"=>"Jordan",
                    "code" => "JOR",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/kaz.svg",
                    "title" => "Kazakhstan",
                    "slug"=>"Kazakhstan",
                    "code" => "KAZ",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/ken.svg",
                    "title" => "Kenya",
                    "slug"=>"Kenya",
                    "code" => "KEN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/kir.svg",
                    "title" => "Kiribati",
                    "slug"=>"Kiribati",
                    "code" => "KIR",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/kwt.svg",
                    "title" => "Kuwait",
                    "slug"=>"Kuwait",
                    "code" => "KWT",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/kgz.svg",
                    "title" => "Kyrgyzstan",
                    "slug"=>"Kyrgyzstan",
                    "code" => "KGZ",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/lao.svg",
                    "title" => "Lao People's Democratic Republic",
                    "slug"=>"Lao People's Democratic Republic",
                    "code" => "LAO",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/lva.svg",
                    "title" => "Latvia",
                    "slug"=>"Latvia",
                    "code" => "LVA",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/lbn.svg",
                    "title" => "Lebanon",
                    "slug"=>"Lebanon",
                    "code" => "LBN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/lso.svg",
                    "title" => "Lesotho",
                    "slug"=>"Lesotho",
                    "code" => "LSO",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/lbr.svg",
                    "title" => "Liberia",
                    "slug"=>"Liberia",
                    "code" => "LBR",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/lby.svg",
                    "title" => "Libya",
                    "slug"=>"Libya",
                    "code" => "LBY",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/lie.svg",
                    "title" => "Liechtenstein",
                    "slug"=>"Liechtenstein",
                    "code" => "LIE",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/ltu.svg",
                    "title" => "Lithuania",
                    "slug"=>"Lithuania",
                    "code" => "LTU",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/lux.svg",
                    "title" => "Luxembourg",
                    "slug"=>"Luxembourg",
                    "code" => "LUX",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/mac.svg",
                    "title" => "Macao",
                    "slug"=>"Macao",
                    "code" => "MAC",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/mkd.svg",
                    "title" => "Macedonia (the former Yugoslav Republic of)",
                    "slug"=>"Macedonia (the former Yugoslav Republic of)",
                    "code" => "MKD",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/mdg.svg",
                    "title" => "Madagascar",
                    "slug"=>"Madagascar",
                    "code" => "MDG",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/mwi.svg",
                    "title" => "Malawi",
                    "slug"=>"Malawi",
                    "code" => "MWI",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/mys.svg",
                    "title" => "Malaysia",
                    "slug"=>"Malaysia",
                    "code" => "MYS",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/mdv.svg",
                    "title" => "Maldives",
                    "slug"=>"Maldives",
                    "code" => "MDV",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/mli.svg",
                    "title" => "Mali",
                    "slug"=>"Mali",
                    "code" => "MLI",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/mlt.svg",
                    "title" => "Malta",
                    "slug"=>"Malta",
                    "code" => "MLT",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/mhl.svg",
                    "title" => "Marshall Islands",
                    "slug"=>"Marshall Islands",
                    "code" => "MHL",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/mtq.svg",
                    "title" => "Martinique",
                    "slug"=>"Martinique",
                    "code" => "MTQ",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/mrt.svg",
                    "title" => "Mauritania",
                    "slug"=>"Mauritania",
                    "code" => "MRT",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/mus.svg",
                    "title" => "Mauritius",
                    "slug"=>"Mauritius",
                    "code" => "MUS",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/myt.svg",
                    "title" => "Mayotte",
                    "slug"=>"Mayotte",
                    "code" => "MYT",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/mex.svg",
                    "title" => "Mexico",
                    "slug"=>"Mexico",
                    "code" => "MEX",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/fsm.svg",
                    "title" => "Micronesia (Federated States of)",
                    "slug"=>"Micronesia (Federated States of)",
                    "code" => "FSM",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/mda.svg",
                    "title" => "Moldova (Republic of)",
                    "slug"=>"Moldova (Republic of)",
                    "code" => "MDA",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/mco.svg",
                    "title" => "Monaco",
                    "slug"=>"Monaco",
                    "code" => "MCO",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/mng.svg",
                    "title" => "Mongolia",
                    "slug"=>"Mongolia",
                    "code" => "MNG",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/mne.svg",
                    "title" => "Montenegro",
                    "slug"=>"Montenegro",
                    "code" => "MNE",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/msr.svg",
                    "title" => "Montserrat",
                    "slug"=>"Montserrat",
                    "code" => "MSR",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/mar.svg",
                    "title" => "Morocco",
                    "slug"=>"Morocco",
                    "code" => "MAR",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/moz.svg",
                    "title" => "Mozambique",
                    "slug"=>"Mozambique",
                    "code" => "MOZ",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/mmr.svg",
                    "title" => "Myanmar",
                    "slug"=>"Myanmar",
                    "code" => "MMR",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/nam.svg",
                    "title" => "Namibia",
                    "slug"=>"Namibia",
                    "code" => "NAM",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/nru.svg",
                    "title" => "Nauru",
                    "slug"=>"Nauru",
                    "code" => "NRU",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/npl.svg",
                    "title" => "Nepal",
                    "slug"=>"Nepal",
                    "code" => "NPL",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/nld.svg",
                    "title" => "Netherlands",
                    "slug"=>"Netherlands",
                    "code" => "NLD",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/ncl.svg",
                    "title" => "New Caledonia",
                    "slug"=>"New Caledonia",
                    "code" => "NCL",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/nzl.svg",
                    "title" => "New Zealand",
                    "slug"=>"New Zealand",
                    "code" => "NZL",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/nic.svg",
                    "title" => "Nicaragua",
                    "slug"=>"Nicaragua",
                    "code" => "NIC",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/ner.svg",
                    "title" => "Niger",
                    "slug"=>"Niger",
                    "code" => "NER",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/nga.svg",
                    "title" => "Nigeria",
                    "slug"=>"Nigeria",
                    "code" => "NGA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/niu.svg",
                    "title" => "Niue",
                    "slug"=>"Niue",
                    "code" => "NIU",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/nfk.svg",
                    "title" => "Norfolk Island",
                    "slug"=>"Norfolk Island",
                    "code" => "NFK",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/prk.svg",
                    "title" => "Korea (Democratic People's Republic of)",
                    "slug"=>"Korea (Democratic People's Republic of)",
                    "code" => "PRK",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/mnp.svg",
                    "title" => "Northern Mariana Islands",
                    "slug"=>"Northern Mariana Islands",
                    "code" => "MNP",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/nor.svg",
                    "title" => "Norway",
                    "slug"=>"Norway",
                    "code" => "NOR",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/omn.svg",
                    "title" => "Oman",
                    "slug"=>"Oman",
                    "code" => "OMN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/pak.svg",
                    "title" => "Pakistan",
                    "slug"=>"Pakistan",
                    "code" => "PAK",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/plw.svg",
                    "title" => "Palau",
                    "slug"=>"Palau",
                    "code" => "PLW",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/pse.svg",
                    "title" => "Palestine, State of",
                    "slug"=>"Palestine, State of",
                    "code" => "PSE",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/pan.svg",
                    "title" => "Panama",
                    "slug"=>"Panama",
                    "code" => "PAN",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/png.svg",
                    "title" => "Papua New Guinea",
                    "slug"=>"Papua New Guinea",
                    "code" => "PNG",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/pry.svg",
                    "title" => "Paraguay",
                    "slug"=>"Paraguay",
                    "code" => "PRY",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/per.svg",
                    "title" => "Peru",
                    "slug"=>"Peru",
                    "code" => "PER",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/phl.svg",
                    "title" => "Philippines",
                    "slug"=>"Philippines",
                    "code" => "PHL",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/pcn.svg",
                    "title" => "Pitcairn",
                    "slug"=>"Pitcairn",
                    "code" => "PCN",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/pol.svg",
                    "title" => "Poland",
                    "slug"=>"Poland",
                    "code" => "POL",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/prt.svg",
                    "title" => "Portugal",
                    "slug"=>"Portugal",
                    "code" => "PRT",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/pri.svg",
                    "title" => "Puerto Rico",
                    "slug"=>"Puerto Rico",
                    "code" => "PRI",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/qat.svg",
                    "title" => "Qatar",
                    "slug"=>"Qatar",
                    "code" => "QAT",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/kos.svg",
                    "title" => "Republic of Kosovo",
                    "slug"=>"Republic of Kosovo",
                    "code" => "KOS",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/reu.svg",
                    "title" => "Réunion",
                    "slug"=>"Réunion",
                    "code" => "REU",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/rou.svg",
                    "title" => "Romania",
                    "slug"=>"Romania",
                    "code" => "ROU",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/rus.svg",
                    "title" => "Russian Federation",
                    "slug"=>"Russian Federation",
                    "code" => "RUS",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/rwa.svg",
                    "title" => "Rwanda",
                    "slug"=>"Rwanda",
                    "code" => "RWA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/blm.svg",
                    "title" => "Saint Barthélemy",
                    "slug"=>"Saint Barthélemy",
                    "code" => "BLM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/shn.svg",
                    "title" => "Saint Helena, Ascension and Tristan da Cunha",
                    "slug"=>"Saint Helena, Ascension and Tristan da Cunha",
                    "code" => "SHN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/kna.svg",
                    "title" => "Saint Kitts and Nevis",
                    "slug"=>"Saint Kitts and Nevis",
                    "code" => "KNA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/lca.svg",
                    "title" => "Saint Lucia",
                    "slug"=>"Saint Lucia",
                    "code" => "LCA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/maf.svg",
                    "title" => "Saint Martin (French part)",
                    "slug"=>"Saint Martin (French part)",
                    "code" => "MAF",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/spm.svg",
                    "title" => "Saint Pierre and Miquelon",
                    "slug"=>"Saint Pierre and Miquelon",
                    "code" => "SPM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/vct.svg",
                    "title" => "Saint Vincent and the Grenadines",
                    "slug"=>"Saint Vincent and the Grenadines",
                    "code" => "VCT",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/wsm.svg",
                    "title" => "Samoa",
                    "slug"=>"Samoa",
                    "code" => "WSM",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/smr.svg",
                    "title" => "San Marino",
                    "slug"=>"San Marino",
                    "code" => "SMR",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/stp.svg",
                    "title" => "Sao Tome and Principe",
                    "slug"=>"Sao Tome and Principe",
                    "code" => "STP",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/sau.svg",
                    "title" => "Saudi Arabia",
                    "slug"=>"Saudi Arabia",
                    "code" => "SAU",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/sen.svg",
                    "title" => "Senegal",
                    "slug"=>"Senegal",
                    "code" => "SEN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/srb.svg",
                    "title" => "Serbia",
                    "slug"=>"Serbia",
                    "code" => "SRB",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/syc.svg",
                    "title" => "Seychelles",
                    "slug"=>"Seychelles",
                    "code" => "SYC",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/sle.svg",
                    "title" => "Sierra Leone",
                    "slug"=>"Sierra Leone",
                    "code" => "SLE",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/sgp.svg",
                    "title" => "Singapore",
                    "slug"=>"Singapore",
                    "code" => "SGP",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/sxm.svg",
                    "title" => "Sint Maarten (Dutch part)",
                    "slug"=>"Sint Maarten (Dutch part)",
                    "code" => "SXM",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/svk.svg",
                    "title" => "Slovakia",
                    "slug"=>"Slovakia",
                    "code" => "SVK",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/svn.svg",
                    "title" => "Slovenia",
                    "slug"=>"Slovenia",
                    "code" => "SVN",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/slb.svg",
                    "title" => "Solomon Islands",
                    "slug"=>"Solomon Islands",
                    "code" => "SLB",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/som.svg",
                    "title" => "Somalia",
                    "slug"=>"Somalia",
                    "code" => "SOM",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/zaf.svg",
                    "title" => "South Africa",
                    "slug"=>"South Africa",
                    "code" => "ZAF",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/sgs.svg",
                    "title" => "South Georgia and the South Sandwich Islands",
                    "slug"=>"South Georgia and the South Sandwich Islands",
                    "code" => "SGS",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/kor.svg",
                    "title" => "Korea (Republic of)",
                    "slug"=>"Korea (Republic of)",
                    "code" => "KOR",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/ssd.svg",
                    "title" => "South Sudan",
                    "slug"=>"South Sudan",
                    "code" => "SSD",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/esp.svg",
                    "title" => "Spain",
                    "slug"=>"Spain",
                    "code" => "ESP",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/lka.svg",
                    "title" => "Sri Lanka",
                    "slug"=>"Sri Lanka",
                    "code" => "LKA",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/sdn.svg",
                    "title" => "Sudan",
                    "slug"=>"Sudan",
                    "code" => "SDN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/sur.svg",
                    "title" => "Suriname",
                    "slug"=>"Suriname",
                    "code" => "SUR",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/sjm.svg",
                    "title" => "Svalbard and Jan Mayen",
                    "slug"=>"Svalbard and Jan Mayen",
                    "code" => "SJM",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/swz.svg",
                    "title" => "Swaziland",
                    "slug"=>"Swaziland",
                    "code" => "SWZ",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/swe.svg",
                    "title" => "Sweden",
                    "slug"=>"Sweden",
                    "code" => "SWE",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/che.svg",
                    "title" => "Switzerland",
                    "slug"=>"Switzerland",
                    "code" => "CHE",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/syr.svg",
                    "title" => "Syrian Arab Republic",
                    "slug"=>"Syrian Arab Republic",
                    "code" => "SYR",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/twn.svg",
                    "title" => "Taiwan",
                    "slug"=>"Taiwan",
                    "code" => "TWN",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/tjk.svg",
                    "title" => "Tajikistan",
                    "slug"=>"Tajikistan",
                    "code" => "TJK",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/tza.svg",
                    "title" => "Tanzania, United Republic of",
                    "slug"=>"Tanzania, United Republic of",
                    "code" => "TZA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/tha.svg",
                    "title" => "Thailand",
                    "slug"=>"Thailand",
                    "code" => "THA",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/tls.svg",
                    "title" => "Timor-Leste",
                    "slug"=>"Timor-Leste",
                    "code" => "TLS",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/tgo.svg",
                    "title" => "Togo",
                    "slug"=>"Togo",
                    "code" => "TGO",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/tkl.svg",
                    "title" => "Tokelau",
                    "slug"=>"Tokelau",
                    "code" => "TKL",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/ton.svg",
                    "title" => "Tonga",
                    "slug"=>"Tonga",
                    "code" => "TON",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/tto.svg",
                    "title" => "Trinidad and Tobago",
                    "slug"=>"Trinidad and Tobago",
                    "code" => "TTO",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/tun.svg",
                    "title" => "Tunisia",
                    "slug"=>"Tunisia",
                    "code" => "TUN",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/tur.svg",
                    "title" => "Turkey",
                    "slug"=>"Turkey",
                    "code" => "TUR",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/tkm.svg",
                    "title" => "Turkmenistan",
                    "slug"=>"Turkmenistan",
                    "code" => "TKM",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/tca.svg",
                    "title" => "Turks and Caicos Islands",
                    "slug"=>"Turks and Caicos Islands",
                    "code" => "TCA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/tuv.svg",
                    "title" => "Tuvalu",
                    "slug"=>"Tuvalu",
                    "code" => "TUV",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/uga.svg",
                    "title" => "Uganda",
                    "slug"=>"Uganda",
                    "code" => "UGA",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/ukr.svg",
                    "title" => "Ukraine",
                    "slug"=>"Ukraine",
                    "code" => "UKR",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/are.svg",
                    "title" => "United Arab Emirates",
                    "slug"=>"United Arab Emirates",
                    "code" => "ARE",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/gbr.svg",
                    "title" => "United Kingdom of Great Britain and Northern Ireland",
                    "slug"=>"United Kingdom of Great Britain and Northern Ireland",
                    "code" => "GBR",
                    "region_id" => 4
                ],
                [
                    "flag" => "https://restcountries.eu/data/usa.svg",
                    "title" => "United States of America",
                    "slug"=>"United States of America",
                    "code" => "USA",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/ury.svg",
                    "title" => "Uruguay",
                    "slug"=>"Uruguay",
                    "code" => "URY",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/uzb.svg",
                    "title" => "Uzbekistan",
                    "slug"=>"Uzbekistan",
                    "code" => "UZB",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/vut.svg",
                    "title" => "Vanuatu",
                    "slug"=>"Vanuatu",
                    "code" => "VUT",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/ven.svg",
                    "title" => "Venezuela (Bolivarian Republic of)",
                    "slug"=>"Venezuela (Bolivarian Republic of)",
                    "code" => "VEN",
                    "region_id" => 2
                ],
                [
                    "flag" => "https://restcountries.eu/data/vnm.svg",
                    "title" => "Viet Nam",
                    "slug"=>"Viet Nam",
                    "code" => "VNM",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/wlf.svg",
                    "title" => "Wallis and Futuna",
                    "slug"=>"Wallis and Futuna",
                    "code" => "WLF",
                    "region_id" => 5
                ],
                [
                    "flag" => "https://restcountries.eu/data/esh.svg",
                    "title" => "Western Sahara",
                    "slug"=>"Western Sahara",
                    "code" => "ESH",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/yem.svg",
                    "title" => "Yemen",
                    "slug"=>"Yemen",
                    "code" => "YEM",
                    "region_id" => 3
                ],
                [
                    "flag" => "https://restcountries.eu/data/zmb.svg",
                    "title" => "Zambia",
                    "slug"=>"Zambia",
                    "code" => "ZMB",
                    "region_id" => 1
                ],
                [
                    "flag" => "https://restcountries.eu/data/zwe.svg",
                    "title" => "Zimbabwe",
                    "slug"=>"Zimbabwe",
                    "code" => "ZWE",
                    "region_id" => 1
                ]
            ]


        );
    }
}
