<?php

use App\User;
use Illuminate\Database\Seeder;

class MDBUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mdbSave = app('\App\Http\Controllers\Wsapi\MongoSaveController');
        $users = User::select('id', 'role_id')->orderBy('id')->get();
        $total_count = 0;
        $error_count = 0;
        $start = time();
        foreach ($users as $user) {
            $res = $mdbSave->mongoSave($user->role_id, $user->id);
            echo "==============================================\n";
            echo "Current ID: $user->id \n";
            if ($res) {
                echo "Status: Success\n";
            } else {
                $error_count++;
                echo "Status: Error\n";
            }
            $total_count++;
            echo "Current Total: $total_count\n";
            echo "Current Execution Time: " . number_format((float)((time() - $start) / 60), 2, '.', '') . " Minutes \n";
        }
        $end = time();
        $executionTime = ($end - $start) / 60;
        echo "Net Total: $total_count, Success: ".($total_count-$error_count).", Errors: $error_count\n";
        echo "Execution Time: " . number_format((float)$executionTime, 2, '.', '') . " Minutes";
    }
}
