<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\State::insert(
            [
                [
                    'country_id' => 170,
                    'title' => 'Azad Jammu & Kashmir',
                ],
                [
                    'country_id' => 170,
                    'title' => 'Balochistan',
                ],
                [
                    'country_id' => 170,
                    'title' => 'Gilgit-Baltistan',
                ],
                [
                    'country_id' => 170,
                    'title' => 'Islamabad Capital Territory',
                ],
                [
                    'country_id' => 170,
                    'title' => 'Khyber Pakhtunkhwa',
                ],
                [
                    'country_id' => 170,
                    'title' => '	Punjab',
                ],
                [
                    'country_id' => 170,
                    'title' => '	Sindh',
                ]
            ]
        );
    }
}
