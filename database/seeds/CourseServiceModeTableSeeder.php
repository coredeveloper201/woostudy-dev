<?php

use Illuminate\Database\Seeder;

class CourseServiceModeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory( App\CourseServiceMode::class, 3 )->create();
    }
}
