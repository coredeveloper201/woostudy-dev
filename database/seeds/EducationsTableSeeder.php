<?php

use App\Education;
use Illuminate\Database\Seeder;
use Faker\Factory;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Education::truncate();
        $faker = Factory::create();
        foreach (range( 0, 15 ) as $key) {
            Education::create( [
                "title" => $faker->name,
                "user_id" => rand( 1, 4 ),
                "grade" => $faker->word,
                "institute_id" =>$faker->randomElement( \App\Institute::pluck( 'id' )->toArray() ),
                "completion_date" => $faker->date( $format = 'Y-m-d', $max = 'now' ),
                "percentage" => rand( 50, 90 ),
                "education_level_id" => $faker->randomElement( \App\EducationLevel::pluck( 'id' )->toArray() ),
            ] );
        }
    }
}
