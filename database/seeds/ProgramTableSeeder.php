<?php

use Illuminate\Database\Seeder;

class ProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '1024M');
        $programs = App\Program::get();
        foreach ($programs as $program) {
            $programLevelArr = array();
            if (strpos($program->degrees, '[') !== false) {
                $degrees = json_decode($program->degrees);
                foreach ($degrees as $degree) {
                    $educationLevel =  App\EducationLevel::where('title',$degree)->first();
                    $programLevelArr = array(
                        'educational_level_id' => $educationLevel->id,
                        'program_id' => $program->id,
                    );
                    App\ProgramEducationLevel::create($programLevelArr);
                }
            } else {
                $programLevelArr = array(
                    'educational_level_id' => $program->education_level_id,
                    'program_id' => $program->id,
                );
                App\ProgramEducationLevel::create($programLevelArr);
            }
        }
    }
}
