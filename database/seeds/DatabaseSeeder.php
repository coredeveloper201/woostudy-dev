<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call( UsersTableSeeder::class );
        $this->call( RolesTableSeeder::class );
        $this->call( EducationLevelSeeder::class );
        $this->call( InstituteTableSeeder::class );
        $this->call( EducationsTableSeeder::class );
        $this->call( ProfileOptionsTableSeeder::class );
        $this->call( IeltsExamLevelTableSeeder::class );
        $this->call( IeltsTableSeeder::class );
        $this->call( InterestsTableSeeder::class );
        $this->call( RegionSeeder::class );
        $this->call( CountrySeeder::class );
        $this->call( LicenceTableSeeder::class );
        $this->call( CourseTableSeeder::class );
        $this->call( InstituteRoleTableSeeder::class );
        $this->call( DepartmentTableSeeder::class );
        $this->call( CourseServiceModeTableSeeder::class );
    }
}
