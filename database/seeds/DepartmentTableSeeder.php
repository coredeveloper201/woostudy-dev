<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        App\Department::insert(
            [
                [
                    'title' => 'Computer Department',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth,
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],[
                    'title' => 'Electronics Department',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth,
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],
                [
                    'title' => 'English Department',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth,
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ] ,[
                    'title' => 'Religion Department',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth,
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],[
                    'title' =>'Physics Department',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth,
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],
                [
                    'title' => 'Math Department',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth,
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ]
            ]
        );
    }
}
