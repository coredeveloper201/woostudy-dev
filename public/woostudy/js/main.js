$(document).ready(function(){

$(".message_area").animate({ scrollTop: $(document).height() }, "slow");


        $('#datepicker').datepicker();

        $('#chooseFile').bind('change', function () {
          var filename = $("#chooseFile").val();
          if (/^\s*$/.test(filename)) {
            $(".file-upload").removeClass('active');
            $("#noFile").text("No file chosen..."); 
          }
          else {
            $(".file-upload").addClass('active');
            $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
          }
        });

        $('.popup_emoji').click(function(){
            $('.popup_emoji_content').toggle();
        });

        $( ".chat_llst li" ).click(function() {
              $('.msg_popup').show();   
        });
        $('.close_pop_up').click(function(){
            $('.msg_popup').hide(); 
        });

        $('.notification_menu').click(function(){
            $('.chat_notification').toggle();
            return false;
        });

        $('.close_toogle_icon').click(function(){
            $('.chat_llst').toggleClass('toogle_chat');
            return false;
        });

        $(document).mouseup(function (e){
            var container = $('.popup_emoji , .chat_notification , .msg_popup ,  .notification_menu');

            if (!container.is(e.target) // if the target of the click isn't the container...
              && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
              $('.popup_emoji_content').hide();
              $('.chat_notification').hide();
            }
          });
        /*
        =========================================================================================
            BANNER SLIDER
        =========================================================================================
        */

        var connection_slide = jQuery("#connection_slide");
        connection_slide.owlCarousel({
            loop: true,
            margin: 0,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,                
            autoplay:false,
            nav: false,
            dots:true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

        // $(window).on("load",function(){
        //     $(".message_area").mCustomScrollbar();
        // });
});
