<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramBranch extends Model
{
    protected $fillable = ['institute_id', 'program_id', 'address_id'];
}
