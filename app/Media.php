<?php

namespace App;


class Media extends Models
{
    protected $table = 'medias';

    protected $fillable = ['title', 'filename', 'ext', 'mime_type', 'size', 'original_filename', 'status', 'mediable_id', 'mediable_type'];

    public function mediable()
    {
        return $this->morphTo();
    }
}
