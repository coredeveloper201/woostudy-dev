<?php

namespace App;


class EducationLevel extends Models
{
    protected $fillable = ['id', 'title', 'status', 'created_by', 'approved_by', 'approved_at'];

    public function educations()
    {
        return $this->hasMany( Education::class );
    }

    public function courses()
    {
        return $this->hasMany( Course::class );
    }

    public function programs()
    {
        return $this->belongsToMany(Program::class, 'program_education_levels', 'education_level_id', 'program_id')->withPivot('institute_id')->withTimestamps();
    }
}
