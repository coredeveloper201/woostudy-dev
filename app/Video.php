<?php

namespace App;

use Illuminate\Support\Facades\Storage;

class Video extends Models
{
    protected $guarded = [];

    protected $appends = ['video_url'];

    public function getVideoUrlAttribute()
    {
        return url( Storage::url( 'uploads/' . $this->video ) );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function medias()
    {
        return $this->morphMany( Media::class, 'mediable' );
    }
}
