<?php
namespace App\Libraries;

use App\ProfileOption;
class Helpers {
  const USERS = [
    [
      'name' => 'shibu',
      'email' => 'shibu@gmail.com',
      'role_id' => 2,
    ],
    [
      'name' => 'ridwanul',
      'email' => 'ridwanul@gmail.com',
      'role_id' => 2,
    ],
    [
      'name' => 'palash',
      'email' => 'palash@gmail.com',
      'role_id' => 2,
    ],
    [
      'name' => 'sumon',
      'email' => 'sumon@gmail.com',
      'role_id' => 1,
    ],
  ];
  const ROLES = [
    [
      'name' => 'Admin',
      'id' => 1,
    ],
    [
      'name' => 'Student',
      'id' => 2,
    ],
    [
      'name' => 'School',
      'id' => 3,
    ],
    [
      'name' => 'Tutor',
      'id' => 4,
    ],
    [
      'name' => 'Counselor',
      'id' => 5,
    ],
    [
      'name' => 'Parent',
      'id' => 6,
    ],
  ];
  public static function set_or_update_profile_options($key, $value, $user_id) {
    $option = ProfileOption::where('option_key', $key)->where('user_id', $user_id)->first();
    if ($option) {
      $option->option_value = $value;
      $option->save();
    } else {
      ProfileOption::create([
        'user_id' => $user_id,
        'option_key' => $key,
        'option_value' => $value,
      ]);
    }
  }

  public static function set_or_update_profile_options_by_fields($fields, $user_id)
  {
    foreach ($fields as $key => $value) {
      self::set_or_update_profile_options($key, $value, $user_id);
    }
    return true;
  }







}
