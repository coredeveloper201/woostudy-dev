<?php

namespace App;

class Experience extends Models
{
    protected $table = 'experiences';
    protected $fillable = ['title','institute_id', 'user_id', 'from_date', 'to_date', 'continue'];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function institute()
    {
        return $this->belongsTo( Institute::class );
    }
}
