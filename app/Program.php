<?php

namespace App;


class Program extends Models
{
    protected $table = 'programs';
    protected $fillable = ['address_id', 'institute_id', 'title', 'fee', 'fee_int', 'admission_date', 'process_days', 'application_fee', 'description'];

    //Save in MongoDB
    protected static function boot()
    {
        parent::boot();
        static::created(function () {
            $data = \App\User::mongoSave();
        });

        static::updated(function () {
            $data = \App\User::mongoSave();
        });

        static::deleted(function () {
            $data = \App\User::mongoSave();
        });
    }

    // public function educationLevel()
    // {
    //     return $this->belongsTo(EducationLevel::class, 'education_level_id');
    // }

    public function educationLevels()
    {
        return $this->belongsToMany(EducationLevel::class, 'program_education_levels', 'program_id', 'education_level_id')->withPivot('institute_id')->withTimestamps();
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class, 'institute_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    public function branches()
    {
        return $this->belongsToMany(Address::class, 'program_branches', 'program_id', 'address_id')->withPivot('institute_id')->withTimestamps();
    }
}
