<?php

namespace App\Http\Controllers;

use App\Mail\InviteStudentsMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailStudentsController extends Controller
{
    private $show_view;
    private $email;
    private $static_email;

    public function __construct()
    {
        $this->show_view = false;
        $this->static_email = true;
        //$this->email = 'anjum.farrukh@gmail.com';
        $this->email = 'sudip.mediusware@gmail.com';
    }

    public function sendConnectionInvitation()
    {
        $updated = false;
        $students = User::where('role_id', 2)->inRandomOrder()->limit(8)->get();

        /*$schools = User::where('role_id', 3)->where('email_send_status', 0);
        $school = $schools->first();*/
        $school = User::find(2);
        if ($this->show_view) {
            return view('emails.schoolEmail', compact('school', 'students'));
        }

        if ($this->static_email) {
            Mail::to($this->email)->send(new InviteStudentsMail($school, $students));
        } else {
            Mail::to($school->email)->send(new InviteStudentsMail($school, $students));
        }

        if (Mail::failures()) {
        } else {
            $updated = User::where('id', $school->id)->update(['email_send_status' => 1]);
        }
    }
}
