<?php
/**
 * Created by PhpStorm.
 * User: alamincse
 * Date: 1/29/2019
 * Time: 9:20 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocialController
{

   public function fetch_facebook_feed()
   {
       $facebook = [
           'logged_in' => false,
           'data' => []
       ];

       return compact('facebook');
   }
    public function fetch_instagram_feed(){

        $instagram = [
            'logged_in' => false,
            'data' => []
        ];

        return compact('instagram');
    }

    public function fetch_linkedin_feed(){
        $linkedin = [
            'logged_in' => false,
            'data' => []
        ];

        return compact('linkedin');
    }
}
