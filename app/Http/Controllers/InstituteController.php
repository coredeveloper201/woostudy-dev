<?php

namespace App\Http\Controllers;

use App\Institute;
use App\InstituteAuthor;
use App\User;
use Illuminate\Http\Request;

class InstituteController extends Controller
{

    public function index()
    {
        return Institute::select('id', 'title')->get();
    }


    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {
        $institute = new Institute();
        $data = $request->all(); //all request data
        $data['user_id'] = auth()->id();
        $data['created_by'] = $data['user_id'];
        $institute->fill($data); //fill all data
        $institute->save(); //save to database
        if (!$institute->author) {
            (new \App\InstituteAuthor)->where('created_by', auth()->id())->update(['institute_id' => $institute->id]);
        }
        return $institute;
    }


    public function show($id)
    {
        $institute = Institute::with('educationLevel')->whereUserId($id)->first();
        return response()->json($institute);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $institute = Institute::findOrFail($id); // find model
        $data = $request->all(); //all request data
        $institute->fill($data); //fill all data
        $institute->save(); //save to database
        return $institute;
    }


    public function destroy($id)
    {
        //
    }

    public function sendEmailInvitation(Request $request)
    {
        if (!isset($request->email) || $request->email == '') {
            return response()->json(['success' => false, 'message' => auth()->user()], 400);
        }

        $mailMessage = '';
        if (!isset($request->email) || $request->email == '') {
            $mailMessage = $request->mailMessage;
        }
        $emails = explode(',', $request->email);

        \Mail::send('emails.invite', ['user' => auth()->user(), 'mailMessage' => $mailMessage], function ($m) use ($emails) {
            $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $m->to($emails)->subject(config('app.name', 'Laravel') . 'Connection Invitation');
        });
        return response()->json(['success' => true, 'message' => 'Email sent!'], 200);
    }

    public function showApprovalRequest()
    {
        $sub_accounts = User::where('parent_id', auth()->id())->get();
        return response()->json(['success' => true, 'data' => $sub_accounts], 200);
    }

    public function changeApprovalStatus($id)
    {
        $sub_account = User::where('id', $id)->where('parent_id', auth()->id())->first();
        $update = $sub_account->update(['approved' => $sub_account->approved == 0 ? 1 : 0]);
        if ($update) {
            return response()->json(['success' => true, 'data' => $sub_account], 200);
        } else {
            return response()->json(['success' => false, 'error' => 'Whoops! Account Not Found!'], 200);
        }
    }
}
