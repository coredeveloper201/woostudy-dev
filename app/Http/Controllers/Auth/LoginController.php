<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  protected $redirectTo = '/home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  /**
   * The user has been authenticated.
   *
   * @param \Illuminate\Http\Request $request
   * @param mixed $user
   * @return mixed
   */
  protected function authenticated(Request $request, $user)
  {
    if ($user->parent_id && $user->role_id == 3){
      if ($user->approved == 0) {
        Session::flash('message', 'You are not approved from the Main School.');
        Auth::logout();
        return redirect()->route('home');
      }  else {
        Session::put('isChild', true);
        Session::put('loginChildId', $user->id);
        Auth::loginUsingId($user->parent_id);
      }
    }
    else if ($user->role_id == 4) {
        Session::flash('message', 'Tutor Role is currently disabled');
        Auth::logout();
        return redirect()->route('login');
      }
  }

  public function logout(Request $request)
  {
    if (Auth::check()) {
      Auth::logout();
    }
    return redirect()->route('login');
  }
}
