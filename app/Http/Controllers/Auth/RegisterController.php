<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SubSchoolRegistrationMail;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return 'hello world';
    }

    public function showRegistrationForm()
    {
//        Session::flash('message', 'A Registration email is send to the Main School account wait for their response.');
        $roles = Role::where('id', '!=', 1)
        ->where('id', '!=', 4)  //this is temporary requirement to hide all tutor from the system
        ->get();
        return view('auth.register', compact('roles'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'role_id' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $parent_id = null;
        $approved = 1;
        $check = null;
        if ($data['role_id'] == 3) {
            $commonMails = ['gmail.com','gmail','mailinator.com','mailinator','yahoo.com','yahoo','hotmail.com','hotmail','live.com','live','outlook.com','outlook','icloud.com','icloud'];
            $emailFr = explode("@", $data['email']);
            $emailDomain = strtolower($emailFr[1]);
            if(in_array($emailDomain, $commonMails)){
                // New School
            } else {
                $conditions = array(
                    ['role_id', 3],
                    ['email', 'like', "%@" . $emailDomain],
                );
                $check = User::where($conditions)->whereNull('parent_id')->first();
                if ($check) {
                    $parent_id = $check->id;
                    $approved = 0;
                }
            }
        }
        $user = User::create([
            'name' => ucfirst($data['name']),
            'email' => $data['email'],
            'parent_id' => $parent_id,
            'approved' => $approved,
            'role_id' => $data['role_id'],
            'password' => Hash::make($data['password']),
        ]);
        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if ($user->parent_id && $user->role_id == 3) {
            $parentAccount = User::find($user->parent_id);
            Mail::to($parentAccount->email)->send(new SubSchoolRegistrationMail($user));
            Session::flash('message', 'A Registration email is send to the Main school account wait for their response.');
            Session::flash('combineSchool', 'A Registration email is send to the Main school account wait for their response.');
            Auth::logout();
            return redirect()->route('register');
        }
    }
}
