<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\SocialProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialProviderController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver( $provider )->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $socialUser = Socialite::driver( $provider )->user();

            // $user->token;

            $user = $this->checkProviderIsExists( $socialUser );

            if ($user) {
                Auth::login( $user, true );
                return redirect( '/home' );
            }


            $user = $this->findEmailIfExists( $socialUser );

            if ($user) {
                $userSocial = $this->createProvider( $socialUser, $provider, $user );
                if ($userSocial) {
                    Auth::login( $user, true );
                    //return redirect( '/home' );
                    return redirect( '/redirect-url' );
                }
            }

            session( ['socialUser' => serialize( $socialUser )] );
            session( ['provider' => $provider] );

            $roles = Role::where( 'id', '!=', 1 )->get();
            $emailInput = !$socialUser->email ? true : false;

            return redirect( '/redirect-url' )->with('emailInput', $emailInput); //For social popup
            //return View( 'auth.set-role', compact( 'roles', 'emailInput' ) );
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return View( 'auth.login', compact( 'error' ) );
        }
        /*$user = $this->findOrCreateUser( $socialUser, $provider );

        $this->findOrCreateProvider( $socialUser, $provider, $user );

        Auth::login( $user, true );
        $user = Auth::user();*/
    }

    //For social popup
    public function redirectPopup()
    {
      $logSlag = '';
      if (Auth::user()) {
          $logSlag = 1;
      }
      return View( 'auth.popup-window' )->with('logSlag', $logSlag);
    }
    //For social popup
    public function setRoleView()
    {
        $roles = Role::where( 'id', '!=', 1 )->get();
        $emailInput = session('emailInput');
        return View( 'auth.set-role', compact( 'roles', 'emailInput' ) );
    }

    private function findEmailIfExists($social)
    {
        $authUser = User::whereEmail( $social->email )->first();

        return $authUser;
    }

    private function findOrCreateUser($social, $provider)
    {
        return User::create( [
            'name' => $social->name,
            'email' => $social->email,
        ] );
    }

    private function checkProviderIsExists($social)
    {
        $social = SocialProvider::whereProviderId( $social->id )->first();
        if ($social) {
            return $social->user;
        }
    }

    private function createProvider($social, $provider, $user)
    {
        return $user->socialProviders()->create( [
            'provider_id' => $social->id,
            'token' => $social->token,
            'refresh_token' => $social->refreshToken ?? null,
            'token_expire' => $social->expiresIn ?? null,
            'token_secret' => $social->tokenSecret ?? null,
            'provider' => $provider,
        ] );

    }

    public function createUser($request, $social)
    {
        return User::create( [
            'name' => $social->name,
            'email' => $social->email ?? $request->email,
            'role_id' => $request->role_id
        ] );
    }

    public function setRole(Request $request)
    {
        if ($request->email) {
            $request->validate( [
                'role_id' => 'required',
                'email' => 'required|string|email|max:255|unique:users',
            ] );
        } else {
            $request->validate( [
                'role_id' => 'required',
            ] );
        }

        $social = unserialize( session( 'socialUser' ) );

        $provider = session( 'provider' );

        $user = $this->createUser( $request, $social );
        $userProvider = $this->createProvider( $social, $provider, $user );

        Auth::login( $user, true );

        return redirect( '/home' );
    }
}
