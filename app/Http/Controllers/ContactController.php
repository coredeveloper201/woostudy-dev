<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Subscribe;

class ContactController extends Controller
{
    public function sendMail(Request $request)
    {
        if (!isset($request->name) || $request->name=='') {
            return response()->json(['success' => false, 'message' =>  'Name is required'], 200);
        }
        if (!isset($request->email) || $request->email=='') {
            return response()->json(['success' => false, 'message' =>  'Email is required'], 200);
        }
        if (!isset($request->message) || $request->message=='') {
            return response()->json(['success' => false, 'message' =>  'Message is required'], 200);
        }
        $data = (object) [
          'name' => $request->name, 'email' => $request->email, 'message' => $request->message
        ];
        \Mail::send('emails.contact', ['data' => $data], function ($m) use ($request) {
            $m->from($request->email);
            $m->subject(config('app.name', 'Laravel').' User query');
            $m->to(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
        });
        return response()->json(['success' => true, 'message' => 'Email sent!'], 200);
    }

    public function subscribe(Request $request)
    {
        if (!isset($request->email) || $request->email=='') {
            return response()->json(['success' => false, 'message' =>  'Email is required'], 200);
        }
        $data = (object) [
          'name' => $request->name, 'email' => $request->email, 'message' => $request->message
        ];

        $sus = Subscribe::firstOrCreate(
            ['email' => $request->email],
            ['email' => $request->email]
        );

        if ($sus) {
            \Mail::send('emails.subscribe', ['data' => $data], function ($m) use ($request) {
                $m->subject(config('app.name', 'Laravel').' Email subscription confirmation');
                $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $m->to($request->email);
            });
            return response()->json(['success' => true, 'message' => 'Subscribe successfully!'], 200);
        }
        return response()->json(['success' => false, 'message' => 'Email subscription failed!'], 200);
    }
}
