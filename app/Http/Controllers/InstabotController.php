<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstabotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.instabot');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        DB::table('abcd')->insert(
            ['text' => $request->input()]
        );
        $data = array (
            'coord' =>
                array (
                    'lon' => -0.13,
                    'lat' => 51.51,
                ),
            'weather' =>
                array (
                    0 =>
                        array (
                            'id' => 300,
                            'main' => 'Drizzle',
                            'description' => 'light intensity drizzle',
                            'icon' => '09d',
                        ),
                ),
            'base' => 'stations',
            'main' =>
                array (
                    'temp' => 280.32,
                    'pressure' => 1012,
                    'humidity' => 81,
                    'temp_min' => 279.15,
                    'temp_max' => 281.15,
                ),
            'visibility' => 10000,
            'wind' =>
                array (
                    'speed' => 4.1,
                    'deg' => 80,
                ),
            'clouds' =>
                array (
                    'all' => 90,
                ),
            'dt' => 1485789600,
            'sys' =>
                array (
                    'type' => 1,
                    'id' => 5091,
                    'message' => 0.0103,
                    'country' => 'GB',
                    'sunrise' => 1485762037,
                    'sunset' => 1485794875,
                ),
            'id' => 2643743,
            'name' => 'London',
            'cod' => 200,
        );
        
        return response()->json($data, 200);
        return response()->json(['data' => $data], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function show($address)
    {
        return auth()->user()->address;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $address = $address->fill( $request->all() )->save();
        if ($address) {
            return response()->json( ['success' => 'Update successfully', 'asdf' => $request->all()] );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $address->delete();
        return response()->json( ['success' => 'deleted successfully'] );
    }


    public function get_contact_details($id)
    {
        $fields = ['country', 'state','city', 'address', 'lat', 'lng'];
        return $this->get_values_from_options_table_by_fields( $fields, $id );
    }


    public function get_social_media_details($id)
    {
        $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
        return $this->get_values_from_options_table_by_fields( $fields, $id );
    }

    public function post_contact_details()
    {
        $fields = ['country','state','city', 'address', 'lat', 'lng'];
        $this->set_or_update_profile_options_by_fields( $fields );
        return response()->json( [
            'success' => 'update information successfully'
        ] );

    }
}
