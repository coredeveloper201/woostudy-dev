<?php

namespace App\Http\Controllers;

use App\Address;
use App\Course;
use App\Education;
use App\Experience;
use App\Ielts;
use App\Institute;
use App\InstituteAuthor;
use App\Interest;
use App\Licence;
use App\Media;
use App\ProfileOption;
use App\Program;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class WebController extends Controller
{
    public function index()
    {
        return view('web.index');
    }
    public function term()
    {
        return view('web.term-of-condition');
    }

    public function policy()
    {
        return view('web.privacy-policy');
    }

    public function services()
    {
        return view('web.services');
    }

    public function jobs()
    {
        return view('web.jobs');
    }

    public function newsEvent()
    {
        return view('web.news-events');
    }

    public function howItWorks()
    {
        return view('web.index');
    }

    public function faqs()
    {
        return view('web.faqs');
    }

	public function aboutus()
    {
        return view('web.about-us');
    }

    public function contactus()
    {
        return view('web.contact-us');
    }
    public function schoolProfile()
    {
        return view('web.school-profile');
    }
    public function email()
    {
        return view('web.schoolEmail');
    }
    public function devDbUsers(Request $request)
    {
        try {
            DB::beginTransaction();

            $ROLE = isset($request->role) ? $request->role : 3;
            $bd = '';
            $rv = [];
            $rv['users'] = DB::table($bd.'addresses')->select($bd.'users.id', $bd.'users.user_name', $bd.'users.name', $bd.'users.email')
                ->leftJoin($bd.'users', $bd.'addresses.addressable_id', '=', $bd.'users.id')
                ->where($bd.'addresses.country_id', 30)
                ->where($bd.'users.role_id', $ROLE)
                ->distinct($bd.'users.id')
                ->get()->toArray();
            $rv = json_decode(json_encode($rv, true), true);

            DB::rollBack();
            return response()->json($rv, 200);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 200);
        }
    }
    public function devDbMigration()
    {
        try {
            DB::beginTransaction();

            $ROLE = 3;
            $bd = 'aaa_';
            $rv = [];
            $rv['users'] = DB::table($bd.'addresses')->select($bd.'users.*')
                ->leftJoin($bd.'users', $bd.'addresses.addressable_id', '=', $bd.'users.id')
                ->where($bd.'addresses.country_id', 30)
                ->where($bd.'users.role_id', $ROLE)
                ->distinct($bd.'users.id')
                ->paginate(30)->toArray();
            $rv = json_decode(json_encode($rv, true), true);
            foreach ($rv['users']['data'] as &$user){
                // Insert Data
                //==========================
                $userInsert = $user;
                unset($userInsert['id']);
                $userNew = User::create($userInsert);
                //==========================

                $user['address'] = DB::table($bd.'addresses')
                    ->where($bd.'addresses.country_id', 30)
                    ->where($bd.'addresses.addressable_id', $user['id'])
                    ->get()->toArray();
                $user['address'] = json_decode(json_encode($user['address'], true), true);
                // Insert Data
                //==========================
                $addressInserts = $user['address'];
                foreach ($addressInserts as &$addressInsert){
                    $addressInsert['old_id'] = $addressInsert['id'];
                    $addressInsert['addressable_id'] = $userNew->id;
                    unset($addressInsert['id']);
                }
                Address::Insert($addressInserts);
                //==========================

                $user['ielts'] = DB::table($bd.'ielts')
                    ->where($bd.'ielts.user_id', $user['id'])
                    ->get()->toArray();
                $user['ielts'] = json_decode(json_encode($user['ielts'], true), true);
                // Insert Data
                //==========================
                $ieltsInserts = $user['ielts'];
                foreach ($ieltsInserts as &$ieltsInsert){
                    $ieltsInsert['user_id'] = $userNew->id;
                    unset($ieltsInsert['id']);
                }
                Ielts::Insert($ieltsInserts);
                //==========================

                $user['interests'] = DB::table($bd.'interests')
                    ->where($bd.'interests.user_id', $user['id'])
                    ->get()->toArray();
                $user['interests'] = json_decode(json_encode($user['interests'], true), true);
                // Insert Data
                //==========================
                $interestsInserts = $user['interests'];
                foreach ($interestsInserts as &$interestsInsert){
                    $interestsInsert['user_id'] = $userNew->id;
                    unset($interestsInsert['id']);
                }
                Interest::Insert($interestsInserts);
                //==========================

                $user['profile_options'] = DB::table($bd.'profile_options')
                    ->where($bd.'profile_options.user_id', $user['id'])
                    ->get()->toArray();
                $user['profile_options'] = json_decode(json_encode($user['profile_options'], true), true);
                // Insert Data
                //==========================
                $profileInserts = $user['profile_options'];
                foreach ($profileInserts as &$profileInsert){
                    $profileInsert['user_id'] = $userNew->id;
                    unset($profileInsert['id']);
                }
                ProfileOption::Insert($profileInserts);
                //==========================

                if($user['role_id'] == 4){
                    $user['licences'] = DB::table($bd.'licences')->select($bd.'licences.*')
                        ->where($bd.'licences.user_id', $user['id'])
                        ->get()->toArray();
                    $user['licences'] = json_decode(json_encode($user['licences'], true), true);
                    // Insert Data
                    //==========================
                    $licencesInserts = $user['licences'];
                    foreach ($licencesInserts as &$licencesInsert){
                        $licencesInsert['old_id'] = $licencesInsert['id'];
                        $licencesInsert['user_id'] = $userNew->id;
                        unset($licencesInsert['id']);
                    }
                    Licence::Insert($licencesInserts);
                    //==========================
                }
                $user['courses'] = DB::table($bd.'courses')
                    ->where($bd.'courses.user_id', $user['id'])
                    ->get()->toArray();
                $user['courses'] = json_decode(json_encode($user['courses'], true), true);
                // Insert Data
                //==========================
                $coursesInserts = $user['courses'];
                foreach ($coursesInserts as &$coursesInsert){
                    if($coursesInsert['licence_id'] != null){
                        $licence_id = Licence::where('old_id', $coursesInsert['licence_id'])->get()->first();
                        if($licence_id != null){
                            $coursesInsert['licence_id'] = $licence_id->id;
                        }
                    }
                    $coursesInsert['user_id'] = $userNew->id;
                    unset($coursesInsert['id']);
                }
                Course::Insert($coursesInserts);
                //==========================

                $user['medias'] = DB::table($bd.'medias')
                    ->where($bd.'medias.mediable_id', $user['id'])
                    ->where($bd.'medias.mediable_type', 'App\User')
                    ->get()->toArray();
                $user['medias'] = json_decode(json_encode($user['medias'], true), true);
                // Insert Data
                //==========================
                $mediasInserts = $user['medias'];
                foreach ($mediasInserts as &$mediasInsert){
                    $mediasInsert['mediable_id'] = $userNew->id;
                    unset($mediasInsert['id']);
                }
                Media::Insert($mediasInserts);
                //==========================

                if($user['role_id'] == 2){
                    $user['institutes'] = DB::table($bd.'educations')->select($bd.'institutes.*')
                        ->leftJoin($bd.'institutes', $bd.'institutes.id', '=', $bd.'educations.institute_id')
                        ->where($bd.'educations.user_id', $user['id'])
                        ->get()->toArray();
                    $user['institutes'] = json_decode(json_encode($user['institutes'], true), true);
                }
                if($user['role_id'] == 3 || $user['role_id'] == 4){
                    $user['institutes'] = DB::table($bd.'institute_author_user')->select($bd.'institutes.*')
                        ->leftJoin($bd.'institutes', $bd.'institutes.id', '=', $bd.'institute_author_user.institute_id')
                        ->where($bd.'institutes.user_id', $user['id'])
                        ->get()->toArray();
                    $user['institutes'] = json_decode(json_encode($user['institutes'], true), true);
                }
                foreach ($user['institutes'] as &$institute){

                    // Insert Data
                    //==========================
                    $institutesInsert = $institute;
                    $Institute_id = Institute::where('old_id', $institutesInsert['id'])->get()->first();
                    if($Institute_id != null){
                        $instituteNew = $Institute_id;
                    } else {
                        $institutesInsert['user_id'] = $userNew->id;
                        $institutesInsert['created_by'] = $userNew->id;
                        $institutesInsert['old_id'] = $institutesInsert['id'];
                        unset($institutesInsert['id']);
                        $instituteNew = Institute::create($institutesInsert);
                    }
                    //==========================

                    if($user['role_id'] == 2){
                        $institute['educations'] = DB::table($bd.'educations')
                            ->where($bd.'educations.institute_id', $institute['id'])
                            ->where($bd.'educations.user_id', $user['id'])
                            ->get()->toArray();
                        $institute['educations'] = json_decode(json_encode($institute['educations'], true), true);
                        // Insert Data
                        //==========================
                        $educationsInserts = $institute['educations'];
                        foreach ($educationsInserts as &$educationsInsert){
                            $educationsInsert['user_id'] = $userNew->id;
                            $educationsInsert['institute_id'] = $instituteNew->id;
                            unset($educationsInsert['id']);
                        }
                        Education::Insert($educationsInserts);
                        //==========================
                    }
                    if($user['role_id'] == 3){
                        $institute['instituteAuthor'] = DB::table($bd.'institute_author_user')
                            ->where($bd.'institute_author_user.institute_id', $institute['id'])
                            ->get()->toArray();
                        $institute['instituteAuthor'] = json_decode(json_encode($institute['instituteAuthor'], true), true);
                        // Insert Data
                        //==========================
                        $instituteAuthorInserts = $institute['instituteAuthor'];
                        foreach ($instituteAuthorInserts as &$instituteAuthorInsert){
                            $instituteAuthorInsert['created_by'] = $userNew->id;
                            $instituteAuthorInsert['institute_id'] = $instituteNew->id;
                            unset($instituteAuthorInsert['id']);
                        }
                        InstituteAuthor::Insert($instituteAuthorInserts);
                        //==========================

                        $institute['programs'] = DB::table($bd.'programs')
                            ->where($bd.'programs.institute_id', $institute['id'])
                            ->get()->toArray();
                        $institute['programs'] = json_decode(json_encode($institute['programs'], true), true);
                        // Insert Data
                        //==========================
                        $programInserts = $institute['programs'];
                        foreach ($programInserts as &$programInsert){
                            if($programInsert['address_id'] != null){
                                $address_id = Address::where('old_id', $programInsert['address_id'])->get()->first();
                                if($address_id != null){
                                    $programInsert['address_id'] = $address_id->id;
                                }
                            }
                            $programInsert['institute_id'] = $instituteNew->id;
                            unset($programInsert['id']);
                        }
                        Program::Insert($programInserts);
                        //==========================
                    }
                    if($user['role_id'] == 4){
                        $institute['experiences'] = DB::table($bd.'experiences')
                            ->where($bd.'experiences.institute_id', $institute['id'])
                            ->where($bd.'experiences.user_id', $user['id'])
                            ->get()->toArray();
                        $institute['experiences'] = json_decode(json_encode($institute['experiences'], true), true);
                        // Insert Data
                        //==========================
                        $experienceInserts = $institute['experiences'];
                        foreach ($experienceInserts as &$experienceInsert){
                            $experienceInsert['user-id'] = $userNew->id;
                            $experienceInsert['institute_id'] = $instituteNew->id;
                            unset($experienceInsert['id']);
                        }
                        InstituteAuthor::Insert($experienceInserts);
                        //==========================
                    }
                }


                //\App\WsApiModel\User::mongoSaveApi($ROLE, $userNew->id);
            }

            DB::rollBack();
            return response()->json($rv, 200);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 200);
        }
    }
    public function devDbMigrationDeletePak()
    {
        try {
            DB::beginTransaction();

            $del = array(
                'medias' => [],
                'interests' => [],
                'licences' => [],
                'courses' => [],
                'experiences' => [],
                'educations' => [],
                'ielts' => [],
                'instituteAuthor' => [],
                'programs' => [],
                'institutes' => [],
                'profile_options' => [],
                'address' => [],
                'users' => [],
            );

            $bd = 'aaa_';
            $bd = '';
            $rv = [];
            $rv['users'] = DB::table($bd.'addresses')->select($bd.'users.*')
                ->leftJoin($bd.'users', $bd.'addresses.addressable_id', '=', $bd.'users.id')
                ->where($bd.'addresses.country_id', 30)
                ->paginate(1000)->toArray();
            $rv = json_decode(json_encode($rv, true), true);
            // ids for Delete
            $users = array_unique(array_column($rv['users']['data'], 'id'));
            $del['users'] = array_merge($del['users'], $users);
            foreach ($rv['users']['data'] as &$user){
                $user['address'] = DB::table($bd.'addresses')
                    ->where($bd.'addresses.country_id', 30)
                    ->where($bd.'addresses.addressable_id', $user['id'])
                    ->get()->toArray();
                $user['address'] = json_decode(json_encode($user['address'], true), true);
                // ids for Delete
                $address = array_unique(array_column($user['address'], 'id'));
                $del['address'] = array_merge($del['address'], $address);

                $user['ielts'] = DB::table($bd.'ielts')
                    ->where($bd.'ielts.user_id', $user['id'])
                    ->get()->toArray();
                $user['ielts'] = json_decode(json_encode($user['ielts'], true), true);
                // ids for Delete
                $ielts = array_unique(array_column($user['ielts'], 'id'));
                $del['ielts'] = array_merge($del['ielts'], $ielts);

                $user['interests'] = DB::table($bd.'interests')
                    ->where($bd.'interests.user_id', $user['id'])
                    ->get()->toArray();
                $user['interests'] = json_decode(json_encode($user['interests'], true), true);
                // ids for Delete
                $interests = array_unique(array_column($user['interests'], 'id'));
                $del['interests'] = array_merge($del['interests'], $interests);

                $user['profile_options'] = DB::table($bd.'profile_options')
                    ->where($bd.'profile_options.user_id', $user['id'])
                    ->get()->toArray();
                $user['profile_options'] = json_decode(json_encode($user['profile_options'], true), true);
                // ids for Delete
                $profile_options = array_unique(array_column($user['profile_options'], 'id'));
                $del['profile_options'] = array_merge($del['profile_options'], $profile_options);

                $user['courses'] = DB::table($bd.'courses')
                    ->where($bd.'courses.user_id', $user['id'])
                    ->get()->toArray();
                $user['courses'] = json_decode(json_encode($user['courses'], true), true);
                // ids for Delete
                $courses = array_unique(array_column($user['courses'], 'id'));
                $del['courses'] = array_merge($del['courses'], $courses);

                $user['medias'] = DB::table($bd.'medias')
                    ->where($bd.'medias.mediable_id', $user['id'])
                    ->where($bd.'medias.mediable_type', 'App\User')
                    ->get()->toArray();
                $user['medias'] = json_decode(json_encode($user['medias'], true), true);
                // ids for Delete
                $mediaIds = array_unique(array_column($user['medias'], 'id'));
                $del['medias'] = array_merge($del['medias'], $mediaIds);

                if($user['role_id'] == 2){
                    $user['institutes'] = DB::table($bd.'educations')->select($bd.'institutes.*')
                        ->leftJoin($bd.'institutes', $bd.'institutes.id', '=', $bd.'educations.institute_id')
                        ->where($bd.'educations.user_id', $user['id'])
                        ->get()->toArray();
                    $user['institutes'] = json_decode(json_encode($user['institutes'], true), true);
                }
                if($user['role_id'] == 3 ){
                    $user['institutes'] = DB::table($bd.'institutes')->select($bd.'institutes.*')
                        ->where($bd.'institutes.user_id', $user['id'])
                        ->get()->toArray();
                    $user['institutes'] = json_decode(json_encode($user['institutes'], true), true);
                }
                if($user['role_id'] == 4){
                    $user['licences'] = DB::table($bd.'licences')->select($bd.'licences.*')
                        ->where($bd.'licences.user_id', $user['id'])
                        ->get()->toArray();
                    $user['licences'] = json_decode(json_encode($user['licences'], true), true);
                    // ids for Delete
                    $licences = array_unique(array_column($user['licences'], 'id'));
                    $del['licences'] = array_merge($del['licences'], $licences);
                }
                if($user['role_id'] == 3 || $user['role_id'] == 4){
                    $user['institutes'] = DB::table($bd.'institute_author_user')->select($bd.'institutes.*')
                        ->leftJoin($bd.'institutes', $bd.'institutes.id', '=', $bd.'institute_author_user.institute_id')
                        ->where($bd.'institutes.user_id', $user['id'])
                        ->get()->toArray();
                    $user['institutes'] = json_decode(json_encode($user['institutes'], true), true);
                    // ids for Delete
                    $institutes = array_unique(array_column($user['institutes'], 'id'));
                    $del['institutes'] = array_merge($del['institutes'], $institutes);
                }
                foreach ($user['institutes'] as &$institute){
                    if($user['role_id'] == 2){
                        $institute['educations'] = DB::table($bd.'educations')
                            ->where($bd.'educations.institute_id', $institute['id'])
                            ->where($bd.'educations.user_id', $user['id'])
                            ->get()->toArray();
                        $institute['educations'] = json_decode(json_encode($institute['educations'], true), true);
                        // ids for Delete
                        $educations = array_unique(array_column($institute['educations'], 'id'));
                        $del['educations'] = array_merge($del['educations'], $educations);
                    }
                    if($user['role_id'] == 3){
                        $institute['instituteAuthor'] = DB::table($bd.'institute_author_user')
                            ->where($bd.'institute_author_user.institute_id', $institute['id'])
                            ->get()->toArray();
                        $institute['instituteAuthor'] = json_decode(json_encode($institute['instituteAuthor'], true), true);
                        // ids for Delete
                        $instituteAuthor = array_unique(array_column($institute['instituteAuthor'], 'id'));
                        $del['instituteAuthor'] = array_merge($del['instituteAuthor'], $instituteAuthor);

                        $institute['programs'] = DB::table($bd.'programs')
                            ->where($bd.'programs.institute_id', $institute['id'])
                            ->get()->toArray();
                        $institute['programs'] = json_decode(json_encode($institute['programs'], true), true);
                        // ids for Delete
                        $programs = array_unique(array_column($institute['programs'], 'id'));
                        $del['programs'] = array_merge($del['programs'], $programs);
                    }
                    if($user['role_id'] == 4){
                        $institute['experiences'] = DB::table($bd.'experiences')
                            ->where($bd.'experiences.institute_id', $institute['id'])
                            ->where($bd.'experiences.user_id', $user['id'])
                            ->get()->toArray();
                        $institute['experiences'] = json_decode(json_encode($institute['experiences'], true), true);
                        // ids for Delete
                        $experiences = array_unique(array_column($institute['experiences'], 'id'));
                        $del['experiences'] = array_merge($del['experiences'], $experiences);
                    }
                }
            }

/*            Media::whereIn('id', $del['medias'])->delete();
            Interest::whereIn('id', $del['interests'])->delete();
            Licence::whereIn('id', $del['licences'])->delete();
            Course::whereIn('id', $del['courses'])->delete();
            Experience::whereIn('id', $del['experiences'])->delete();
            Education::whereIn('id', $del['educations'])->delete();
            Ielts::whereIn('id', $del['ielts'])->delete();
            InstituteAuthor::whereIn('id', $del['instituteAuthor'])->delete();
            Program::whereIn('id', $del['programs'])->delete();
            Institute::whereIn('id', $del['institutes'])->delete();
            ProfileOption::whereIn('id', $del['profile_options'])->delete();
            Address::whereIn('id', $del['address'])->delete();
            User::whereIn('id', $del['users'])->delete();

            DB::rollBack();*/
            return response()->json($del, 200);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 200);
        }
    }
}
