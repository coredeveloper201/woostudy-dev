<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator as Faker;
Use Faker\Factory;

class FakerController extends Controller
{


    public function runUser($loop){
        try {
            $this->userData($loop);
            $msg = 'Success';
        } catch(\Exception $e) {
            $msg = $e->getMessage();
        }
      return $msg;
    }

    public function userData($loop){
        $faker = Factory::create();
        for ($i = 0; $i < $loop; $i++) {
            $user = \App\User::create([
              'name' => $faker->name,
              'role_id' => 2,
              'user_name' => $faker->unique()->userName,
              'email' => $faker->unique()->safeEmail,
              'email_verified_at' => now(),
              'password' => bcrypt("123456"),
              'faker_status' => 1,
              'remember_token' => str_random(10),
            ]);

            \App\Address::create([
              'country_id' => 182,
              'state_id' => null,
              'city' => $faker->city,
              'address' => $faker->address,
              'lat' => null,
              'lng' => null,
              'addressable_id' => $user->id,
              'addressable_type' => "App\User",
            ]);

            \App\WsApiModel\User::mongoSaveApi(2, $user->id);
        }

      return true;
    }
}
