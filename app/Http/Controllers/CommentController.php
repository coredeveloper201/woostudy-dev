<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function store(Request $request)
    {
      if(isset($request->body) && $request->body!='') {
          //$post = Post::select('posts.user_id')->join('friendships', 'posts')->find($request->post_id);
          //$post = Post::whereUserId( auth()->id() )->whereId( $request->post_id )->first();
          $post = Post::whereId( $request->post_id )->first();
          $checkFriendship = \DB::table('friendships')->where(function ($query) {
              $query->where('sender_id', auth()->id())
                    ->orWhere('recipient_id', auth()->id());
          })->where(function ($query) use($post) {
              $query->where('sender_id', $post->user_id)
                    ->orWhere('recipient_id', $post->user_id);
          })->first();

          $message = '';
          if (!empty($checkFriendship) || $post->user_id==auth()->id()) {
              $comment = $post->comments()->create( ['body' => $request->body, 'user_id' => auth()->id()] );
              $comment->user;

              $message = 'Comment posted successfully';
          } else {
              $comment = '';
              $message = 'You are not Authorized to comment this post.';
          }
          return response()->json(['message' => $message, 'comment' => $comment]);
      }
      return response()->json(['message' => 'Comment not found', 'comment' => '']);
    }


    public function update(Request $request, Comment $comment)
    {
        $comment->message = $request->message;
        $comment->update();

        return response()->json( ['message' => 'update successfully'] );
    }


    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response()->json( ['message' => 'Delete successfully'] );
    }
}
