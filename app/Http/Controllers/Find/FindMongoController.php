<?php

namespace App\Http\Controllers\Find;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FindMongoController extends Controller
{
    public function findStudent(Request $request)
    {
        $sql = \DB::connection('mongodb')->collection('users');
        if ($request->region!='') {
            $sql->where('address.country.region_id', $request->region);
        }
        if ($request->country!='') {
            $sql->where('address.country_id', $request->country);
        }
        if ($request->state!='') {
            $sql->where('address.state_id', $request->state);
        }
        if ($request->program!='') {
            $sql->where('educations.title', $request->program);
        }
        if ($request->score!='') {
            $sql->where('educations.percentage', $request->score);
        }
        if ($request->grade!='') {
            $sql->where('educations.grade', $request->grade);
        }
        if ($request->gpa!='') {
            $sql->where('educations.gpa', $request->gpa);
        }
        // if ($request->distance!='') {
        //     $radius = (double)$request->distance;
        //     $lat = (float)request()->lat;
        //     $lng = (float)request()->lng;
        //     //$unit = 6378.10;

        //     $sql->where('location', 'near', [
        //         '$geometry' => [
        //             'type' => 'Point',
        //             'coordinates' => [$lat, $lng],
        //         ],
        //         '$maxDistance' => $radius,
        //     ]);
        // }

        //Ignore current Friendlist
        $user = User::find(auth()->id());
        $friendShip = $user->getAllFriendships()->toArray();
        $frIds = [];
        if (!empty($friendShip)) {
            $friendSender = array_column($friendShip, 'sender_id');
            $friendReceiver = array_column($friendShip, 'recipient_id');
            $frIds = array_unique(array_merge($friendSender, $friendReceiver));
        }
        if (!empty($frIds)) {
            $sql->whereNotIn('id', $frIds);
        }

        $student = $sql->where('role_id', '=', 2)->where('id', '!=', auth()->user()->id)->paginate( 8 );

        return $student;
    }

    public function findTutor(Request $request)
    {
        $sql = \DB::connection('mongodb')->collection('users');
        if ($request->region!='') {
            $sql->where('address.country.region_id', $request->region);
        }
        if ($request->country!='') {
            $sql->where('address.country_id', $request->country);
        }
        if ($request->state!='') {
            $sql->where('address.state_id', $request->state);
        }
        if ($request->title!='') {
            $sql->where('name', $request->title);
        }
        if ($request->course!='') {
            $sql->where('courses.title', $request->course);
        }
        if ($request->interest!='') {
            $sql->where('interests.title', $request->interest);
        }
        // if ($request->distance!='') {
        //     $radius = (double)$request->distance;
        //     $lat = (float)request()->lat;
        //     $lng = (float)request()->lng;
        //     //$unit = 6378.10;

        //     $sql->where('location', 'near', [
        //         '$geometry' => [
        //             'type' => 'Point',
        //             'coordinates' => [$lat, $lng],
        //         ],
        //         '$maxDistance' => $radius,
        //     ]);
        // }

        //Ignore current Friendlist
        $user = User::find(auth()->id());
        $friendShip = $user->getAllFriendships()->toArray();
        $frIds = [];
        if (!empty($friendShip)) {
            $friendSender = array_column($friendShip, 'sender_id');
            $friendReceiver = array_column($friendShip, 'recipient_id');
            $frIds = array_unique(array_merge($friendSender, $friendReceiver));
        }
        if (!empty($frIds)) {
            $sql->whereNotIn('id', $frIds);
        }

        $tutor = $sql->where('role_id', '=', 4)->where('id', '!=', auth()->user()->id)->paginate( 8 );
        return $tutor;
    }

    public function findInstitute(Request $request)
    {
        $sql = \DB::connection('mongodb')->collection('users');
        if ($request->region!='') {
            $sql->where('address.country.region_id', $request->region);
        }
        if ($request->country!='') {
            $sql->where('address.country_id', $request->country);
        }
        if ($request->state!='') {
            $sql->where('address.state_id', $request->state);
        }
        if ($request->institute!='') {
            $sql->where('institute.title', $request->institute);
        }
        if ($request->role!='') {
            $sql->where('institute.author.institute_role_id', $request->role);
        }
        if ($request->department!='') {
            $sql->where('institute.author.department_id', $request->department);
        }
        if ($request->title!='') {
            $sql->where('institute.program.title', $request->title);
        }
        if (is_array($request->range)) {
            $sql->whereBetween('institute.programs.fee', (array) $request->range);
        }

        if ($request->distance!='') {
            // $radius = (double)$request->distance;
            // $lat = (float)request()->lat;
            // $lng = -(float)request()->lng;
            // //$unit = 6378.10;

            // $sql->where('address', 'near', [
            //     '$geometry' => [
            //         'type' => 'Point',
            //         'coordinates' => [$lat, $lng],
            //     ],
            //     '$maxDistance' => $radius,
            // ]);

            //$sql->aggregate('$geoNear', ['near' => [-118.09771, 33.89244], "distanceField" => "distance", "spherical" => true]);

        }

        //Ignore current Friendlist
        $user = User::find(auth()->id());
        $friendShip = $user->getAllFriendships()->toArray();
        $frIds = [];
        if (!empty($friendShip)) {
            $friendSender = array_column($friendShip, 'sender_id');
            $friendReceiver = array_column($friendShip, 'recipient_id');
            $frIds = array_unique(array_merge($friendSender, $friendReceiver));
        }
        if (!empty($frIds)) {
            $sql->whereNotIn('id', $frIds);
        }

        $institute = $sql->where('role_id', '=', 3)->where('id', '!=', auth()->user()->id)->paginate( 8 );
        return $institute;
    }

    public function mutuals(Request $request)
    {
        $sql = \DB::connection('mongodb')->collection('users');
        if ($request->region!='') {
            $sql->where('address.country.region_id', $request->region);
        }
        if ($request->country!='') {
            $sql->where('address.country_id', $request->country);
        }
        if ($request->state!='') {
            $sql->where('address.state_id', $request->state);
        }

        if ($request->common!='') {
            $sql->where('name', $request->common);
        }
        if ($request->gender!='') {
            $sql->where('profile_options.option_key', 'gender');
            $sql->where('profile_options.option_value', $request->gender);
        }
        if ($request->key!='') {
            $sql->where([
                'name' => '%'.$request->key.'%',
                'institutes.title' => '%'.$request->key.'%',
            ]);
        }
        if ($request->school!='') {
            $sql->where('institutes.title', $request->school);
        }

        // if ($request->distance!='') {
        //     $radius = (double)$request->distance;
        //     $lat = (float)request()->lat;
        //     $lng = (float)request()->lng;
        //     //$unit = 6378.10;

        //     $sql->where('address', 'near', [
        //         '$geometry' => [
        //             'type' => 'Point',
        //             'coordinates' => [$lat, $lng],
        //         ],
        //         '$maxDistance' => $radius,
        //     ]);
        // }

        $user = User::find(auth()->id());
        $friendOfFriend = $user->getFriendsOfFriends()->toArray();
        $frIds = array_column($friendOfFriend, 'id');

        $mutuals = $sql->whereIn('id' , $frIds)->where('id', '!=', auth()->user()->id)->paginate( 8 );
        return $mutuals;
    }
}
