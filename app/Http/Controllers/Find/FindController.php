<?php

namespace App\Http\Controllers\Find;

use App\Filters\FindStudentFilter;
use App\Filters\FindTutorFilter;
use App\Filters\FindInstituteFilter;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FindController extends Controller
{
    public function findStudent(FindStudentFilter $filter)
    {
        $student = User::select('users.*', 'friendships.id AS fId')
        ->with( ['address', 'lastEducation.institute', 'lastIelts', 'profile_options' => function ($q) {
            $q->whereIn( 'option_key', ['date_of_birth' ,'facebook_url', 'twitter_url', 'instragram_url', 'google_url'] );
        }])
        ->leftJoin('friendships', function ($join) {
            $join->on('users.id', 'friendships.sender_id');
            $join->orOn('users.id', 'friendships.recipient_id');
            $join->where( function ($q) {
              $q->where( function ($q) {
                  $q->where('friendships.sender_id', auth()->user()->id )
                  ->orWhere('friendships.recipient_id', auth()->user()->id );
              });
          });
        })->whereRoleId( 2 )->whereNull('friendships.id')->where('users.id', '!=', auth()->user()->id)->filter( $filter )->paginate( 8 );

        return $student;
    }

    public function findTutor(FindTutorFilter $filter)
    {
        $tutor = User::select('users.*', 'friendships.id AS fId')
        ->with( ['address', 'courses.licence', 'experiences' => function ($q) {
            $q->orderBy( 'from_date' );
        }])
        ->leftJoin('friendships', function ($join) {
            $join->on('users.id', 'friendships.sender_id');
            $join->orOn('users.id', 'friendships.recipient_id');
            $join->where( function ($q) {
              $q->where( function ($q) {
                  $q->where('friendships.sender_id', auth()->user()->id )
                  ->orWhere('friendships.recipient_id', auth()->user()->id );
              });
          });
        })->whereRoleId( 4 )->whereNull('friendships.id')->where('users.id', '!=', auth()->user()->id)->filter( $filter )->paginate( 8 );

        return $tutor;
    }

    public function findInstitute(FindInstituteFilter $filter)
    {
        $tutor = User::select('users.*', 'friendships.id AS fId')
        ->with( ['institute.programs', 'address'])
        ->leftJoin('friendships', function ($join) {
            $join->on('users.id', 'friendships.sender_id');
            $join->orOn('users.id', 'friendships.recipient_id');
            $join->where( function ($q) {
              $q->where( function ($q) {
                  $q->where('friendships.sender_id', auth()->user()->id )
                  ->orWhere('friendships.recipient_id', auth()->user()->id );
              });
          });
        })->whereRoleId( 3 )->whereNull('friendships.id')->where('users.id', '!=', auth()->user()->id)->filter( $filter )->paginate( 8 );

        return $tutor;
    }

    public function mutuals(Request $request)
    {
        $user = User::find(auth()->id());
        $fr = $user->getFriendsOfFriends()->toArray();
        $frIds = array_column($fr, 'id');

        $input = [];
        $input['name'] = isset($request->name) ? $request->name : '';
        $page = isset($request->page) ? $request->page : 1;
        $input['region'] = isset($request->region) ? $request->region : 0;
        $input['common'] = isset($request->common) ? $request->common : '';
        $input['lat'] = isset($request->common) ? $request->common : 0;
        $input['gender'] = isset($request->gender) ? $request->gender : '';
        $input['key'] = isset($request->key) ? $request->key : '';
        $input['school'] = isset($request->school) ? $request->school : '';
        $input['distance'] = isset($request->distance) ? $request->distance : '';

        $users = User::select('users.*', 'countries.region_id')
            ->leftjoin('addresses', 'addresses.addressable_id', 'users.id')
            ->leftjoin('countries', 'countries.id', 'addresses.country_id')
            ->leftjoin('institutes', 'institutes.user_id', 'users.id')
            ->leftjoin('profile_options', 'profile_options.user_id', 'users.id')
            ->leftjoin('interests', 'interests.user_id', 'users.id')
            ->where(function ($query) use ($input) {
                if($input['name'] != ''){
                    $query->where('users.name', 'Like', '%'.$input['name'].'%');
                }

                if($input['region'] != 0){
                    $query->where('countries.region_id', $input['region']);
                }

                if($input['school'] != ''){
                    $query->where('institutes.title', 'LIKE' ,  "%".$input['school']."%");
                }

                if($input['gender'] != ''){
                    $query->where([
                        'profile_options.option_key' => 'gender',
                        'profile_options.option_value' => $input['gender']
                    ]);
                }
                if($input['key'] != ''){
                    $query->where([
                        'users.name' => '%'.$input['key'].'%',
                        'institutes.title' => '%'.$input['key'].'%',
                    ]);
                }

                if($input['common'] != ''){
                    $query->where('interests.title', 'LIKE' ,  "%".$input['common']."%");
                }

            })->whereIn('users.id' , $frIds)->distinct('users.id')->with('address' , 'ielts' , 'profile_options' , 'lastEducation.institute')->paginate(8);

        return $users;
    }
}
