<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\MediaController;
use App\Http\Requests\MediaImageRequest;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UserCoverController extends Controller
{
    public function store(MediaImageRequest $request)
    {
        $image = (new MediaController())->ImageUpload($request);
        if (auth()->user()->avater) {
            $ds = Storage::delete('/public/uploads/images/' . auth()->user()->getOriginal('cover'));
        }
        $updated = User::find(auth()->id())->update(['cover' => $image['filename']]);

        if ($updated) {
            return response()->json([
                'success' => 'Image upload successfully',
                'url' => User::find(auth()->id())->cover
            ]);
        }
    }
}
