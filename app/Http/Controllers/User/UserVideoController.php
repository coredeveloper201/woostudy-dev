<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\MediaController;
use App\Http\Requests\MediaVideoRequest;
use App\User;
use App\Http\Controllers\Controller;

class UserVideoController extends Controller
{
    public function store(MediaVideoRequest $request)
    {
        $videos = (new MediaController())->videoUpload( $request );
        auth()->user()->medias()->create( $videos );

        return response()->json( [
            'success' => 'Video Details update successfully',
        ] );
    }

    public function show($id)
    {
        $user = User::find( $id );
        return $user->videos;
    }

    public function destroy($id)
    {
        $videos = (new MediaController())->delete( [$id],'videos/' );

        return response()->json( [
            'success' => 'Video Deleted successfully',
            'status' => $videos,
        ] );
    }
}
