<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\LiecenceRequest;
use App\Licence;

class UserLicenceController extends Controller
{
    public function show($id)
    {
        return Licence::whereUserId( $id )->get();
    }

    public function store(LiecenceRequest $request)
    {
        $licence = new Licence();
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $licence = $licence->fill( $data )->save();

        if ($licence) {
            return response()->json( ['success' => 'Licence create successfully'] );
        }
    }

    public function update(LiecenceRequest $request, $id)
    {
        $licence = Licence::whereUserId( auth()->id() )->find( $id );
        $licence = $licence->fill( $request->all() )->save();

        if ($licence) {
            return response()->json( ['success' => 'Licence update successfully'] );
        }
    }

    public function destroy($id)
    {
        $licence = Licence::whereUserId( auth()->id() )->find( $id );
        $licence = $licence->delete();

        if ($licence) {
            return response()->json( ['success' => 'Licence delete successfully'] );
        }
    }
}
