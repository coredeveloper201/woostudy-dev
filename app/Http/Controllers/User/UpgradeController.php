<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Upgrade;
use App\User;
use App\Setting;
use Illuminate\Contracts\Encryption\DecryptException;
use CreditCard;
use Validator;
use Mail;

class UpgradeController extends Controller
{
    public function upgrade(Request $request) {
        $validator = Validator::make($request->all(), [
            'paymentMethod' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 400);
        }

        if ($request->paymentMethod == 'stripe') {
            $validator = Validator::make($request->all(), [
                'number' => 'required|max:191|min:6',
                'name' => 'required|max:191',
                'expiry' => 'required|date_format:"m/y"',
                'cvc' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()], 400);
            }

            $validator->after(function ($validator) use($request) {
                // Card Number Check
                $card = CreditCard::validCreditCard($request->number);
                if (!$card['valid'])
                    $validator->errors()->add('number', 'Invalid Card Number');

                // CVC Check
                $validCvc = CreditCard::validCvc($request->cvc, $card['type']);
                if (!$validCvc)
                    $validator->errors()->add('cvc', 'Invalid CVC');

                // Expiry Check
                $tmp  = explode('/', $request->expiry);
                $validDate = CreditCard::validDate('20'.$tmp[1], $tmp[0]);
                if (!$validDate)
                    $validator->errors()->add('expiry', 'Invalid Expiry');
            });

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()], 400);
            }

            $setting = Setting::find(1);
            $amount = $setting->upgrade_amount;

            $upgrade['amount'] = $amount;
            $upgrade['payment_date'] = date('Y-m-d');

            $card = CreditCard::validCreditCard($request->number);
            $upgrade['payment_type'] = $card['type'];
            $upgrade['card_number'] = encrypt($request->number);
            $upgrade['card_full_name'] = encrypt($request->name);
            $upgrade['card_expire'] = encrypt($request->expiry);
            $upgrade['card_cvc'] = encrypt($request->cvc);

            //Stripe::Start
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe = \Stripe\Charge::create([
                "amount" => (int)($amount*100),
                "currency" => "usd",
                "source" => $request->stripeToken, // obtained with Stripe.js
                "description" => "Charge for Account Upgrade",
                'capture' => true
            ]);
            $stripe_payment_info = null;
            if ( $stripe ) {
                $stripe_payment_info = array(
                    'id' => $stripe->id,
                    'object' => $stripe->object,
                    'amount' => $stripe->amount,
                    'amount_refunded' => $stripe->amount_refunded,
                    'balance_transaction' => $stripe->balance_transaction,
                    'captured' => $stripe->captured,
                    'currency' => $stripe->currency,
                    'description' => $stripe->description,
                    'receipt_url' => $stripe->receipt_url,
                    'refunds' => $stripe->refunds,
                    'source' => $stripe->source,
                    'status' => $stripe->source,
                );
                $stripe_payment_info = json_encode($stripe_payment_info, true);
            }
            $upgrade['payment_info'] = $stripe_payment_info;
            //Stripe::End

            $authUser = auth()->user();
            $upgrade['user_id'] = $authUser->id;
            $upgrades =  Upgrade::create( $upgrade );
            if ($upgrades) {
                //User Table Updated...
                $userData = User::find( $authUser->id );
                $userData->update(['premium_user' => 1]);

                Mail::send('emails.upgrade', ['receiver' => 'user', 'user' => $authUser], function ($message) use ($authUser) {
                    $message->subject('Your Account Successfully Upgraded - '.env('APP_NAME'));
                    $message->to($authUser->email, $authUser->name);
                });

                Mail::send('emails.upgrade', ['receiver' => 'admin', 'user' => $authUser], function ($message) use ($authUser) {
                    $message->subject('New Account Upgraded - '.$authUser->name);
                    $message->to(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                });

                return response()->json( ['success' => true, 'message' => 'Account Upgraded successfully.', 'data' => $upgrade], 200 );
            } else {
                return response()->json( ['success' => false, 'message' => 'Upgrade failed!', 'data' => ''], 200 );
            }
        } else {
            //Nothing...
        }
    }
}
