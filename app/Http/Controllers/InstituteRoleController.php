<?php

namespace App\Http\Controllers;

use App\InstituteRole;
use Illuminate\Http\Request;

class InstituteRoleController extends Controller
{
    public function index()
    {
        return InstituteRole::all();
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
