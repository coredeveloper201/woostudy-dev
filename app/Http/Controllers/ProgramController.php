<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (isset(auth()->user()->institute)) {
          $programs = Program::whereInstituteId( auth()->user()->institute->id )->with( 'educationLevels', 'address', 'branches' )->orderBy('admission_date', 'DESC')->get();
          return response()->json( ['success' => true, 'programs' => $programs] );
      }
      return response()->json( ['success' => false, 'programs' => []] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['fee_int'] = $request->fee ? preg_replace('/[^0-9.]+/', '', $request->fee) : '';
        $data['institute_id'] = auth()->user()->institute ? auth()->user()->institute->id : '';
        $program = Program::create( $data );
        $educationLevelPivotData = [];
        foreach ($request->education_levels as $level) {
            $educationLevelPivotData[$level] = [
                'institute_id' => $data['institute_id']
            ];
        }
        $branchPivotData = [];
        foreach ($request->branches as $branch) {
            $branchPivotData[$branch] = [
                'institute_id' => $data['institute_id']
            ];
        }
        $program->branches()->sync($branchPivotData);
        $program = $program->educationLevels()->sync($educationLevelPivotData);
        return $program;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Program $program
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $program)
    {

        $program = Program::findOrFail( $program );
        $data = $request->all();
        $data['fee_int'] = $request->fee ? preg_replace('/[^0-9.]+/', '', $request->fee) : '';
        $data['institute_id'] = auth()->user()->institute ? auth()->user()->institute->id : '';
        $educationLevelPivotData = [];
        foreach ($request->education_levels as $level) {
            $educationLevelPivotData[$level] = [
                'institute_id' => $data['institute_id']
            ];
        }
        $branchPivotData = [];
        foreach ($request->branches as $branch) {
            $branchPivotData[$branch] = [
                'institute_id' => $data['institute_id']
            ];
        }
        $program->branches()->sync($branchPivotData);
        $updated = $program->educationLevels()->sync($educationLevelPivotData);
        $program = $program->fill( $data )->save();
        if ($program) {
            return response()->json( ['success' => 'update successfully'] );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($program)
    {
        $program = Program::find( $program );
        $program->delete();
        return response()->json( ['success' => 'deleted successfully'] );
    }
}
