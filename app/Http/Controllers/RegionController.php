<?php

namespace App\Http\Controllers;

use App\Region;

class RegionController extends Controller
{
    public function index()
    {
        return Region::all();
    }
}
