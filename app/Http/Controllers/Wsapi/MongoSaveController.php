<?php

namespace App\Http\Controllers\Wsapi;

use App\WsApiModel\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MongoSaveController extends Controller
{
    public function mongoSave($role_id = 3, $user_id = 2) {
        return User::mongoSaveApi($role_id, $user_id);
    }
}
