<?php

namespace App;


class CourseServiceMode extends Models
{
    protected $table = 'course_service_mode';
    protected $fillable = ['course_id', 'mode'];

    public function course()
    {
        return $this->belongsTo( Course::class );
    }
}
