<?php

namespace App;

class Setting extends Models
{
    protected $table = 'settings';
    protected $fillable = ['upgrade_amount'];
}
