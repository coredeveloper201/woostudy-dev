<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramLevel extends Model
{
    protected $fillable = ['program_id', 'education_level_id'];
}
