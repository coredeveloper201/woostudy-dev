<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InviteStudentEmailCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invite-student';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Email Sending to School';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $app = app('App\Http\Controllers\EmailStudentsController')->sendConnectionInvitation();
        $this->info('invite-student Command Run successfully!');
    }
}
