<?php

namespace App\WsApiModel;
use Illuminate\Database\Eloquent\Model;


class InstituteAuthor extends Model
{
    protected $table = 'institute_author_user';
}
