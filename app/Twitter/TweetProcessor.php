<?php
namespace App\Twitter;
use App\SocialProvider;
use App\Tweet;
use Carbon\Carbon;
use DateTime;
use Mockery\Exception;
use Thujohn\Twitter\Twitter;
use App\Models\Ner\NameEntityRecongizer;
use AYLIEN;

class TweetProcessor
{
    private $hashtags;
    private $symbols;
    private $userMentions;
    private $potentialReach;
    private $potentialImpression;
    private $followers;
    private $friends;

    private $impressionGraphData;
    private $tweetsTimeLineData;

    private $noOfTweets;
    private $contributers;
    private $timeInterval;
    private $extraAggrates;
    private $text;
    private $response;
    private $query;


    public function testGeoPoint(){
        $response = Twitter::getGeoSearch(['lat' => "30.375321",
            'long' => "69.34511599999996",
            'count' => 200,
            'format' => 'array']);

        dd($response);
    }

    public function searchFromDB($query){
        $this->query = $query;

        if($query != null ){
            $rows = Tweet::where('search_query','like',$query)
                ->where('is_enriched','=',"Yes")
                ->latest()
                ->get();
        }
        else {
            $rows = Tweet::where('is_enriched','=',"Yes")
                ->take(1000)
                ->latest()
                ->get();
        }

        return $this->fetchTweetsFromRows($rows);
    }

    public function fetchTimeLineTweetsFromDB($userID){
        $rows = Tweet::where('user_id','=',$userID)->whereNull('search_query')->get();
        //dd($rows->toArray());
        return $this->fetchTweetsFromRows($rows);
    }

    private function fetchTweetsFromRows($rows){
        //dd($query);
        $this->response = array();
        $tweets = array();
        $entities = array();
        $sentiments = array();
        $tweetsIds = array();
        foreach($rows as $row){
            $tweetIds[] = $row->tweet_id;

            $tweets[] = json_decode($row->tweet, true);
            $decodedEntites = json_decode($row->entities, true);
            $sentiment = $row->predictied_sentiment;

            if( $sentiment != ""){
                if( isset( $sentiments[$sentiment]))
                {
                    $sentiments[$sentiment] = $sentiments[$sentiment] + 1;
                }
                else
                {
                    $sentiments[$sentiment] = 0;
                }
            }

            if($decodedEntites != null ){
               if(is_array($decodedEntites)){
                   if(count($decodedEntites) > 0)
                   {
                       try{
                           foreach($decodedEntites as $key=>$entity){
                               if(is_array($entity)){
                                   foreach($entity as $item){
                                       $entities[$key][] = $item;
                                   }
                               }
                               else
                               {
                                   $entities[$key][] = $entity;
                               }

                           }
                       }
                       catch (Exception $ex){
                           print($ex);
                           continue;
                       }
                   }
               }
            }

        }
        //dd($sentiments);

        $tweets= $this->extractTwitterInformation($tweets);
        $this->calculateMetrics( $tweets );

        $potentialReach = $this->potentialReach;
        $potentialImpression = $this->potentialImpression;
        $followers = $this->followers;
        $friends = $this->friends;

        $userMentions = generateTagCloud($this->userMentions);
        $hashtags = generateTagCloud($this->hashtags);

        //dd($this->hashtags);

        $impressionGraphData = generateBarChartGraphData($this->impressionGraphData, true);
        $tweetsTimeLineData = generateBarChartGraphData($this->tweetsTimeLineData);
        $contributers = $this->contributers;
        $noOfTweets = $this->noOfTweets;
        $timeInterval = $this->timeInterval;
        $text = $this->text;

        //$textForAnalysis = implode(".", $text);
        //$entites = NameEntityRecongizer::getNamedEntities($textForAnalysis);
        //dd($entites);

        $locationsArr = $this->extractEntityCount($entities,"LOCATION");
        $personsArr = $this->extractEntityCount($entities,"PERSON");
        $organizationsArr = $this->extractEntityCount($entities,"ORGANIZATION");

        $colors = ['positive'=>'green','negative'=>'red', 'neutral'=>'grey'];

        $sentimentsGraphData = generatePieChartGraphData($sentiments, $colors);

        // dd($sentimentsGraphData);


        $locations = createTagCloudArray($locationsArr, LOCATION_COLOR);
        $persons =  createTagCloudArray($personsArr, PERSON_COLOR);
        $organizations = createTagCloudArray($organizationsArr, ORGANIZATION_COLOR);
        $entitesArr = array_merge($locations, $persons, $organizations);

        $entities = json_encode($entitesArr);

        $q = $this->query;

        return compact('potentialReach',
            'potentialImpression',
            'followers',
            'sentimentsGraphData',
            'entities',
            'text',
            'q',
            'tweetIds',
            'friends',
            'userMentions',
            'hashtags',
            'impressionGraphData',
            'tweetsTimeLineData',
            'contributers',
            'noOfTweets',
            'tweets',
            'timeInterval'
        );
    }

    public function queryDbByIds($ids){
        $rows = Tweet::whereIn('tweet_id', $ids)
            ->where('is_enriched','like','Yes')
            ->get();

        $conceptsArr = array();
        $entities = array();
        $categories = array();
        $languages = array();
        $sources = array();
        $tweets = array();
        $influencers = array();

        foreach($rows as $row){

            $dTweet = json_decode($row->tweet);

            //Calculating Influencer
            if( isset($dTweet->user ) ){
                $username = $dTweet->user->name . " ( ". $dTweet->user->screen_name. " ) ";
                $followercount = $dTweet->user->followers_count;

                if(!isset($influencers[$username])){
                    $influencers[$username] = $followercount;
                }
            }


            $language = $dTweet->lang;
            if(!isset($languages[$language])){
                $languages[$language] = 1;
            }
            else {
                $languages[$language] = $languages[$language] + 1;
            }

            $source = $dTweet->source;
            if(!isset($sources[$source])){
                $sources[$source] = 1;
            }
            else {
                $sources[$source] = $sources[$source] + 1;
            }

            $tweets[$row->tweet_id]['sentiment'] = $row->predictied_sentiment;

            $rawEntites = json_decode($row->entities);
            if( is_object($rawEntites) && is_object( $rawEntites->entities)  ) {
                $tweets[$row->tweet_id]['entities'] = $rawEntites->entities;
                $this->createKeyValuesMap($rawEntites->entities, $entities);
            }

            $rawClassifications = json_decode($row->classifications);
            if( is_object($rawClassifications) && is_object( $rawClassifications->categories)  ) {
                $tweets[$row->tweet_id]['classifications'] = $rawClassifications->categories;
                $this->createKeyValuesMap($rawClassifications->categories, $categories);
            }

            $rawConcepts = json_decode($row->concepts);
            if( is_object($rawConcepts) && is_object( $rawConcepts->concepts)  ) {

                $concepts = $rawConcepts->concepts;
                $cleanConcepts = [];
                $titles = [];
                foreach($concepts as $key => $objProp) {
                    $title = getLastPartOfURL($key);
                    $deepCategories = array();
                    foreach($objProp->types as $type ) {
                        $deepCategories[] = getLastPartOfURL($type);
                    }
                    if($title == "RT_(TV_network)"){
                        continue;
                    }
                    $cleanConcepts[$title] = array_unique($deepCategories);
                    $titles[] = $title;
                }
                if(count($titles) > 0){
                    foreach($titles as $title){
                        if($title == "RT_(TV_network)"){
                            continue;
                        }
                        if(!isset($conceptsArr[$title])){
                            $conceptsArr[$title] = 1;
                        }
                        else {
                            $conceptsArr[$title] = $conceptsArr[$title] + 1;
                        }
                    }
                }

                $tweets[$row->tweet_id]['concepts'] = $cleanConcepts;
            }

        }
        //arsort($entities['keyword']);
        $this->sortTheFields($entities);
        $this->sortTheFields($categories);
        arsort($conceptsArr);
        arsort($sources);
        arsort($languages);
        arsort($influencers);
        $influencers = array_splice($influencers, 0, BAR_CHART_LIMIT);
        $conceptsArr = array_splice($conceptsArr, 0, BAR_CHART_LIMIT);
        $chartData = array_merge($entities, $categories);
        $chartData['concepts'] = generateHighChartBarJson($conceptsArr);
        $chartData['sources'] = generateHighChartBarJson($sources);
        $chartData['languages'] = generateHighChartBarJson($languages);
        $chartData['influencers'] = generateHighChartBarJson($influencers);
        $finalArr['tweets'] = $tweets;
        $finalArr['chartData'] = $chartData;

        return $finalArr;
    }

    private function sortTheFields(&$arr){
        foreach($arr as $key => $sub){
            arsort($arr[$key]);
            $arr[$key] = array_splice($arr[$key], 0, BAR_CHART_LIMIT);
            $arr[$key] = generateHighChartBarJson($arr[$key]);
        }
    }

    private function createKeyValuesMap($arr, &$keyValueMap){
        if($arr != null ){
            foreach($arr as $key => $values){
                foreach($values as $value){
                    if(!isset($keyValueMap[$key][$value])){
                        $keyValueMap[$key][$value] = 1;
                    }
                    else {
                        $keyValueMap[$key][$value] = $keyValueMap[$key][$value] + 1;
                    }
                }
            }
        }
        return $keyValueMap;
    }

    private function sortTheyArray($arr, &$keyValueMap){
        if($arr != null ){
            foreach($arr as $key => $value){
                ksort($value);
            }
        }
        return $keyValueMap;
    }

    public function searchFromDBMinimalData($query){
        $rows = Tweet::where('search_query','like',$query)->get();
        //dd($query);
        $this->response = array();
        $tweets = array();
        $entities = array();
        $sentiments = array();
        foreach($rows as $row){
            $tweets[] = json_decode($row->tweet, true);
            $decodedEntites = json_decode($row->entities, true);
            $sentiment = $row->predictied_sentiment;

            if( $sentiment != ""){
                if( isset( $sentiments[$sentiment]))
                {
                    $sentiments[$sentiment] = $sentiments[$sentiment] + 1;
                }
                else
                {
                    $sentiments[$sentiment] = 0;
                }
            }

            if(is_array($decodedEntites) && count($decodedEntites) > 0)
            {
                foreach($decodedEntites as $key=>$entity){
                    if( is_array($entity)){
                        foreach($entity as $item){
                            $entities[$key][] = $item;
                        }
                    }
                    else {
                        $entities[$key] = $entity;
                    }


                }
            }
        }
        //dd($sentiments);

        $tweets= $this->extractTwitterInformation($tweets);
        $this->calculateMetrics($tweets , $query);


        $locationsArr = $this->extractEntityCount($entities,"LOCATION");
        $personsArr = $this->extractEntityCount($entities,"PERSON");
        $organizationsArr = $this->extractEntityCount($entities,"ORGANIZATION");

        $colors = ['neutral'=>'grey', 'positive'=>'green','negative'=>'red'];

        $sentimentsGraphData = generatePieChartGraphData($sentiments, $colors);



        $locations = createTagCloudArray($locationsArr, LOCATION_COLOR);
        $persons =  createTagCloudArray($personsArr, PERSON_COLOR);
        $organizations = createTagCloudArray($organizationsArr, ORGANIZATION_COLOR);
        $entitesArr = array_merge($locations, $persons, $organizations);
        $entities = json_encode($entitesArr);
        //dd($entites);


        $q = $this->query;


        return compact(
            'sentimentsGraphData',
            'entities'
        );

    }

    public function initTweetsForTwitterTimeLine($userID)
    {
        $tweetData = array();
        try
        {
            //Twitter::getGeo();
            $response = Twitter::getHomeTimeline(['count' => 500, 'format' => 'array']);

            //dd($response);

            if(isset($response)){
                foreach($response as $tweet){

                    $createdOn = Carbon::createFromTimestamp(strtotime($tweet['created_at']));
                    //dd($tweet);

                    $tweets = [];
                    $tweets['tweet_id'] = $tweet['id'];
                    $tweets['text'] = $tweet['text'];

                    //dd($createdOn);
                    $model = Tweet::firstOrNew(['tweet_id'=>$tweet['id']]);

                    if($model->tweet == null){
                        $tweets['user_id'] = $userID;
                        $model->user_id = $userID;

                        $tweets['tweet'] = json_encode($tweet);
                        $model->tweet = json_encode($tweet);

                        $tweets['created_at'] = $createdOn;
                        $model->created_at = $createdOn;

                        array_push($tweetData, $tweets);
                        $model->save();
                    }

                    unset($model);
                }
            }

            //$users = Twitter::getTrendsPlace();///(['screen_name'=>"mr_farrukh"]);


            $this->response = $response;
            $this->query = "";
        }
        catch (Exception $e)
        {
            // dd(Twitter::error());
            dd(Twitter::logs());
        }

        return $tweetData;
    }

    public function populateTrainingData($query, $userID)
    {

        $response = Twitter::getSearch(['q' => $query,
            'count' => 200,
            'format' => 'array']);

        $this->response = $response;
        $this->query = $query;

        $tweetData = array();
        if(isset($response['statuses'])){
            foreach($response['statuses'] as $tweet){

                $createdOn = Carbon::createFromTimestamp(strtotime($tweet['created_at']));
                //dd($tweet);

                $tweets = [];
                $tweets['tweet_id'] = $tweet['id'];
                $tweets['text'] = $tweet['text'];

                //dd($createdOn);
                $model = Tweet::firstOrCreate(['tweet_id'=>$tweet['id']]);


                $tweets['user_id'] = $userID;
                $model->user_id = $userID;

                $tweets['tweet'] = json_encode($tweet);
                $model->tweet = json_encode($tweet);


                $tweets['search_query'] = $query;
                $model->search_query = $query;


                $tweets['created_at'] = $createdOn;
                $model->created_at = $createdOn;

                array_push($tweetData, $tweets);
                $model->save();
                unset($model);
            }
        }

        //dispatch(new ExtractNamedEntites($tweetData));

        return $tweetData;

    }

    public function populateTwitterData($query, $userID, $connectorID)
    {

        $connector = SocialProvider::find($connectorID);
        if($connector->conn_type == TWITTER_KEYOWRD){
            $response = Twitter::getSearch(['q' => $query,
                'count' => 200,
                'format' => 'array']);;
        }
        else if($connector->conn_type == TWITTER_GEOPOINT){

            $response = Twitter::getGeoSearch(['lat' => $connector->lat,
                'long' => $connector->lng,
                'count' => 200,
                'format' => 'array']);

        }


        $this->response = $response;
        $this->query = $query;

        $tweetData = array();
        if(isset($response['statuses'])){
            foreach($response['statuses'] as $tweet){

                $createdOn = Carbon::createFromTimestamp(strtotime($tweet['created_at']));
                //dd($tweet);

                $tweets = [];
                $tweets['tweet_id'] = $tweet['id'];
                $tweets['text'] = $tweet['text'];

                //dd($createdOn);
                $model = Tweet::firstOrCreate(['tweet_id'=>$tweet['id']]);


                $tweets['user_id'] = $userID;
                $model->user_id = $userID;

                $tweets['tweet'] = json_encode($tweet);
                $model->tweet = json_encode($tweet);


                $tweets['search_query'] = $query;
                $model->search_query = $query;


                $tweets['created_at'] = $createdOn;
                $model->created_at = $createdOn;

                array_push($tweetData, $tweets);
                $model->save();

                if ( $model->wasRecentlyCreated ) {
                    $connector->total_records = $connector->total_records + 1;
                    $connector->save();
                }

                unset($model);
            }
        }

        //dispatch(new ExtractNamedEntites($tweetData));

        return $tweetData;

    }

    public function getUserTimeLine($screenname){
        $tweets = Twitter::getUserTimeline(['screen_name' => $screenname, 'count' => 200, 'format' => 'object']);
        $followers_count = $tweets[0]->user->followers_count;

    }


    public function tagTweetsWithNER($tweetIds){
        ini_set('max_execution_time', 180); //3 minutes
        foreach($tweetIds as $tweetID){
            //dd($tweet);
            $model = Tweet::where('tweet_id','=',$tweetID)->first();

            $decodedTweet = json_decode($model->tweet);
            if($model->entities == null || $model->entities == ""){
                $entites = NameEntityRecongizer::getNamedEntities($decodedTweet->text);
                $model->entities = json_encode($entites);
                $model->save();
            }

            unset($model);
        }
    }


    public function tagTweetsFromAliyan($tweetIds){
        ini_set('max_execution_time', 180000); //3000 minutes

        $textapi = new AYLIEN\TextAPI(ALYIIAN_APP_ID, ALYIAN_KEY);

        //$maxCount = 0;
        $count = 0;
        foreach($tweetIds as $tweetID){
            //dd($tweet);
            $model = Tweet::where('tweet_id','=',$tweetID)
                ->where('is_enriched','=','No')->first();

            $decodedTweet = json_decode($model->tweet);
            if($model->entities == null || $model->entities == ""){
                $text = $decodedTweet->text;
                $sentiment = $textapi->Sentiment(array(
                    'text' => $text
                ));

                if(is_object($sentiment)) {
                    $polarity = $sentiment->polarity;
                    $model->predictied_sentiment = $polarity;
                }



                $concepts = $textapi->Concepts(array('text' => $text));
                $model->concepts = json_encode($concepts);

                $entites = $textapi->Entities(array(
                    'text' => $text
                ));
                $model->entities = json_encode($entites);

                $classifications = $textapi->Classify(array('text' => $text));
                $model->classifications = json_encode($classifications);

                $model->is_enriched = "Yes";
                $model->save();
            }

            unset($model);
            $count++;
//            if($maxCount != 0 && $count >= $maxCount){
//                break;
//            }
        }
    }

    public function enrichPendingTweets(){
        ini_set('max_execution_time', 180000); //3000 minutes

        $textapi = new AYLIEN\TextAPI(ALYIIAN_APP_ID, ALYIAN_KEY);

        $tweets = Tweet::where('is_enriched','=','No')->get();
        $count = 0;
        foreach($tweets as $model){
            $decodedTweet = json_decode($model->tweet);
            if($model->entities == null || $model->entities == ""){
                $text = $decodedTweet->text;
                $sentiment = $textapi->Sentiment(array(
                    'text' => $text
                ));

                if(is_object($sentiment)) {
                    $polarity = $sentiment->polarity;
                    $model->predictied_sentiment = $polarity;
                }

                $concepts = $textapi->Concepts(array('text' => $text));
                $model->concepts = json_encode($concepts);

                $entites = $textapi->Entities(array(
                    'text' => $text
                ));
                $model->entities = json_encode($entites);

                $classifications = $textapi->Classify(array('text' => $text));
                $model->classifications = json_encode($classifications);

                $model->is_enriched = "Yes";
                $model->save();
            }

            unset($model);
            $count++;
        }
    }


    public function getTwitterResponse($field = "statuses"){

        $response = $this->response;

        $dataField = $response;
        if(isset($response[$field]))
        {
            $dataField = $response[$field];
        }


        $tweets= $this->extractTwitterInformation($dataField);
        $this->calculateMetrics($tweets , $this->query);

        $potentialReach = $this->potentialReach;
        $potentialImpression = $this->potentialImpression;
        $followers = $this->followers;
        $friends = $this->friends;


        $userMentions = generateTagCloud($this->userMentions);
        $hashtags = generateTagCloud($this->hashtags);

        //dd($hashtags);

        $impressionGraphData = generateBarChartGraphData($this->impressionGraphData, true);
        $tweetsTimeLineData = generateBarChartGraphData($this->tweetsTimeLineData);
        $contributers = $this->contributers;
        $noOfTweets = $this->noOfTweets;
        $timeInterval = $this->timeInterval;
        $text = $this->text;
        $q = $this->query;

        $textForAnalysis = implode(".",$text);
        $entites = NameEntityRecongizer::getNamedEntities($textForAnalysis);

        $locationsArr = $this->extractEntityCount($entites,"LOCATION");
        $personsArr = $this->extractEntityCount($entites,"PERSON");
        $organizationsArr = $this->extractEntityCount($entites,"ORGANIZATION");


        $locations = createTagCloudArray($locationsArr, LOCATION_COLOR);
        $persons =  createTagCloudArray($personsArr, PERSON_COLOR);
        $organizations = createTagCloudArray($organizationsArr, ORGANIZATION_COLOR);
        $entitesArr = array_merge($locations, $persons, $organizations);

        $entities = json_encode($entitesArr);

        return compact('potentialReach',
            'potentialImpression',
            'followers',
            'entities',
            'text',
            'q',
            'friends',
            'userMentions',
            'hashtags',
            'impressionGraphData',
            'tweetsTimeLineData',
            'contributers',
            'noOfTweets',
            'tweets',
            'timeInterval'
        );

    }

    public function search($query){

        $response = Twitter::getSearch(['q'=>$query,
            'count' => 200,
            'format' => 'array']);

        $tweets= $this->extractTwitterInformation($response['statuses']);
        $this->calculateMetrics($tweets , $query);

        $potentialReach = $this->potentialReach;
        $potentialImpression = $this->potentialImpression;
        $followers = $this->followers;
        $friends = $this->friends;

        $userMentions = generateTagCloud($this->userMentions);
        $hashtags = generateTagCloud($this->hashtags);

        //dd($hashtags);

        $impressionGraphData = generateBarChartGraphData($this->impressionGraphData, true);
        $tweetsTimeLineData = generateBarChartGraphData($this->tweetsTimeLineData);
        $contributers = $this->contributers;
        $noOfTweets = $this->noOfTweets;
        $timeInterval = $this->timeInterval;
        $text = $this->text;



        return compact('potentialReach',
            'potentialImpression',
            'followers',
            'text',
            'friends',
            'userMentions',
            'hashtags',
            'impressionGraphData',
            'tweetsTimeLineData',
            'contributers',
            'noOfTweets',
            'tweets',
            'timeInterval'
            );

    }

    public function populateTextFromDocuments( $documents )
    {
        $this->text = array();
        foreach($documents as $tweet)
        {
            $arrTweet = $tweet->toArray();
            $tweetText = $arrTweet['_source']['text'];

            $this->text[] = $this->cleanifyTweet($tweetText);
        }
    }

    public function injectAndPrepareData( $documents, $aggregations ) {

        $this->populateTextFromDocuments($documents);
        $this->extractAggregates($aggregations);

        $tweets = $this->cleanifyInformation($documents);
        $this->calculateMetrics($tweets);

        $potentialReach = $this->potentialReach;
        $potentialImpression = $this->potentialImpression;
        $followers = $this->followers;
        $friends = $this->friends;

        $userMentions = generateTagCloud($this->userMentions);
        $hashtags = generateTagCloud($this->hashtags);

        //dd($hashtags);

        $impressionGraphData = generateBarChartGraphData($this->impressionGraphData, true);
        $tweetsTimeLineData = generateBarChartGraphData($this->tweetsTimeLineData);
        $contributers = $this->contributers;
        $noOfTweets = $this->noOfTweets;
        $timeInterval = $this->timeInterval;
        $text = $this->text;
        $colors = ['pos'=>'green','neg'=>'red'];

        $sentimentsGraphData = generatePieChartGraphData($this->extraAggrates['Sentiments'], $colors);



        $textForAnalysis = implode(".",$text);
        $entites = NameEntityRecongizer::getNamedEntities($textForAnalysis);

        $locationsArr = $this->extractEntityCount($entites,"LOCATION");
        $personsArr = $this->extractEntityCount($entites,"PERSON");
        $organizationsArr = $this->extractEntityCount($entites,"ORGANIZATION");


        $locations = createTagCloudArray($locationsArr, LOCATION_COLOR);
        $persons =  createTagCloudArray($personsArr, PERSON_COLOR);
        $organizations = createTagCloudArray($organizationsArr, ORGANIZATION_COLOR);
        $entitesArr = array_merge($locations, $persons, $organizations);

        $entities = json_encode($entitesArr);

        return compact('potentialReach',
            'potentialImpression',
            'sentimentsGraphData',
            'followers',
            'entities',
            'text',
            'q',
            'friends',
            'userMentions',
            'hashtags',
            'impressionGraphData',
            'tweetsTimeLineData',
            'contributers',
            'noOfTweets',
            'tweets',
            'timeInterval'
        );


    }

    private function cleanifyInformation($documents){

        $tweets = array();
        //dd($documents);

        foreach($documents as $document){
            $raw = $document->toArray();

            $data = $raw['_source'];
            $data['id'] = $raw['_id'];

            $datetime = new DateTime($data['created_at']);
            //$datetime->setTimezone( null ); //new DateTimeZone('Asia/Karachi'));
            $created_at =  $datetime->format('c');
            $date =  $datetime->format('M j, Y g:i A');
            $dateMySQL =  $datetime->format('Y-m-d h:i:s');

            $data['date_mysql'] = $dateMySQL;
            $data['date_time'] = $datetime->format('Y,n,j,h,i,s');
            $data['date'] = $date;
            $data['created_at'] = $created_at;



            $this->extractCraftedEntitesCount($data, 'hashtags',  $this->hashtags);




            //$this->extractEntitesCount($tweet, 'symbols', $this->symbols);
            $this->extractCraftedEntitesCount($data, 'user_mentions_names', $this->userMentions);

            array_push($tweets, $data);

        }

        return $tweets;

    }

    protected function extractEntityCount($entites, $field){
        $entitiesData = array();
        if( isset($entites[$field] ) ){
            foreach($entites[$field] as $entity){
                $entity = trim($entity);
                if( !isset( $entitiesData[$entity] ) ){
                    $entitiesData[$entity] = 1;
                }
                else
                {
                    $entitiesData[$entity]++;
                }
            }
        }

        return $entitiesData;
    }

    protected function buildEntitesFromTweets($tweets){
        foreach ($tweets as $tweet){
            dd($tweet);
        }
    }

    protected function extractTwitterInformation($response){
        $tweetData = array();

        $this->hashtags = array();
        $this->text = array();
       //dd($response['statuses']);

        if(isset($response)){
            foreach($response as $tweet){

                $tweetExtracted = array();

                $tweetExtracted['id'] = $tweet['id'];
                $tweetExtracted['text'] = $tweet['text'];

                $this->text[] = $this->cleanifyTweet($tweet['text']);

                $tweetExtracted['retweet_count'] = $tweet['retweet_count'];
                $tweetExtracted['favorite_count'] = $tweet['favorite_count'];

                $tweetExtracted['hashtags'] = $tweet['entities']['hashtags'];
                $tweetExtracted['symbols'] = $tweet['entities']['symbols'];
                $tweetExtracted['urls'] = $tweet['entities']['urls'];
                $tweetExtracted['user_mentions'] = $tweet['entities']['user_mentions'];

                $this->extractEntitesCount($tweet, 'hashtags', 'text', $this->hashtags);
                //$this->extractEntitesCount($tweet, 'symbols', $this->symbols);
                $this->extractEntitesCount($tweet, 'user_mentions','name', $this->userMentions);

                $datetime = new DateTime($tweet['created_at']);
                //$datetime->setTimezone( null ); //new DateTimeZone('Asia/Karachi'));
                $created_at =  $datetime->format('c');
                $date =  $datetime->format('M j, Y g:i A');
                $dateMySQL =  $datetime->format('Y-m-d h:i:s');

                $tweetExtracted['date_mysql'] = $dateMySQL;
                $tweetExtracted['date_time'] = $datetime->format('Y,n,j,h,i,s');
                $tweetExtracted['date'] = $date;
                $tweetExtracted['created_at'] = $created_at;
                $tweetExtracted['user_handle'] = $tweet['user']['screen_name'];
                $tweetExtracted['user_name'] = $tweet['user']['name'];
                $tweetExtracted['user_image'] = $tweet['user']['profile_image_url'];
                $tweetExtracted['user_followers_count'] = $tweet['user']['followers_count'];
                $tweetExtracted['user_following_count'] = $tweet['user']['following'];
                $tweetExtracted['user_friends_count'] = $tweet['user']['friends_count'];
                $tweetExtracted['geo_enabled'] = $tweet['user']['geo_enabled'];
                $tweetExtracted['verified'] = $tweet['user']['verified'];


                array_push($tweetData, $tweetExtracted);
            }
        }


        return $tweetData;
    }

    protected function extractCraftedEntitesCount($tweet, $entity, &$arr){

        if(isset($tweet[$entity])){

            foreach( $tweet[$entity] as $objEntity){

                if(!isset($arr[$objEntity])){
                    $arr[$objEntity] = 1;
                }
                else {
                    $arr[$objEntity] += 1;
                }

            }
        }

    }

    protected function extractEntitesCount($tweet, $entity, $field, &$arr, $prefix=""){

        if(isset($tweet['entities'][$entity])){
            foreach( $tweet['entities'][$entity] as $objEntity){


                if(!isset($arr[$objEntity[$field]])){
                    $arr[$prefix.$objEntity[$field]] = 1;
                }
                else {
                    $arr[$prefix.$objEntity[$field]] += 1;
                }

            }
        }

    }

    protected function calculateMetrics( $tweets){

        $apperanceInQuery = 1;
        $userFollowersBase = array();
        $userFollowingBase = array();


        $impressionGraph = array();
        $impressionGraph['100'] = 0;
        $impressionGraph['1000'] = 0;
        $impressionGraph['10000'] = 0;
        $impressionGraph['100000'] = 0;
        $impressionGraph['100000000'] = 0;

        $dateTimeArr = array();
        $rawDateTime = array();

        foreach($tweets as $tweet){
            $userFollowersBase[$tweet['user_handle']] = $tweet['user_followers_count'];
            $userFollowingBase[$tweet['user_handle']] = $tweet['user_friends_count'];

            $rawDateTime[] = $tweet['date_mysql'];

            if(!isset($dateTimeArr[$tweet['date_time']])){
                $dateTimeArr[$tweet['date_time']] = 0;
            }
            $dateTimeArr[$tweet['date_time']] = $dateTimeArr[$tweet['date_time']] + 1;


            if($tweet['user_followers_count'] < 100) {
                $impressionGraph['100'] = $impressionGraph['100'] + 1;
            }
            else if($tweet['user_followers_count'] < 1000) {
                $impressionGraph['1000'] = $impressionGraph['1000'] + 1;
            }
            else if($tweet['user_followers_count'] < 10000) {
                $impressionGraph['10000'] = $impressionGraph['10000'] + 1;
            }
            else if($tweet['user_followers_count'] < 100000) {
                $impressionGraph['100000'] = $impressionGraph['100000'] + 1;
            }
            else if($tweet['user_followers_count'] < 100000000) {
                $impressionGraph['100000000'] = $impressionGraph['100000000'] + 1;
            }
            $usermentions = [];
            if(isset( $tweet['user_mentions']) )
            {
                $usermentions = $tweet['user_mentions'];
            }

            if(isset( $tweet['user_mentions_names']) )
            {
                $usermentions = $tweet['user_mentions_names'];
            }


            $apperanceInQuery = count($tweet['hashtags']) + count($usermentions);
        }

        usort($rawDateTime, function($a, $b) {
            $dateTimestamp1 = strtotime($a);
            $dateTimestamp2 = strtotime($b);

            return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
        });

        $timeInterval = "";
        if( is_array($rawDateTime) && count($rawDateTime)>0 )
        {
            $fromDate =  $rawDateTime[0];
            $toDate =  $rawDateTime[count($rawDateTime) - 1];
            $timeInterval = dayTimeInterval($fromDate,$toDate);
        }

        $userCounts = count($userFollowersBase);
        $userFollowers = array_reduce($userFollowersBase, "sum");

        $userFollowings = array_reduce($userFollowingBase, "sum");
        $impressionGraph = natkrsort($impressionGraph);

        $this->timeInterval = $timeInterval;
        $this->noOfTweets = count($tweets);
        $this->contributers = count($userFollowingBase);

        $this->impressionGraphData = $impressionGraph;
        $this->tweetsTimeLineData = $dateTimeArr;

        $this->friends = $userFollowings;
        $this->followers = $userFollowers;
        $this->potentialReach = $userCounts + $userFollowers;
        $this->potentialImpression = $apperanceInQuery * $this->potentialReach;
    }

    private function extractAggregates($aggregations)
    {
        $facets = array();
        foreach($aggregations as $key=>$aggregation){
            if(isset($aggregation['buckets']))
            {
                $facets[$key] = array();
                foreach($aggregation['buckets'] as $data ){
                    $facets[$key][$data['key']] =  $data['doc_count'];
                }
            }
        }
        $this->extraAggrates = $facets;

    }

    protected function cleanifyTweet($str){

        $str = str_replace('RT', '', $str); // @someone
        $str = preg_replace('/#([\w-]+)/i', '', $str); // @someone
        $str = preg_replace('/@([\w-]+)/i', '', $str); // #tag
        $str = preg_replace('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', '', $str);

        return $str;
    }
}