<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Messages\MailMessage;

class CourseInvitationNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    private $course;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($course)
    {
        $this->course = $course;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line( $this->course->message ?? ' A user ' . auth()->user()->name . ' want to join your course ' . $this->course->title )
            ->action( ' User details', url( '/program/details' ) );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->course->message ?? 'Want to connect your course ' . $this->course->title,
            'to'=>$notifiable,
            'from'=>auth()->user(),
            'module'=>'course',
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel( 'App.User.' . $this->course->user->id );
    }
}
