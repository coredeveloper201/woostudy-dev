<?php

namespace App;


class Education extends Models
{
    protected $fillable = ['title', 'grade', 'user_id', 'institute_id', 'completion_date', 'percentage', 'education_level_id'];
    protected $table = 'educations';

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function institute()
    {
        return $this->belongsTo( Institute::class );
    }

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class );
    }
}
