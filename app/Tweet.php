<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $fillable = ['user_id', 'tweet_id', 'tweet_user_id', 'tweet_screen_name', 'tweet_created_at', 'tweet', 'tweet_text', 'category', 'entities', 'user_sentiment', 'predictied_sentiment', 'classifications', 'concepts', 'search_query', 'is_enriched'];

    public static function getFieldGroupData($field, $emptyFieldText = "NonLabled"){

        $sentimentCounts = self::groupBy($field)->select($field, \DB::raw('count(*) as total'))
            ->orderBy('created_at', 'desc')->get();
        $sentArr = $sentimentCounts->toArray();
        $groupData = array();
        foreach($sentArr as $item){
            if( $item[$field] != "" ){
                $groupData[$item[$field]] = $item['total'];
            }
        }

        return $groupData;
    }

    public static function getFieldGroupDataWithKey($field, $emptyFieldText = "NonLabled"){

        $sentimentCounts = self::groupBy($field)->select($field, \DB::raw('count(*) as total'))
            ->orderBy('created_at', 'desc')->get();
        $sentArr = $sentimentCounts->toArray();
        $groupData = array();
        $i = 0;
        foreach($sentArr as $item){
            if( $item[$field] != "" ){
                $groupData[$i]['key'] = $item[$field];
                $groupData[$i]['value'] = $item['total'];
                $i++;
            }
        }
        return $groupData;
    }

    public static function findEmpty($field){
        $emptyCounts =   \DB::table('tweets')->where($field, '=', '')->orWhereNull($field)->count();
        return $emptyCounts;
    }
}
