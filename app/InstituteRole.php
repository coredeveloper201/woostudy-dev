<?php

namespace App;


class InstituteRole extends Models
{
    protected $table = 'institute_roles';

    protected $fillable = ['title', 'slug'];


    public function instituteAuthors()
    {
        return $this->hasMany( InstituteAuthor::class );

    }
}
