<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchLog extends Model
{
    protected $table = 'search_logs';
    protected $fillable = ['id', 'user_id', 'search_criteria', 'date_time', 'geo_location', 'ip_address', 'type'];
}
