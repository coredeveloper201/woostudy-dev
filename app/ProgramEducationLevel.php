<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramEducationLevel extends Model
{
    protected $fillable = ['educational_level_id', 'program_id'];
}
