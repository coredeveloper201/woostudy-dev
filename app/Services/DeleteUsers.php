<?php


namespace App\Services;


use App\Address;
use App\Institute;
use App\ProfileOption;
use App\Program;
use App\Tweet;
use App\User;

class DeleteUsers
{
    public $country;
    public $country_id;

    public function __construct()
    {
        $this->country = 'USA';
        $this->country_id = 182;
    }

    public function getAllAddresses()
    {
        $addresses = Address::select('id', 'addressable_id')->where('country_id', $this->country_id);
        $listOfUserIds = [];
        $listOfAddressIds = [];
        foreach ($addresses->get() as $address){
            $listOfUserIds[] = $address->addressable_id;
            $listOfAddressIds[] = $address->id;
        }
        $deletedInstitutes = $this->deleteInstitute($listOfUserIds);
        $deletedProfileOptions = $this->deleteProfileOptions($listOfUserIds);
        $deletedPrograms = $this->deletePrograms($listOfAddressIds);
        $deletedTweets = $this->deleteTweets($listOfUserIds);
        $deletedUsers = $this->deleteUsers($listOfAddressIds);
        $deletedAddresses = $this->deleteAddresses($addresses);
        $deleteInfo = array(
            'deletedInstitutes' => $deletedInstitutes,
            'deletedProfileOptions' => $deletedProfileOptions,
            'deletedPrograms' => $deletedPrograms,
            'deletedTweets' => $deletedTweets,
            'deletedUsers' => $deletedUsers,
            'deletedAddresses' => $deletedAddresses,
        );
        return $deleteInfo;
    }

    public function deleteInstitute($listOfUserIds)
    {
        $institutes = Institute::whereIn('user_id', $listOfUserIds);
        if ($institutes) return $institutes->delete();
    }

    public function deleteProfileOptions($listOfUserIds)
    {
        $profileOptions = ProfileOption::whereIn('user_id', $listOfUserIds);
        if ($profileOptions) return $profileOptions->delete();
    }

    public function deletePrograms($listOfAddressIds)
    {
        $programs = Program::whereIn('address_id', $listOfAddressIds);
        if ($programs) return $programs->delete();
    }

    public function deleteTweets($listOfUserIds)
    {
        $tweets = Tweet::whereIn('user_id', $listOfUserIds);
        if ($tweets) return $tweets->delete();
    }

    public function deleteUsers($listOfAddressIds)
    {
        $users = User::whereIn('id', $listOfAddressIds);
        if ($users) return $users->delete();
    }

    public function deleteAddresses($addresses)
    {
        if ($addresses) return $addresses->delete();
    }
}
