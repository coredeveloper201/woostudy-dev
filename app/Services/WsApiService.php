<?php

namespace App\Services;

use App\Http\Controllers\Wsapi\WsApiController;

class WsApiService
{
    public $api;
    public $totalPages;

    public function __construct()
    {
        $this->api = new WsApiController();
    }

    public function getTotalPages($countryName)
    {
        $data = $this->api->loadView($countryName);
        $this->totalPages = $data['total_page'];
        return $data;
    }

    public function getData($countryName, $page)
    {
        return $this->api->apiTestMethod($countryName, $page);
    }

    public function importData($last_page)
    {
        $url = 'http://127.0.0.1:8000/api/ws-institute/USA/1/' . $last_page;
        $postdata = null;
        $returnDetails = json_decode($this->curloperation($url, $postdata));
        return $returnDetails;
    }

    public function getDetails($details)
    {
        $url = "http://ws.aaemnow.com/instituteDetails";

        $postdata = [
            'indexCountry' => $details->country,
            'output_mode' => 'json',
            'item_id' => $details->id
        ];

        $returnDetails = json_decode($this->curloperation($url, $postdata));
//        $returnDetails->data->storepai_id = $details->id;
        return $returnDetails;
    }

    public function curloperation($url, $dataArray)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => json_encode($dataArray, true),
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return null;
        } else {
            return $response;
        }
    }
}
