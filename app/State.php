<?php

namespace App;


class State extends Models
{
    protected $table = 'states';
    protected $fillable = ['country_id', 'title', 'code'];


    public function country()
    {
        return $this->belongsTo( Country::class );
    }


}
