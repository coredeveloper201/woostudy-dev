<?php

namespace App;

class Country extends Models
{
    protected $table = 'countries';

    protected $fillable = ['title', 'code', 'region_id', 'slug', 'flag'];

    public function address()
    {
        return $this->hasMany( Address::class );
    }

    public function states()
    {
        return $this->hasMany( Address::class );
    }

    public function region()
    {
        return $this->belongsTo( Region::class );
    }
}
