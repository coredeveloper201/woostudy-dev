<?php

namespace App;


class Department extends Models
{
    protected $table = 'departments';
    protected $fillable = ['id', 'title', 'status', 'created_by', 'approved_by', 'approved_at'];


    public function instituteAuthor()
    {
        return $this->hasMany( InstituteAuthor::class );
    }


}
